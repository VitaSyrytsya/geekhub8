package org.geekhub.lesson11.task2.resourses;

import org.geekhub.lesson11.Ignore;
import org.geekhub.lesson11.task1.CanBeCloned;

public class HumanSecond implements CanBeCloned {
    private  int age;
    @Ignore
    private String salary;

    @Ignore
    private HumanSecond.Profession profession;

    public HumanSecond.Profession getProfession() {
        return profession;
    }

    public void setProfession(HumanSecond.Profession profession) {
        this.profession = profession;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getSalary() {
        return salary;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public static class Profession  {

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

}
