package org.geekhub.lesson11.task2.resourses;

import org.geekhub.lesson11.task1.CanBeCloned;

public class HumanFirst implements CanBeCloned {
    private  int age;

    private String salary;

    private Profession profession;

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getSalary() {
        return salary;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

   public static class Profession implements CanBeCloned {

        private String type;

       public String getType() {
           return type;
       }

       public void setType(String type) {
           this.type = type;
       }
   }

}
