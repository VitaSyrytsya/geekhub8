package org.geekhub.lesson11.task2.resourses;

import org.geekhub.lesson11.task1.CanBeCloned;

import java.util.List;

public class Human implements CanBeCloned{

    private Profession profession ;

    private List<Relatives> relativesList;

    public static class Relatives implements CanBeCloned{
        private String mom;
        private String dad;

        public String getDad() {
            return dad;
        }

        public void setDad(String dad) {
            this.dad = dad;
        }

        public String getMom() {
            return mom;
        }

        public void setMom(String mom) {
            this.mom = mom;
        }

        @Override
        public String toString() {
            return mom + ""+ dad;
        }
    }


    public static class Profession implements CanBeCloned {
        private String type;
        private int salary;
        private Directors directors;

        public static class Directors implements CanBeCloned{
            private String Director;
            private String Manager;

            public String getManager() {
                return Manager;
            }

            public void setManager(String manager) {
                Manager = manager;
            }

            public String getDirector() {
                return Director;
            }

            public void setDirector(String director) {
                Director = director;
            }
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }

        public Directors getDirectors() {
            return directors;
        }

        public void setDirectors(Directors directors) {
            this.directors = directors;
        }

    }

    public List<Relatives> getRelativesList() {
        return relativesList;
    }

    public void setRelativesList(List<Relatives> relativesList) {
        this.relativesList = relativesList;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }


}
