package org.geekhub.lesson11.task1;

import org.geekhub.lesson11.task1.resourses.Human;
import org.geekhub.lesson11.task1.resourses.HumanFirst;
import org.geekhub.lesson11.task1.resourses.HumanSecond;
import org.geekhub.lesson11.ObjectsEquals;
import org.geekhub.lesson11.task1.resourses.StudentFirst;
import org.geekhub.lesson11.task1.resourses.StudentSecond;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CloneCreatorImplTest {
    private CloneCreatorImpl cloneCreator;

    @BeforeMethod
    public void setUp() {
        cloneCreator = new CloneCreatorImpl();
    }

    @Test(expectedExceptions = CanNotCloneException.class)
    public void shouldThrowExceptionIfObjectHasUncloneableFields() throws CanNotCloneException {
        StudentFirst studentFirst = new StudentFirst();
        StudentFirst.Professor professor = new StudentFirst.Professor();
        professor.setName("Arnold");
        List<StudentFirst.Professor> professorList = Collections.singletonList(professor);
        studentFirst.setProfessors(professorList);

        cloneCreator.clone(studentFirst);
    }

    @Test
    public void cloneObjectIfObjectDoesNotHaveUncloneableFields() throws CanNotCloneException {
        ObjectsEquals equals = new ObjectsEquals();
        StudentSecond student = new StudentSecond();
        StudentSecond.Professor professor = new StudentSecond.Professor();
        professor.setName("Arnold");
        ArrayList<StudentSecond.Professor> professorList = new ArrayList<>();
        professorList.add(professor);
        student.setProfessors(professorList);

        StudentSecond clonedStudent = cloneCreator.clone(student);

        Assert.assertTrue(equals.equalsComplicatedObjects(student, clonedStudent));
        Assert.assertEquals(clonedStudent.getProfessors().get(0).getName(), "Arnold");
    }

    @Test
    public void cloneObjectIfSomeFieldsHaveNotBeenSet() throws CanNotCloneException {
        ObjectsEquals equals = new ObjectsEquals();
        HumanSecond human = new HumanSecond();
        human.setAge(418);

        HumanSecond clonedHuman = cloneCreator.clone(human);

        Assert.assertTrue(equals.equalsComplicatedObjects(clonedHuman, human));

    }

    @Test
    public void shouldMakeDEEPcopyOfObject() throws CanNotCloneException {
        ObjectsEquals equals = new ObjectsEquals();
        StudentSecond student = new StudentSecond();
        StudentSecond.Professor professor = new StudentSecond.Professor();
        professor.setName("Arnold");
        ArrayList<StudentSecond.Professor> professorList = new ArrayList<>();
        professorList.add(professor);
        student.setProfessors(professorList);

        StudentSecond clonedStudent = cloneCreator.clone(student);
        student.getProfessors().get(0).setName("Mark");

        Assert.assertTrue(!equals.equalsComplicatedObjects(student, clonedStudent));
        Assert.assertNotEquals(clonedStudent.getProfessors().get(0).getName(), "Mark");
    }

    @Test
    public void shouldMakeDEEPcopyOfObject2() throws CanNotCloneException {
        ObjectsEquals equals = new ObjectsEquals();
        Human human = new Human();
        Human.Profession profession = new Human.Profession();
        Human.Profession.Directors directors = new Human.Profession.Directors();
        directors.setDirector("Director");
        directors.setManager("Manager");
        profession.setDirectors(directors);
        profession.setSalary(500);
        profession.setType("Ecologist");
        List<Human.Profession> professionList = new ArrayList<>();
        professionList.add(profession);
        human.setProfessionList(professionList);

        Human clonedHuman = cloneCreator.clone(human);
        human.getProfessionList().get(0).getDirectors().setManager("Manager2");
        human.getProfessionList().get(0).setType("Trainer");

        Assert.assertFalse(equals.equalsComplicatedObjects(human, clonedHuman));
        Assert.assertEquals(clonedHuman.getProfessionList().get(0).getDirectors().
                getManager(), "Manager");
        Assert.assertEquals(clonedHuman.getProfessionList().get(0).getType(), "Ecologist");

    }

    @Test
    public void shouldNotCloneIgnoredFields() throws CanNotCloneException {
        ObjectsEquals equals = new ObjectsEquals();
        HumanFirst human = new HumanFirst();
        human.setSalary("500$");
        human.setAge(12);
        HumanFirst.Profession profession = new HumanFirst.Profession();
        profession.setType("Teacher");
        human.setProfession(profession);

        HumanFirst clonedHuman = cloneCreator.clone(human);
        human.getProfession().setType("Dentist");
        human.setSalary("1000$");

        Assert.assertTrue(equals.equalsComplicatedObjects(clonedHuman, human));
        Assert.assertNull(clonedHuman.getSalary());
        Assert.assertNull(clonedHuman.getProfession().getType());
    }

    @Test
    public void shouldNotCloneIgnoredNotCloneableFields() throws CanNotCloneException {
        ObjectsEquals equals = new ObjectsEquals();
        HumanSecond human = new HumanSecond();
        human.setAge(18);
        human.setSalary("322$");
        HumanSecond.Profession profession = new HumanSecond.Profession();
        profession.setType("City farmer");
        human.setProfession(profession);

        HumanSecond clonedHuman = cloneCreator.clone(human);
        human.getProfession().setType("Manager");

        Assert.assertTrue(equals.equalsComplicatedObjects(human, clonedHuman));
        Assert.assertNull(clonedHuman.getProfession());
    }


}
