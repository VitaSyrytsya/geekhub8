package org.geekhub.lesson11.task1.resourses;

import org.geekhub.lesson11.task1.CanBeCloned;

import java.util.List;

public class StudentFirst implements CanBeCloned {

    private List<Professor> professors;

    public void setProfessors(List<Professor>professors) {
        this.professors = professors;
    }

    public List<Professor> getProfessors() {
        return professors;
    }


    public static class Professor {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
