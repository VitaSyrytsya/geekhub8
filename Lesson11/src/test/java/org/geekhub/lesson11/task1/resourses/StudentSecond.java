package org.geekhub.lesson11.task1.resourses;

import org.geekhub.lesson11.task1.CanBeCloned;

import java.util.List;

public class StudentSecond implements CanBeCloned {

    private List<StudentSecond.Professor> professors;

    public void setProfessors(List<StudentSecond.Professor> professors) {
        this.professors = professors;
    }

    public List<StudentSecond.Professor> getProfessors() {
        return professors;
    }


    public static class Professor implements CanBeCloned {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
