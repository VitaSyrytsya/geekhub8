package org.geekhub.lesson11.task2;

import org.geekhub.lesson11.Ignore;
import org.geekhub.lesson11.ObjectsEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.reflect.*;

public class ChangeSetExtractorImpl implements ChangeSetExtractor {

    private ObjectsEquals equals = new ObjectsEquals();

    private static final Logger log = Logger.getLogger(ChangeSetExtractorImpl.class.getName());

    @Override
    public ChangeSet extract(Object oldObject, Object newObject) {
        Map<String, ArrayList> x = new HashMap<>();
        ChangeSet result = new ChangeSet(x);
        if (equals.objectBuiltInJdk(oldObject) &&
                !(oldObject instanceof ArrayList) &&
                !(Objects.equals(oldObject, null))) {
            if (!oldObject.equals(newObject)) {
                ArrayList<Object> a = new ArrayList<>();
                a.add(oldObject);
                a.add(newObject);
                x.put("Old object:", a);
            }
        } else {
            result.setOldObject(oldObject);
            result.setNewObject(newObject);
            compare(oldObject, newObject, x);
        }
        log.info("ChangeSet of object"+ oldObject+ "has been created");
        return result;
    }

    private void compare(Object oldObject, Object newObject, Map<String, ArrayList> x) {
        Field[] fields = oldObject.getClass().getDeclaredFields();
        ArrayList<Object> arrayList = new ArrayList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Ignore.class)) {
                try {
                    Object a = field.get(oldObject);
                    Object b = field.get(newObject);
                    if (a != null && b != null) {
                        if (equals.objectBuiltInJdk(a)) {
                            if (!equals.equalsComplicatedObjects(a, b)) {
                                    arrayList.add(a);
                                    arrayList.add(b);
                                    x.put(field.getName(), arrayList);
                            }
                        } else {
                            compare(a, b, x);
                        }
                    }
                } catch (IllegalAccessException e) {
                    log.log(Level.SEVERE, "Field might have private access", e);
                }
            }
        }
    }
}
