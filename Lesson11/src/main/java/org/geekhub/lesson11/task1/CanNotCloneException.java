package org.geekhub.lesson11.task1;

public class CanNotCloneException  extends Exception {

   public CanNotCloneException(String s)
    {
        super(s);
    }

}
