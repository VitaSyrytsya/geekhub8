package org.geekhub.lesson11.task1;
//

public interface CloneCreator {

    <T extends CanBeCloned> T clone(T object) throws  CanNotCloneException;

}
