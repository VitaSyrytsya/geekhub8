package org.geekhub.lesson11.task1;

import org.geekhub.lesson11.Ignore;
import org.geekhub.lesson11.ObjectsEquals;
import org.geekhub.lesson11.task2.ChangeSetExtractorImpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CloneCreatorImpl implements CloneCreator {

    private static final Logger log = Logger.getLogger(ChangeSetExtractorImpl.class.getName());

    private ObjectsEquals equals = new ObjectsEquals();

    @Override
    public <T extends CanBeCloned> T clone(T object) throws CanNotCloneException {

        return cloneObject(object);
    }

    private  <T> T cloneObject(T obj) throws CanNotCloneException {

        try {
            T clone = (T) obj.getClass().newInstance();
            for (Field field : obj.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.get(obj) != null &&
                        !Modifier.isFinal(field.getModifiers()) &&
                        !field.isAnnotationPresent(Ignore.class)) {

                    if (equals.fieldBuiltInJdk(field)) {
                        field.set(clone, copyingObjects(field.get(obj)));
                    } else {
                        field.set(clone, cloneObject(field.get(obj)));
                    }
                }
            }
            return clone;
        } catch (Exception e) {
           log.log(Level.SEVERE, "Can not create new instance of class"+ obj.getClass(), e);
        }
        copyingObjects(obj);
        return null;
    }

    private Object copyingObjects(Object object) throws CanNotCloneException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (IOException e) {
            throw new CanNotCloneException("Object has uncloneable fields!");
        } catch (ClassNotFoundException e) {
            log.log(Level.SEVERE, "Class "+ object+ "cannot be found in the classpath" , e);
        }
        return null;
    }
}



