package org.geekhub.lesson1.task1;


public class Main {
    public static void main(String[]args){

        int upperBoundExclusive = Integer.parseInt(args[0]);
        int[]array = PrimeNumbersCalculator.findPrimeNumbersLessThan(upperBoundExclusive);
        for (int i = array.length-1; i>=0;i--){
            System.out.println(array[i]);
        }
    }
}
