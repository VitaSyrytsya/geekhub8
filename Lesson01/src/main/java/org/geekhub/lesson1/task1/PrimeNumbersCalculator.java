package org.geekhub.lesson1.task1;


public class PrimeNumbersCalculator {

    public static int[] findPrimeNumbersLessThan(int upperBoundExclusive) {

        if (upperBoundExclusive <= 2) {
            return new int[0];
        }
        int[] intermediateArray = new int[upperBoundExclusive - 1];
        for (int index = 0, k = 1; k <= upperBoundExclusive - 1; k++, index++) {
            intermediateArray[index] = k;
        }
        int finalArrayLength = upperBoundExclusive - 2;
        for (int index = 0; index < intermediateArray.length; index++) {
            for (int value = 2; value < upperBoundExclusive - 1; value++) {
                if (intermediateArray[index] % value == 0 && intermediateArray[index] != value) {
                    finalArrayLength--;
                    intermediateArray[index] = 0;
                    break;
                }
            }
        }

        int[] finalArray = new int[finalArrayLength];

        for (int i = 0; i < finalArrayLength; i++) {
            for (int index = 1; index < intermediateArray.length; index++) {
                if (intermediateArray[index] != 0) {
                    finalArray[i] = intermediateArray[index];
                    i++;
                }
            }
        }
        return finalArray;
    }
}
