package org.geekhub.lesson1.task3;

public class MatrixUtils {

    public static int[][] createMatrix(int rowsCount, int columnsCount) {
        return new int[rowsCount][columnsCount];
    }

    public static void fillMatrix(int[][] matrix, int i) {
        for (int z= 0; z < matrix.length; z++,System.out.println()) {
            for (int j = 0; j < matrix[z].length; j++) {
                matrix[z][j] = i;
                i++;
            }
        }

    }
    public static int[] findCellsWithIntegerSquareRootValue(int[][] matrix) {
       int finalArrayLength = 0;
        for (int[] aMatrix1 : matrix) {
            for (int anAMatrix1 : aMatrix1) {
                if (Math.sqrt(anAMatrix1) % 1 == 0) {
                    System.out.println("cell has value:"+ anAMatrix1
                    + " it's root:"+ (int)Math.sqrt(anAMatrix1));
                    finalArrayLength++;
                }
            }
        }
        int[]finalArrayList = new int[finalArrayLength];
        for(int index = finalArrayLength-1; index>=0; index--){
            for (int[] aMatrix : matrix) {
                for (int anAMatrix : aMatrix) {
                    if (Math.sqrt(anAMatrix) % 1 == 0) {
                        finalArrayList[index] = anAMatrix;
                        index--;
                    }
                }
            }
        }
        return finalArrayList;
    }

    public static int sumRootsOfCellsWithIntegerSquareRoot(int[][] matrix) {
        int sum = 0;
        for (int[] aMatrix : matrix) {
            for (int anAMatrix : aMatrix) {
                if (Math.sqrt(anAMatrix) % 1 == 0) {
                    sum = sum + (int) Math.sqrt(anAMatrix);
                }
            }
        }
        return sum;
    }
}

