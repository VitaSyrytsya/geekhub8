package org.geekhub.lesson2.task4;
//import com.sun.jdi.IntegerValue;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class PolynomialSolver {


    public static double calculateDiscriminant(double x, double y, double z){
        double discriminant = Math.pow(y, 2) - 4 * x * z;
        return discriminant;
    }

    public  Optional<Set<Integer>> findRoots(int a, int b, int c)
    {
        Set<Integer> roots = new HashSet<Integer>();

        if (calculateDiscriminant(a,b,c) < 0) {
            return (Optional.empty());
        }
        if (calculateDiscriminant(a,b,c) > 0) {
            int x1 = (int) (-b + Math.sqrt(calculateDiscriminant(a,b,c))) / 2 * a;
            int x2 = (int) (-b - Math.sqrt(calculateDiscriminant(a,b,c))) / 2 * a;
            roots.add(x1);
            roots.add(x2);
            return Optional.of(roots);
        }
        if (calculateDiscriminant(a,b,c) == 0) {
            int x = -b / (2 * a);
            roots.add(x);
        }
        return Optional.of(roots);

    }

}