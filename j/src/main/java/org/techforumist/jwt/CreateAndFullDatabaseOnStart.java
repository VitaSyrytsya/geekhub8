package org.techforumist.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.techforumist.jwt.domain.AppUser;
import org.techforumist.jwt.domain.ChatMessage;
import org.techforumist.jwt.domain.Dialog;
import org.techforumist.jwt.domain.Doctor;
import org.techforumist.jwt.repository.AppUserRepository;
import org.techforumist.jwt.repository.ChatMessageRepository;
import org.techforumist.jwt.repository.DialogRepository;
import org.techforumist.jwt.repository.DoctorRepository;
import org.techforumist.jwt.service.doctor_service.DoctorService;
import java.util.ArrayList;
import java.util.List;

@Component
public class CreateAndFullDatabaseOnStart {

    private final AppUserRepository appUserRepository;
    private final DialogRepository dialogRepository;
    private final DoctorRepository doctorRepository;
    private final ChatMessageRepository chatMessageRepository;

    @Autowired
    private DoctorService doctorService;



    public CreateAndFullDatabaseOnStart(AppUserRepository appUserRepository, DialogRepository dialogRepository,ChatMessageRepository chatMessageRepository, DoctorRepository doctorRepository) {
        this.appUserRepository = appUserRepository;
        this.dialogRepository = dialogRepository;

        this.doctorRepository = doctorRepository;
        this.chatMessageRepository = chatMessageRepository;
    }

    public void onApplicationEvent() {

        AppUser appUser1 = new AppUser();
        AppUser appUser2 = new AppUser();
        AppUser appUser3 = new AppUser();
        List<String>roles = new ArrayList<>();
        List<String>adminRoles = new ArrayList<>();
        roles.add("DOCTOR");
        adminRoles.add("ADMIN");

        appUser1.setRoles(adminRoles);
        appUser1.setName("Lola");
        appUser1.setUsername("admin");
        appUser1.setPassword("admin");
        appUser1.setDialogs(new ArrayList<>());


        appUser2.setRoles(roles);
        appUser2.setName("Kate");
        appUser2.setUsername("kate");
        appUser2.setPassword("kate");
        appUser2.setDialogs(new ArrayList<>());


        appUser3.setRoles(roles);
        appUser3.setName("Andrew");
        appUser3.setUsername("andrew");
        appUser3.setPassword("andrew");
        appUser3.setPassPhrase("PassPhrase");
        appUser3.setDialogs(new ArrayList<>());


     /*   Dialog dialog = new Dialog();

        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setDate(LocalDateTime.now().toString());
        chatMessage.setSender("admin");
        chatMessage.setReceiver("kate");
        chatMessage.setContent("some message <3");
      //  chatMessage.setDialog(dialog);


       List<Dialog>dialogs = new ArrayList<>();
       List<ChatMessage>messages = new ArrayList<>();
       messages.add(chatMessage);
       dialog.setMessages(messages);
       ChatMessage lastMessage = new ChatMessage();
       //lastMessage.setContent("hello");
     //  chatMessageRepository.save(lastMessage);
     //  dialog.setLastMessage(chatMessage);
dialogs.add(dialog);
        appUser1.setDialogs(dialogs);
        appUser2.setDialogs(dialogs);*/

Doctor doctor = new Doctor();
doctor.setSurname("Hauphman");
doctor.setExperience("1 year");
doctor.setImgSrc("src");
doctor.setUniversity("university");
appUser2.setDoctor(doctor);

       appUserRepository.save(appUser3);
        appUserRepository.save(appUser2);
        appUserRepository.save(appUser1);




     System.out.println("All"+doctorRepository.findAll());





























    }
}
