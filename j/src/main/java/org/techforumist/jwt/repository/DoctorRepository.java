package org.techforumist.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.techforumist.jwt.domain.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    public Doctor findByUser(Long id);

}
