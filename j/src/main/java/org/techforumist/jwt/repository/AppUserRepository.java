package org.techforumist.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.techforumist.jwt.domain.AppUser;

import java.util.List;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
	public AppUser findOneByUsername(String username);
	public List<AppUser>findUsersByRoles(List<String>roles);
	public AppUser findUserByPassPhrase(String passphrase);
}
