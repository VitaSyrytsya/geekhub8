package org.techforumist.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.techforumist.jwt.domain.AppUser;
import org.techforumist.jwt.domain.ChatMessage;
import org.techforumist.jwt.domain.Dialog;
import java.util.List;

@Repository
public interface DialogRepository extends JpaRepository<Dialog, Long> {
   // public Dialog findByParticipants(List<AppUser> participants);
  //  public Dialog findByLastMessage(ChatMessage lastMessage);
}
