package org.techforumist.jwt.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.techforumist.jwt.domain.ChatMessage;
import org.techforumist.jwt.domain.Dialog;
import org.techforumist.jwt.service.chat_message_service.MessageService;
import org.techforumist.jwt.service.dialog_service.DialogService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MessagingController {

    private final SimpMessagingTemplate messagingTemplate;
    private final MessageService messageService;
    private final DialogService dialogService;

    @Autowired
    public MessagingController(SimpMessagingTemplate messagingTemplate, MessageService messageService, DialogService dialogService) {
        this.messagingTemplate = messagingTemplate;
        this.messageService = messageService;
        this.dialogService = dialogService;
    }

    @MessageMapping("/chat.sendMessage")
    public void sendMessage(@Payload ChatMessage chatMessage) {

        if(chatMessage.getSender().compareTo(chatMessage.getReceiver())>0){
            messagingTemplate.convertAndSend("/topic/"+ chatMessage.getReceiver()+ "/"+ chatMessage.getSender(), chatMessage);

        }else{
            messagingTemplate.convertAndSend("/topic/"+ chatMessage.getSender()+ "/"+ chatMessage.getReceiver(), chatMessage);
            System.out.println("Sender"+chatMessage.getSender());
        }

    }

    @PostMapping("/saveMessage/{id}")
    public void saveMessage(@RequestBody ChatMessage chatMessage, @PathVariable Long id) {
        Dialog dialog = dialogService.findDialogById(id);
        List<ChatMessage>messages = dialogService.findDialogById(id).getMessages();
        messages.add(chatMessage);
        dialog.setMessages(messages);
        dialogService.save(dialog);


    }
}
