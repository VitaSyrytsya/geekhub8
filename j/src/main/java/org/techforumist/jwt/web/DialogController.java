package org.techforumist.jwt.web;

import io.jsonwebtoken.Claims;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.techforumist.jwt.domain.ChatMessage;
import org.techforumist.jwt.domain.Dialog;
import org.techforumist.jwt.repository.AppUserRepository;
import org.techforumist.jwt.service.dialog_service.DialogService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class DialogController {

    private final DialogService dialogService;
    private final AppUserRepository appUserRepository;


    public DialogController(DialogService dialogService, AppUserRepository appUserRepository) {
        this.dialogService = dialogService;
        this.appUserRepository = appUserRepository;

    }

    @GetMapping(value = "/dialogs")
    public List<Dialog> dialogs(HttpServletRequest request) {
        Claims claims = (Claims) request.getAttribute("claims");
        String currentUsername = claims.getSubject();
        return appUserRepository.findOneByUsername(currentUsername).getDialogs();

    }

    @GetMapping(value = "/getMessages/{id}")
    public List<ChatMessage> getMessagesFromDialog(@PathVariable Long id) {

        return dialogService.findDialogById(id).getMessages();
    }
}

