package org.techforumist.jwt.service.chat_message_service;

import org.techforumist.jwt.domain.ChatMessage;

public interface MessageService {
  void save(ChatMessage chatMessage);

}
