package org.techforumist.jwt.service.doctor_service;

import org.springframework.stereotype.Service;
import org.techforumist.jwt.domain.Doctor;
import org.techforumist.jwt.repository.DoctorRepository;

import java.util.List;

@Service
public class DoctorServiceImpl implements DoctorService {
    private final DoctorRepository doctorRepository;

    public DoctorServiceImpl(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public void save(Doctor doctor) {
        doctorRepository.save(doctor);
    }

    @Override
    public List<Doctor> findAll() {
      return doctorRepository.findAll();
    }

    @Override
    public Doctor findDoctorByUserId(Long id) {
        return doctorRepository.findByUser(id);
    }
}
