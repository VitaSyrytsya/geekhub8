package org.techforumist.jwt.service.user_service;


import org.springframework.stereotype.Service;
import org.techforumist.jwt.domain.AppUser;

import java.util.List;

public interface UserService {
    AppUser save(AppUser user);
    void deleteUser(AppUser user);
    List<AppUser>findUsersByRoles(List<String>roles);
    List<AppUser>findAllUsers();
    AppUser findUserByUsername(String username);
    AppUser findUserById(Long id);
    AppUser findUserByPassPhrase(String passphrase);


}
