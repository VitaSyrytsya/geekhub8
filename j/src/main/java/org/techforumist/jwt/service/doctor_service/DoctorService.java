package org.techforumist.jwt.service.doctor_service;

import org.techforumist.jwt.domain.Doctor;

import java.util.List;

public interface DoctorService {

     void save(Doctor doctor);

     List<Doctor>findAll();

     Doctor findDoctorByUserId(Long id);
}
