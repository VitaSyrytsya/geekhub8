package org.techforumist.jwt.service.dialog_service;

import org.techforumist.jwt.domain.Dialog;
import java.util.List;

public interface DialogService {

     List<Dialog>getDialogsOrderedByLastMessageDateDescending();
     void save(Dialog dialog);
     Dialog findDialogById(Long id);

}
