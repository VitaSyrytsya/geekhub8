package org.techforumist.jwt.service.user_service;


import org.springframework.stereotype.Service;
import org.techforumist.jwt.domain.AppUser;
import org.techforumist.jwt.repository.AppUserRepository;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final AppUserRepository appUserRepository;

    public UserServiceImpl(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public AppUser save(AppUser user) {
        appUserRepository.save(user);
        return user;
    }

    @Override
    public void deleteUser(AppUser user) {
        appUserRepository.delete(user);
    }

    @Override
    public List<AppUser> findUsersByRoles(List<String> roles) {
        return appUserRepository.findUsersByRoles(roles);
    }

    @Override
    public List<AppUser> findAllUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public AppUser findUserByUsername(String username) {
        return appUserRepository.findOneByUsername(username);
    }

    @Override
    public AppUser findUserById(Long id) {
        return  appUserRepository.findOne(id);
    }

    @Override
    public AppUser findUserByPassPhrase(String passphrase) {
       return appUserRepository.findUserByPassPhrase(passphrase);
    }
}
