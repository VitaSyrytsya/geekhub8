package org.techforumist.jwt.service.dialog_service;

import org.springframework.stereotype.Service;
import org.techforumist.jwt.domain.Dialog;
import org.techforumist.jwt.repository.DialogRepository;

import java.util.List;

@Service
public class DialogServiceImpl implements DialogService {

    private final DialogRepository dialogRepository;

    public DialogServiceImpl(DialogRepository dialogRepository) {
        this.dialogRepository = dialogRepository;
    }

    @Override
    public List<Dialog> getDialogsOrderedByLastMessageDateDescending() {
        return dialogRepository.findAll();
    }

    @Override
    public void save(Dialog dialog) {
        dialogRepository.save(dialog);
    }

    @Override
    public Dialog findDialogById(Long id) {
        return dialogRepository.findOne(id);
    }
}
