angular.module('JWTDemoApp')
	.controller('HomeController', function($http, $scope, $cookies, $state) {
		$http({
			method: 'GET',
			url: 'getUser',
			headers: {
				'Authorization': 'Bearer ' + $cookies.get('token')
			}
		}).success(function(res) {
			$scope.user = res
		}).error(function() {
			$state.go('login');
		});
});
