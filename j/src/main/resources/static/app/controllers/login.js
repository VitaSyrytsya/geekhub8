angular.module('JWTDemoApp')

.controller('LoginController', function($http, $scope, $state,$cookies, AuthService, $rootScope) {
	$scope.login = function() {
		$http({
			url: 'authenticate',
			method: "POST",
			params: {
				username: $scope.username,
				password: $scope.password,
				rememberMe: $scope.rememberMe
			}
		}).success(function () {
			$rootScope.$broadcast('LoginSuccessful');
			$state.go('home');

		}).error(function (error) {
			$scope.password = null;
			$scope.message = error;
		});
	};
});

