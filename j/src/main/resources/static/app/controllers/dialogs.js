angular.module('JWTDemoApp')
    .controller('DialogController', function( $scope, $cookies, $http, $state,  $rootScope) {

        $scope.selectedRow = null;

        $http({
            method: 'GET',
            url: 'dialogs',
            headers: {
                'Authorization': 'Bearer ' + $cookies.get('token')
            }
        }).success(function (response) {
            $scope.dialogs = response;
        }).error(function () {
            $cookies.remove('token');

        });

        $scope.curPage = 0;
        $scope.pageSize = 7;
        $scope.numberOfPages = function() {
            return Math.ceil($scope.dialogs.length / $scope.pageSize);
        };

        $scope.setClickedRow = function(dialog){
            $rootScope.$broadcast('LoginSuccessful');
            $state.go('messages', {dialog:dialog});
        }

    });

angular.module('JWTDemoApp').filter('startPagination', function() {
    return function(input, start) {
        start = +start;
        return input.slice(start);
    };

});