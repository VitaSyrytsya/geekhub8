<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="add" uri="/WEB-INF/standart tag.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Home</title>
    <style>
        .classlink {
            margin: 0;
            border: 0;
            background: none;
            overflow: visible;
            color: #d935ff;
            cursor: pointer;
            text-decoration: underline;
            font-size: large;
        }
    </style>
</head>
<body>
<add:userTag/>
<add:adminTag>
    <header><h1>Choose action:</h1></header>
    <form action="/users/home" method="post" >
        <input type="hidden" name=type value="all">
        <input type="submit" value="get all users" class="classlink">
    </form>
    <form action="/users/home" method="post">
        <input type="hidden" name="type" value="byLogin">
        <input type="submit" value="get user by login" class="classlink">
    </form>
</add:adminTag>
</body>
</html>

