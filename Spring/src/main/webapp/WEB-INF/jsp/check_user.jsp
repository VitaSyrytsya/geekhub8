<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<html>
<head>
    <title>Log in</title>
</head>
<body>

<spring:form modelAttribute="userFromServer" method="post" action="/users/check">
    <spring:input path="login" />
    <spring:password path="password" />
    <spring:button>check user</spring:button>
</spring:form>
</body>
</html>