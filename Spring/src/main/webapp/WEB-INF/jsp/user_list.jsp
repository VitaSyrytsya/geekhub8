<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="add" uri="/WEB-INF/standart tag.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Users</title>
    <style>
        .classlink {
            margin: 0;
            border: 0;
            background: none;
            overflow: visible;
            color: #d935ff;
            cursor: pointer;
            text-decoration: underline;
            font-size: medium;
        }
    </style>
</head>
<body>
<table border="1" cellspacing="0" cellpadding="15" width="40%" height="40">
    <c:forEach var="user" items="${pageContext.session.getAttribute('users')}">
        <tr>
            <td>${user}</td>
            <c:choose>
                <c:when test="${user.getId() == 1}">
                    <td></td>
                </c:when>
                <c:otherwise>
                    <form action="/users/action"  method="post">
                        <input type="hidden" name="action" value="DELETE">
                        <input type="hidden" name="id" value="${user.getId()}">
                        <input type="hidden" name="login" value="${user.getLogin()}">
                        <input type="hidden" name="firstName" value="${user.getFirstName()}">
                        <input type="hidden" name="password" value="${user.getPassword()}">
                        <input type="hidden" name="isAdmin" value="${user.isAdmin()}">
                        <td><input type="submit" value="delete" class="classlink"></td>
                    </form>
            </c:otherwise>
            </c:choose>
            <form action="/users/action"  method="post">
                <input type="hidden" name="action" value="UPDATE">
                <input type="hidden" name="login" value="${user.getLogin()}">
                <td><input type="submit" value="update" class="classlink"></td>
            </form>
        </tr>
    </c:forEach>
</table>
<br>
<br>
<h3>Register new user: </h3>
<table border="0" cellspacing="0" cellpadding="10" width="10%" height="10">
    <form action="/users/action" method="post">
        <input type="hidden" name="action" value="ADD">
        <tr>Login: <input type="text" name="login"></tr>
        <br><br>
        <tr>First name: <input type="text" name="firstName"></tr>
        <br><br>
        <tr>Last name*: <input type="text" name="lastName"></tr>
        <br><br>
        <tr>Password: <input type=password name="password"></tr>
        <br><br>
        <tr><input type="radio" name="isAdmin" value="true">ADMIN</tr>
        <br><br>
        <tr><input type="radio" name="isAdmin" value="false">NOT ADMIN</tr>
        <br><br>
        <tr><input type="submit" value="Register"></tr>
        <br><br>
    </form>
</table>
</body>
</html>
