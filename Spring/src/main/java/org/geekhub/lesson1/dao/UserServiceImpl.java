package org.geekhub.lesson1.dao;

import org.geekhub.lesson1.ConnectionPool;
import org.geekhub.lesson1.EncryptPassword;
import org.geekhub.lesson1.InvalidUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.UserCredentialsDataSourceAdapter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Service
public class UserServiceImpl implements UserService {


    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    @Autowired
    private UserDao userDao;
    @Autowired
    private EncryptPassword encryptor;

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM USERS";
        Connection connection = connectionPool.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(buildUpUser(resultSet));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return users;
    }



    @Override
    public Optional<User> find(String login) {
        User userToFind = null;
        for (User user : getAllUsers()) {
            if (login.equals(user.getLogin())) {
                userToFind = user;
            }
        }
        return Optional.ofNullable(userToFind);
    }

    @Override
    public void add(User user) {
        checkIfUserIsValid(user);


        String SQL_INSERT = "INSERT INTO Users (login, firstName, lastName, password, isAdmin) VALUES (?, ?, ?, ?, ?);";
       if(find(user.getLogin()).isPresent()){
           throw new InvalidUserException("User with such login already exists");
       }

        Object[] values = {
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                encryptor.encrypt(user.getPassword()),
                user.isAdmin()
        };
        try (
                Connection connection = connectionPool.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_INSERT, true, values)
        ) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }


    @Override
    public void delete(User user) {
        String SQL_DELETE = "DELETE FROM Users WHERE Login =?";
        Object[] values = {
                user.getLogin()
        };
        try (
                Connection connection = connectionPool.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_DELETE, false, values)
        ) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(User user) {
        checkIfUserIsValid(user);
        String SQL_UPDATE = " UPDATE Users SET firstName = ?, lastName = ?, password = ?, isAdmin = ? WHERE login ='" + user.getLogin() + "'";

        Object[] values = {
                user.getFirstName(),
                user.getLastName(),
                encryptor.encrypt(user.getPassword()),
                user.isAdmin()
        };
        try (
                Connection connection = connectionPool.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_UPDATE, false, values)
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new RuntimeException("Updating user failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    private void checkIfUserIsValid(User user) {
        if (user.getFirstName().isEmpty() || user.getLogin().isEmpty() ||
                user.getPassword().isEmpty()) {
            throw new InvalidUserException("User has got empty fields!");
        }
    }

    private static PreparedStatement prepareStatement
            (Connection connection, String sql, boolean returnGeneratedKeys, Object... values)
            throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sql,
                returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        setValues(statement, values);
        return statement;
    }

    private static void setValues(PreparedStatement statement, Object... values)
            throws SQLException {
        for (int i = 0; i < values.length; i++) {
            statement.setObject(i + 1, values[i]);
        }
    }

    private static User buildUpUser(ResultSet resultSet) throws SQLException {
        User user;
        int id = resultSet.getInt("id");
        String login = resultSet.getString("login");
        String firstName = resultSet.getString("firstName");
        String lastName = resultSet.getString("lastName");
        String password = resultSet.getString("password");
        boolean isAdmin = resultSet.getBoolean("isAdmin");
        user = new User(login, password);
        user.setAdmin(isAdmin);
        user.setFirstName(firstName);
        user.setLogin(login);
        user.setId(id);
        user.setLastName(lastName);
        return user;
    }


}
