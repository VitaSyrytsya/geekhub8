package org.geekhub.lesson1.tags;

import org.geekhub.lesson1.dao.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.TagSupport;

public class AdminTag extends TagSupport {
    @Override
    public int doStartTag() {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute("user");
        if(currentUser.isAdmin()) {
            session.setAttribute("adminUsers", session.getAttribute("users"));
            session.setAttribute("type", "type");
        }else{
            return SKIP_BODY;
        }
        return 1;
    }

}
