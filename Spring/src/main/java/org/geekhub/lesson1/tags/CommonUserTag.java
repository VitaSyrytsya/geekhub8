package org.geekhub.lesson1.tags;

import org.geekhub.lesson1.dao.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

public class CommonUserTag extends TagSupport {
    @Override
    public int doStartTag() {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        HttpSession session = request.getSession();
        List<User> users = (List<User>) session.getAttribute("users");
        User currentUser = (User) session.getAttribute("user");
        if (!currentUser.isAdmin()) {
            for(User user: users){
                try {
                    pageContext.getOut().print(user);
                    pageContext.getOut().print("<br>");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return SKIP_BODY;
    }

}
