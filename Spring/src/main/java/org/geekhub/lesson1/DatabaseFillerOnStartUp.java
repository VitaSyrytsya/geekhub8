package org.geekhub.lesson1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

@Component
public class DatabaseFillerOnStartUp implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private EncryptPassword encryptor;

    private String DB_URL = getProperty("DB_URL");
    private String login = getProperty("login");;
    private String password = getProperty("password");
    private String DRIVER_NAME =getProperty("DRIVER_NAME");


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try (Connection connection = DriverManager.getConnection(DB_URL, login, password);
             Statement pst = connection.createStatement()) {

            String table = createTableForDatabase();
            pst.executeUpdate(table);
            ResultSet rs = pst.executeQuery("SELECT * from Users ");
            boolean empty = true;
            while (rs.next()) {
                empty = false;
            }
            if (empty) {
                String password = encryptor.encrypt("qwerty");
                String insert = "INSERT  INTO Users(id, login, firstName, lastName, password, isAdmin) " +
                        "values(" + 1 + ",'Carrie','Carrie','Bradshaw', '" + password+ "', true)";
                pst.executeUpdate(insert);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private String createTableForDatabase() {
        return "CREATE TABLE IF NOT EXISTS USERS " +
                "(id int auto_increment, " +
                " login VARCHAR(255), " +
                " firstName VARCHAR(255), " +
                " lastName VARCHAR(255), " +
                " password VARCHAR(128), " +
                " isAdmin boolean, " +
                " PRIMARY KEY ( id ))";
    }

    private static String getProperty(String propertyName){
        Properties prop = new Properties();
        try {
            prop.load(DatabaseFillerOnStartUp.class.getClassLoader().getResourceAsStream("database.properties"));
            return prop.getProperty(propertyName);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
