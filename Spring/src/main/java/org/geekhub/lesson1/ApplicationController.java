package org.geekhub.lesson1;

import org.geekhub.lesson1.action.Action;
import org.geekhub.lesson1.action.ActionFactory;
import org.geekhub.lesson1.action.ActionType;
import org.geekhub.lesson1.dao.User;
import org.geekhub.lesson1.dao.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/users")
public class ApplicationController {

    @Autowired
    private UserService userService;
    @Autowired
    private EncryptPassword encryptor;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody
    List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @RequestMapping(value = "/validate", method = RequestMethod.GET)
    public ModelAndView validateUser() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userFromServer", new User());
        modelAndView.setViewName("check_user");
        return modelAndView;
    }

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public String checkUser(@ModelAttribute("userFromServer") User user, HttpSession session) {
        User userToFind = userService.find(user.getLogin()).get();
        if (userToFind.getPassword().equals(encryptor.encrypt((user.getPassword())))) {
            session.setAttribute("user", userToFind);
            session.setAttribute("users", userService.getAllUsers());
            return "home_page";
        }
        throw new RuntimeException("User is invalid!");
    }

    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public String redirectHomePage(HttpServletRequest request) {
        if (request.getParameter("type").equals("all")) {
            return "user_list";
        }
        return "login_page";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String getUserLogin(HttpServletRequest request) {
        String login = request.getParameter("login");
        if (userService.find(login).isPresent()) {
            ArrayList users = new ArrayList(Collections.singleton(userService.find(login).get()));
            request.getSession().setAttribute("users", users);
            return "user_list";
        }
        throw new RuntimeException("User not found!");
    }

    @RequestMapping(value = "/action", method = RequestMethod.GET)
    public ModelAndView modelAndView() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userToChange", new User());
        modelAndView.setViewName("check_user");
        return modelAndView;
    }


    @RequestMapping(value = "/action", method = RequestMethod.POST)
    public String workWithUser(HttpServletRequest request) {
        String action = request.getParameter("action");
        String login = request.getParameter("login");
        if (action.equals("UPDATE")) {
            request.getSession().setAttribute("login", login);
            return "update_page";

        }
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String password = request.getParameter("password");
        boolean isAdmin = Boolean.parseBoolean(request.getParameter("isAdmin"));
        User user = new User(login, password);
        user.setFirstName(firstName);
        user.setAdmin(isAdmin);
        user.setLastName(lastName);

        ActionType actionType = ActionType.recognize(action);
        Action act = ActionFactory.createAction(actionType);
        act.apply(userService, user);
        request.getSession().setAttribute("users", userService.getAllUsers());

        return "home_page";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateUser(HttpServletRequest request) {
        String action = request.getParameter("action");
        String login = String.valueOf(request.getSession().getAttribute("login"));
        String password = request.getParameter("password");
        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");
        boolean isAdmin = Boolean.parseBoolean(request.getParameter("isAdmin"));
        User user = new User(login, password);
        user.setLastName(lastName);
        user.setAdmin(isAdmin);
        user.setFirstName(firstName);

        ActionType actionType = ActionType.recognize(action);
        Action act = ActionFactory.createAction(actionType);
        act.apply(userService, user);

        request.getSession().setAttribute("users", userService.getAllUsers());
        return "home_page";
    }
}