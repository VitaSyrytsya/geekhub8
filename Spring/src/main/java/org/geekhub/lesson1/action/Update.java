package org.geekhub.lesson1.action;

import org.geekhub.lesson1.dao.User;
import org.geekhub.lesson1.dao.UserService;


public class Update implements Action {

    @Override
    public void apply(UserService userService, User user) {
        userService.update(user);
    }
}
