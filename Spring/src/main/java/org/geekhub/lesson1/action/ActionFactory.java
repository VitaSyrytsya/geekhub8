package org.geekhub.lesson1.action;

import java.util.Map;

import static java.util.Objects.nonNull;

public class ActionFactory {
    private static final Map<ActionType, Action> actions = Map.of(
            ActionType.ADD, new Add(),
            ActionType.DELETE, new Delete(),
            ActionType.UPDATE, new Update()
    );

    public static Action createAction(ActionType actionType) {
        final Action action = actions.get(actionType);

        if (nonNull(action)) {
            return action;
        }

        throw new IllegalArgumentException("Action is not supported: " + actionType);
    }

}
