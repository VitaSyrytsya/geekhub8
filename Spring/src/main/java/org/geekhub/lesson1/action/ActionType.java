package org.geekhub.lesson1.action;

public enum ActionType {
    ADD, DELETE, UPDATE;

    public static ActionType recognize(String actionString) {
        for (ActionType action : values()) {
            if (action.name().equals(actionString)) {
                return action;
            }
        }
        throw new IllegalArgumentException("Action not recognized: " + actionString);
    }
}
