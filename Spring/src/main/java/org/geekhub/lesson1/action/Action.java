package org.geekhub.lesson1.action;

import org.geekhub.lesson1.dao.User;
import org.geekhub.lesson1.dao.UserService;


public interface Action {

    void apply(UserService userService, User user);

}
