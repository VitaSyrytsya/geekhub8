package org.geekhub.lesson1;

import org.geekhub.lesson1.dao.UserDao;
import org.geekhub.lesson1.dao.UserService;
import org.geekhub.lesson1.dao.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@ComponentScan({"org.geekhub.lesson1"})
@Configuration
@EnableWebMvc
public class JavaConfiguration implements WebMvcConfigurer {

    private final RequestLoggingInterceptor requestLoggingInterceptor;

    private final JsonMessageConverter jsonMessageConverter;


    @Bean(name = "databaseFiller")
    public DatabaseFillerOnStartUp getDatabase() {
        return new DatabaseFillerOnStartUp();
    }

    @Bean(name = "userServiceImpl")
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean(name = "passwordEncryptor")
    public EncryptPassword encryptPassword() {
        return new EncryptPassword();
    }


    @Autowired
    public JavaConfiguration(RequestLoggingInterceptor requestLoggingInterceptor, JsonMessageConverter jsonMessageConverter) {
        this.requestLoggingInterceptor = requestLoggingInterceptor;
        this.jsonMessageConverter = jsonMessageConverter;
    }


    @Override
    public void configureMessageConverters(
            List<HttpMessageConverter<?>> converters) {
        converters.add(jsonMessageConverter);

    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestLoggingInterceptor)
                .addPathPatterns("/**/log-incoming-request/**/");
    }


}

