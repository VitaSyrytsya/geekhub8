package org.geekhub.lesson1;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


import javax.servlet.http.HttpServletRequest;
import java.util.logging.Level;
import java.util.logging.Logger;


@ControllerAdvice
@EnableWebMvc
public class GlobalExceptionHandler {
    private final Logger logger = Logger.getLogger(GlobalExceptionHandler.class.getName());
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)


    public ModelAndView handleError(HttpServletRequest request, RuntimeException exception) {
        logger.log(Level.SEVERE," ERROR DURING QUERY EXECUTION: ", exception);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", "ERROR DURING QUERY EXECUTION: " + exception.getMessage());
        return mav;

    }

}
