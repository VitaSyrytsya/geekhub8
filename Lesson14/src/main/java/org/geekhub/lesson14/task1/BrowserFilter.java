package org.geekhub.lesson14.task1;

import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(filterName = "browser")
public class BrowserFilter implements Filter {

     private boolean isBrowserValid(ServletRequest request){
         UserAgent userAgent = UserAgent.parseUserAgentString(((HttpServletRequest) request)
                 .getHeader("User-Agent"));
         String browserName = userAgent.getBrowser().getName();
         return browserName.equals("Chrome") || browserName.equals("Microsoft Edge");
     }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
         if (isBrowserValid(request)) {
            chain.doFilter(request, response);
        }else{
            throw new BrowserUnsupportedException("Your browser does not support this operation!");
        }
    }
}
