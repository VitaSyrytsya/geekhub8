package org.geekhub.lesson14.task1;

import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.sql.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(urlPatterns = {"/home"}, name = "homeServlet")
public class HomeServlet extends HttpServlet {
    private String attribute = "";
    private String key = "";
    private String action = "";
    private String valueToUpdate = "";

    private final static Logger log = Logger.getLogger(HomeServlet.class.getName());
    private Map<String, LinkedList<String>> animals = new HashMap<>();
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private Statement pst;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        LinkedList<String> cats = new LinkedList<>(Arrays.asList("k"));

        animals.put("k", cats);

        if (getBrowserNameFromRequest(req).equals("Chrome")) {
            writeTableIntoDatabase();
            writeAttributesIntoDatabase(animals, pst);
        }
        session.setAttribute("animals", animals);
        req.getRequestDispatcher("/some/jsp-file.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Map<String, LinkedList<String>> animals = (Map<String, LinkedList<String>>) session.getAttribute("animals");
        if (req.getParameter("action").equals("Invalidate")) {
            session.invalidate();
        } else if (req.getParameter("action").equals("Add") || req.getParameter("action").equals("Update")) {
            session.setAttribute("nameToUpdate", req.getParameter("key"));
            session.setAttribute("valueToUpdate", req.getParameter("attribute"));
            session.setAttribute("action", req.getParameter("action"));
            valueToUpdate = req.getParameter("attribute");
            resp.sendRedirect("/some/other.jsp");
        } else {
            action = req.getParameter("action");
            key = req.getParameter("key");
            attribute = req.getParameter("attribute");
            transformMapDependingOnInputAttributes(animals);
            session.removeAttribute("animals");
            session.setAttribute("animals", animals);
            if (getBrowserNameFromRequest(req).equals("Chrome")) {
                try {
                    pst.executeUpdate("DELETE FROM animals");
                } catch (SQLException e) {
                    log.log(Level.SEVERE, e.getMessage());
                    throw new RuntimeException(e.getMessage());
                }
                writeAttributesIntoDatabase(animals, pst);
                String select = "SELECT id, keyy, valuee FROM Animals";
                try {
                    ResultSet rs = pst.executeQuery(select);
                    while(rs.next()){
                        int i = rs.getInt("id");
                        String keyy = rs.getString("keyy");
                        String valuu = rs.getString("valuee");
                        log.info(i+ "");
                        log.info(keyy+ " ");
                        log.info(valuu + " ");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            resp.sendRedirect("/some/jsp-file.jsp");
        }
    }

    protected void transformMapDependingOnInputAttributes(Map<String, LinkedList<String>> map) throws InvalidObjectException {
        switch (action) {
            case "Remove":
                if (map.get(key).size() == 1) {
                    map.remove(key);
                } else {
                    map.get(key).remove(attribute);
                }
                break;
            case "add":
                if (key.isEmpty() || attribute.isEmpty()) {
                    throw new InvalidObjectException("Please, input values!");
                }
                Set<String> keys = map.keySet();
                if (listContainsKey(keys, key)) {
                    map.get(key).add(attribute);
                } else {
                    LinkedList<String> newList = new LinkedList<>(Collections.singleton(attribute));
                    map.put(key, newList);
                }
                break;
            case "update":
                if (key.isEmpty() || attribute.isEmpty()) {
                    throw new InvalidObjectException("Please, input values!");
                }
                map.get(key).remove(valueToUpdate);
                map.get(key).add(attribute);
                break;
        }
    }

    private boolean listContainsKey(Set<String> list, String key) {
        for (String l : list) {
            if (key.equals(l)) {
                return true;
            }
        }
        return false;
    }

    protected void writeAttributesIntoDatabase(Map<String, LinkedList<String>> map, Statement pst) {
        int id = 1;
        for (String key : map.keySet()) {
            for (String value : map.get(key)) {
                String sql = "insert  into Animals (id, keyy, valuee) values(" + id + ",'" + key + "' , '" + value + "')";
                try {
                    pst.executeUpdate(sql);
                    id++;
                } catch (SQLException e) {
                    log.log(Level.SEVERE, e.getMessage());
                    throw new RuntimeException(e.getMessage());
                }
            }

        }
    }

    private String getDatabasePropertyFromConfFile(String property) {
        Properties properties = new Properties();
        try (ByteArrayInputStream fis = (ByteArrayInputStream) this.getClass().getClassLoader().
                getResourceAsStream("application.conf")
        ) {
            properties.loadFromXML(fis);
        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage());
            throw new RuntimeException(e);
        }
        return properties.getProperty(property);
    }

    private String getBrowserNameFromRequest(HttpServletRequest request) {
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        return userAgent.getBrowser().getName();
    }

    private void writeTableIntoDatabase() {
        try {
            String localhost = getDatabasePropertyFromConfFile("host");
            String USER = getDatabasePropertyFromConfFile("USER");
            String PASS = getDatabasePropertyFromConfFile("PASS");
            String DatabaseName = getDatabasePropertyFromConfFile("DatabaseName");
            String DB_URL = "jdbc:h2:" + "~/" + DatabaseName;
            Class.forName(JDBC_DRIVER);
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            pst = conn.createStatement();
            String table = "CREATE TABLE   ANIMALS " +
                    "(id INTEGER not NULL, " +
                    " keyy VARCHAR(255), " +
                    " valuee VARCHAR(255), " +
                    " PRIMARY KEY ( id ))";
            pst.executeUpdate(table);
        } catch (SQLException | ClassNotFoundException e) {
            log.log(Level.SEVERE, e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }
}

