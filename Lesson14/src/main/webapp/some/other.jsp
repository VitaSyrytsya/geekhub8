<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <style>
        .classlink {
            margin: 0;
            border: 0;
            background: none;
            overflow: visible;
            color: blue;
            cursor: pointer;
            text-decoration: underline;
        }
        #update {
            width: 10em;  height: 2em;
        }
    </style>

</head>
<body>
<table border="1" cellspacing="0" cellpadding="15" width="40%" height="40">
    <tr>
        <td><h3>Name</h3></td>
        <td><h3>Value</h3></td>
        <td><h3>Action</h3></td>
        <td></td>

    </tr>
    <c:forEach var="animalType" items="${animals.keySet()}">
        <td>${animalType}</td>
        <c:forEach var="an" items="${animals.get(animalType)}">
            <td> ${an}</td>

            <form action="/home" && action="attribute" && action="key" method="post">
                <td><select name="action" >
                    <option label value=1 selected></option>
                    <option value="Update">Update</option>
                    <option value="Invalidate">Invalidate</option>
                    <option value="Remove">Remove</option>
                    <option value="Add">Add</option>
                </select></td>
                <td>
                    <input type="hidden" name="attribute" value="${an}">
                    <input type="hidden" name="key" value="${animalType}">
                    <input type="submit" value="confirm" class="classlink" >
            </form>

            </td>
            <tr>
            <c:choose>
                <c:when test="${an.equals(valueToUpdate)&&
                animalType.equals(nameToUpdate)&& action.equals('Add')}">
                    <form action="/home" method="post">
                        <td><input type="text" name="key" size="9"></td>
                        <td><input type="text" name="attribute" size="9"></td>
                        <input type="hidden" name="action" value="add">
                        <th colspan="2"> <input type="submit" value="Submit" id = "update"></th>

                    </form>
                    </tr>
                </c:when>
            </c:choose>
            <c:choose>
                <c:when test="${an.equals(valueToUpdate)&&
                animalType.equals(nameToUpdate)&& action.equals('Update')}">
                    <form action="/home" method="post">
                        <td><input type="text" name="key" size="9"></td>
                        <td><input type="text" name="attribute" size="9"></td>
                        <input type="hidden" name="action" value="update">
                        <th colspan="2"> <input type="submit" value="Submit" id = "uupdate"></th>
                    </form>
                    </tr>
                </c:when>
            </c:choose>
            <c:choose>
                <c:when test="${an.equals(animals.get(animalType).getLast())}">
                </c:when>
                <c:otherwise>
                    <td></td>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        </tr>
    </c:forEach>
</table>
</body>
</html>


