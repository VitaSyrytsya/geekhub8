package org.geekhub.lesson14.task1;


import org.testng.Assert;
import org.testng.annotations.Test;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.atLeast;

public class HomeServletTest {

    @Test
    public void checkIfDatabaseCanBeConnectedIfNotExists() throws ClassNotFoundException, SQLException {
        String User = "login";
        String PASS = "password";
        String url = "jdbc:h2:mem://localhost:5432./DatabaseName";

        Class.forName("org.h2.Driver");
        Connection connection = DriverManager.getConnection(url, User, PASS);

        Assert.assertFalse(connection.isClosed());
        connection.close();
    }


    @Test
    public void checkIfDataWouldBeWrittenToDatabase() throws ClassNotFoundException, SQLException {
        HomeServlet servlet = new HomeServlet();
        String User = "login";
        String PASS = "password";
        String url = "jdbc:h2:mem://localhost:5432./DatabaseName";
        LinkedList<String> mouses = new LinkedList<>(Arrays.asList("Jerry", "Micky"));
        Map<String, LinkedList<String>> animals = new HashMap<>();

        animals.put("Mouse", mouses);
        Class.forName("org.h2.Driver");
        Connection connection = DriverManager.getConnection(url, User, PASS);
        Statement statement = connection.createStatement();
        String table = "CREATE TABLE   ANIMALS " +
                "(id INTEGER not NULL, " +
                " keyy VARCHAR(255), " +
                " valuee VARCHAR(255), " +
                " PRIMARY KEY ( id ))";
        statement.executeUpdate(table);
        servlet.writeAttributesIntoDatabase(animals, statement);
        ResultSet result = statement.executeQuery("SELECT id, keyy , valuee FROM Animals");
        result.next();
        String resultValue = result.getString("valuee");
        String resultKey = result.getString("keyy");
        int id = result.getInt("id");
        result.next();
        String resultValue1 = result.getString("valuee");
        String resultKey1 = result.getString("keyy");
        int id1 = result.getInt("id");

        Assert.assertEquals(resultValue, "Jerry");
        Assert.assertEquals(resultValue1, "Micky");
        Assert.assertEquals(resultKey, "Mouse");
        Assert.assertEquals(resultKey1, "Mouse");
        Assert.assertEquals(id, 1);
        Assert.assertEquals(id1, 2);
        statement.executeUpdate("DROP TABLE Animals");
        statement.close();
        connection.close();
    }

    @Test
    public void checkIfAttributeWouldBeAddedIntoMap() throws IOException {
        HomeServlet homeServlet = new HomeServlet();
        HttpSession session = mock(HttpSession.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        LinkedList<String> mouses = new LinkedList<>(Arrays.asList("Jerry", "Micky"));
        Map<String, LinkedList<String>> animals = new HashMap<>();

        animals.put("Mouse", mouses);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("animals")).thenReturn(animals);
        when(request.getParameter("action")).thenReturn("add");
        when(request.getParameter("key")).thenReturn("Mouse");
        when(request.getParameter("attribute")).thenReturn("Jerry");
        homeServlet.doPost(request, response);

        Assert.assertEquals(animals.get("Mouse").getLast(), "Jerry");
    }

    @Test
    public void checkIfAttributeWouldBeRemoved() throws IOException {
        HomeServlet homeServlet = new HomeServlet();
        HttpSession session = mock(HttpSession.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        LinkedList<String> mouses = new LinkedList<>(Arrays.asList("Jerry", "Micky"));
        Map<String, LinkedList<String>> animals = new HashMap<>();

        animals.put("Mouse", mouses);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("animals")).thenReturn(animals);
        when(request.getParameter("action")).thenReturn("Remove");
        when(request.getParameter("key")).thenReturn("Mouse");
        when(request.getParameter("attribute")).thenReturn("Micky");
        homeServlet.doPost(request, response);

        Assert.assertEquals(animals.get("Mouse").getFirst(), "Jerry");
        Assert.assertEquals(animals.get("Mouse").size(), 1);
    }

    @Test
    public void checkIfSessionWouldBeInvalidated() throws IOException {
        HomeServlet homeServlet = new HomeServlet();
        HttpSession session = mock(HttpSession.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpServletRequest request = mock(HttpServletRequest.class);

        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("Invalidate");
        homeServlet.doPost(request, response);

        verify(session, atLeast(1)).invalidate();
    }

    @Test
    public void shouldThrowExceptionWhenInputValuesAreEmpty() throws IOException {
        HomeServlet homeServlet = new HomeServlet();
        HttpSession session = mock(HttpSession.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        boolean thrown = false;
        Map<String, LinkedList<String>> animals = new HashMap<>();
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("add");
        when(request.getParameter("key")).thenReturn("");
        when(request.getParameter("attrubute")).thenReturn("");
        when(session.getAttribute("animals")).thenReturn(animals);

        try {
            homeServlet.doPost(request, response);
        } catch (InvalidObjectException e) {
           thrown = true;
        }

        Assert.assertTrue(thrown);
    }


}
