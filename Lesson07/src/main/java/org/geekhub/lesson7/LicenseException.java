package org.geekhub.lesson7;

public class LicenseException extends RuntimeException {
    public LicenseException(String s)
    {
        super(s);
    }
}
