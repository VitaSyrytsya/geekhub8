package org.geekhub.lesson7;

import org.geekhub.lesson7.task1.License;
import org.geekhub.lesson7.task1.User;
import org.geekhub.lesson7.task1.UserException;
import org.geekhub.lesson7.task1.UserService;

import java.time.LocalDate;
import java.util.*;

public class UserServiceImpl implements UserService {


    public void checkingLicense(User user) {
        if (user.getLicense().isEmpty()) {

            throw new LicenseException("License not found!");

        } else if (user.getLicense().size() > 1) {

            List<License> licenses = user.getLicense();
            int size = user.getLicense().size();
            for (int i = 0, z = 1; z < size; i++, z++) {

                if (licenses.get(i).getExpirationDate().isBefore(LocalDate.now()) &&
                        licenses.get(z).getExpirationDate().isBefore(LocalDate.now())) {

                    throw new LicenseException("Licenses are expired !");

                } else if (!((licenses.get(i).getExpirationDate()).isBefore(licenses.get(z).getStartDate()))) {

                    throw new LicenseException("Licenses intersect!");
                }


            }

        } else if (user.getLicense().size() == 1) {
            if (user.getLicense().get(0).getExpirationDate().isBefore(LocalDate.now())) {
                throw new LicenseException("License is expired!");
            }

        }
    }


    @Override
    public int save(User user, List<User> users) {
        if (users.isEmpty()) {
            checkingLicense(user);
        } else {
            for (User x : users) {
                if (x.getLogin().equals(user.getLogin()) || x.getId() == user.getId()) {
                    throw new UserException("User already exists!");
                }


            }


        }
        checkingLicense(user);
        users.add(user);
        System.out.println(users);
        return 1;
    }


    @Override
    public User getByLogin(String login, List<User> users) {


        for (User u : users) {
            if (u.getLogin().equals(login)) {
                return u;
            }

        }
        throw new UserException("User not found!");

    }

    @Override
    public List<User> getActiveUsers(List<User> users) {
        List<User> ActiveUsers = users;
        Iterator<User> userIterator = ActiveUsers.iterator();
        while (userIterator.hasNext()) {
            User x = userIterator.next();
            Iterator<License> iter = x.getLicense().iterator();
            while (iter.hasNext()) {
                License y = iter.next();
                if (y.getDaysBetween() < 0) {
                    iter.remove();
                }
            }
            if (x.getLicense().size() == 0) {
                userIterator.remove();


            }


        }


        Collections.sort(ActiveUsers, new compareByLicense());

        return ActiveUsers;
    }

    @Override
    public List<User> getExpriringUsers(int n, List<User> users) {
        List<User> ActiveUsers = getActiveUsers(users);
        List<User> UsersSortedByN = new ArrayList<>();
        Iterator<User> userIterator = ActiveUsers.iterator();
        while (userIterator.hasNext()) {
            User a = userIterator.next();
            if (a.getDaysBetween() == n) {

                UsersSortedByN.add(a);

            }

        }

        return UsersSortedByN;
    }

    @Override
    public List<User> getExprirringUsers(LocalDate date, List<User> users) {
        List<User> UsersSortedByDate = new ArrayList<>();
        for (User a : getActiveUsers(users)) {
            if (a.getLicense().get(0).getExpirationDate().isEqual(date)) {
                UsersSortedByDate.add(a);
            }
        }
        return UsersSortedByDate;
    }

    @Override
    public List<User> getAll(List<User> users) {

        Collections.sort(users, new compareByName());


        return users;
    }


    public static class compareByName implements Comparator<User> {
        @Override
        public int compare(User o1, User o2) {
            int n = o1.getFullName().compareTo(o2.getFullName());
            if (n == 0) {
                return o1.getLogin().compareTo(o2.getLogin());
            }
            return o1.getFullName().compareTo(o2.getFullName());
        }
    }

    public static class compareByLicense implements Comparator<User> {
        @Override
        public int compare(User o1, User o2) {

            int a = o2.getLicense().get(0).getExpirationDate().compareTo(o1.getLicense().get(0).getExpirationDate());
            if (a == 0) {
                a = o1.getFullName().compareTo(o2.getFullName());
                if (a == 0) {
                    return o1.getLogin().compareTo(o2.getLogin());
                }
                return o1.getFullName().compareTo(o2.getFullName());


            }


            return a;
        }

    }
}
