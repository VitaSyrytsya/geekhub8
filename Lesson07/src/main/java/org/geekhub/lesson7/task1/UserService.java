package org.geekhub.lesson7.task1;

import java.time.LocalDate;
import java.util.List;

public interface UserService {
    public int  save(User user, List<User>users);
    public User getByLogin(String login, List<User>users);
    public List<User>getActiveUsers(List<User>users);
    public List<User>getExpriringUsers( int n, List<User>users);
    public List<User>getExprirringUsers( LocalDate date, List<User>users);
    public List<User>getAll(List<User>users);

}
