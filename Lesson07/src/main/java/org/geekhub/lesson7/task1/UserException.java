package org.geekhub.lesson7.task1;

public class UserException extends RuntimeException {
    public UserException(String s)
    {
        super(s);
    }
}
