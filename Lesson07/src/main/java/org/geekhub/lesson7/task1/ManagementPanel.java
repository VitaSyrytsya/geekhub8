package org.geekhub.lesson7.task1;

import org.geekhub.lesson7.LicenseException;
import org.geekhub.lesson7.UserServiceImpl;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.LocalDate;
import java.util.*;

public class ManagementPanel {
    UserServiceImpl userService = new UserServiceImpl();
    Storage storage = new Storage();


    public boolean authorization(User user) {
        if (user.getAdmin()) {
            System.out.println("Welcome!");
            return true;
        } else {
            throw new LicenseException("Access denyed!");
        }
    }


    public int remove(User currentUser, String login, List<User> users) {
        authorization(currentUser);
        User userToRemove = userService.getByLogin(login, users);
        if (currentUser.getId() == userToRemove.getId()) {
            throw new LicenseException("Cannot delete current user!");
        }
        users.remove(userToRemove);
        return 1;
    }


    public License creatingLicense() {
        License license;
        String licenseType = "";
        Scanner i = new Scanner(System.in);
        System.out.println("Please, input type of license:");
        String enteredType = i.nextLine();
        System.out.println("Please, input start date in format yyyy-mm-dd :");
        String enteredStartDate = i.nextLine();
        LocalDate startDate = LocalDate.parse(enteredStartDate);
        System.out.println("Please, input expiration date in format yyyy-mm-dd :");
        String enteredExpirationDate = i.nextLine();
        LocalDate expirationDate = LocalDate.parse(enteredExpirationDate);
        if (enteredType.equals("FULL") || enteredType.equals("WEB_ONLY")
                || (enteredType.equals("MOBILE_ONLY"))) {
            licenseType = enteredType;
        } else {
            throw new LicenseException("Incorrect type of license!");
        }
        license = new License(licenseType, startDate, expirationDate);
        return license;
    }


    public int create(User currentUser, List<User> users, String login, String password, String fullName, boolean admin) {

        authorization(currentUser);
        int id = storage.setExpectedId();


        if (login.isEmpty()) {
            throw new LicenseException("Login shouldn't be empty!");
        }
        if (password.isEmpty()) {
            throw new LicenseException("Password shouldn't be empty!");
        }

        if (fullName.isEmpty()) {
            throw new LicenseException("Name shouldn't be empty!");
        }


        List<License> licenses = new ArrayList();
        Scanner i = new Scanner(System.in);
        System.out.println("Now, add license. ");

        String a = "";
        do {
            licenses.add(creatingLicense());
            System.out.println("If you want to add one more license, print 'yes': ");
            a = i.nextLine();
        } while (a.equals("yes"));

        User user = new User(login, password, id, fullName, licenses, admin);

        userService.save(user, users);

        return 1;
    }


    public int update(int id, List<User> users, User currentUser) {
        authorization(currentUser);
        User y = null;
        for (User x : users) {
            if (x.getId() == id) {
                y = x;
                x.setLicense(x.getLicense());
                x.setLogin(x.getLogin());
                x.setPassword(x.getPassword());
                System.out.println(x);


            }
        }
        return 1;
    }

    public List<User> printUsersWithLicenseExpiredInAWeek(List<User> users) {

        List<User> usersExpiredInAWeek = userService.getExpriringUsers(7, users);
        System.out.println("Users, whose license expires in a week: " + userService.getAll(usersExpiredInAWeek));

        return usersExpiredInAWeek;
    }

    public long getClientDaysCount(int clientId, List<User> users) {

        long clientDaysCount = 0;
        List<License> licenses = new ArrayList<>();
        for (User x : users) {
            if (x.getId() == clientId) {
                licenses = x.getLicense();
            }
        }
        for (License lic : licenses) {
            if (lic.getExpirationDate().isBefore(LocalDate.now())) {
                clientDaysCount = clientDaysCount + 1 + DAYS.between(lic.getStartDate(), lic.getExpirationDate());
            } else {
                clientDaysCount = clientDaysCount + 1 + DAYS.between(lic.getStartDate(), LocalDate.now());
            }
        }

        return clientDaysCount;

    }

    public List<User> getAllUsers(List<User> users) {
        System.out.println(userService.getAll(users));
        return users;
    }


    public Map<Boolean, List<User>> map(List<User> users) {
        Map map = new HashMap<>();
        List<User> admins = new ArrayList<>();
        List<User> notAdmins = new ArrayList<>();
        for (User x : users) {
            if (x.getAdmin()) {
                admins.add(x);
            }
        }
        for (User x : users) {
            if (!x.getAdmin()) {
                notAdmins.add(x);
            }
        }
        map.put(true, admins);
        map.put(false, notAdmins);
        return map;
    }


    public Map<String, List<User>> map1(List<User> users) {
        List<User> activeUsers = userService.getActiveUsers(users);
        Map map = new HashMap<>();
        List<User> users_With_FullLicense = new ArrayList<>();
        List<User> users_With_WEB_ONLY_license = new ArrayList<>();
        List<User> users_With_MOBILE_ONLY_license = new ArrayList<>();

        for (User z : activeUsers) {
            if (z.getLicense().get(0).type.equals("FULL")) {
                users_With_FullLicense.add(z);
            } else if (z.getLicense().get(0).type.equals("WEB_ONLY")) {
                users_With_WEB_ONLY_license.add(z);
            } else if (z.getLicense().get(0).type.equals("MOBILE_ONLY")) {
                users_With_MOBILE_ONLY_license.add(z);
            }
        }
        map.put("FULL", users_With_FullLicense);
        map.put("WEB_ONLY", users_With_WEB_ONLY_license);
        map.put("MOBILE_ONLY", users_With_MOBILE_ONLY_license);
        return map;
    }


}


