package org.geekhub.lesson7.task1;

import java.time.LocalDate;
import java.util.List;

public class User implements Comparable<User> {
    private final int id;
    private String login;
    private String password;
    private final String fullName;
    private boolean admin;
    private List<License> license;


    public User(String login, String password, int id, String fullName, List<License> license, boolean admin) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.fullName = fullName;
        this.license = license;
        this.admin = admin;
    }

    public boolean getAdmin() {
        return admin;
    }

    public String getFullName() {
        return fullName;
    }


    public String getLogin() {
        return login;
    }

    public int getId() {

        return id;
    }

    public List<License> getLicense() {
        return license;
    }

    public long getDaysBetween() {
        return license.get(0).getDaysBetween();
    }

    public void setLicense(List<License> license) {
        this.license = license;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {

        for (int i = 0; i < getLicense().size(); i++) {
            if (getLicense().get(i).getExpirationDate().isBefore(LocalDate.now())) {
                return getLogin() + " " + getFullName() + " " + getId() + "  License:  Inactive ";
            } else {
                return getLogin() + " " + getFullName() + " " + getId() + " " + getLicense();
            }

        }
        return getLogin() + " " + getFullName() + " " + getId() + " " + getLicense();
    }

    @Override
    public int compareTo(User o) {
        int result = this.getFullName().compareTo(o.getFullName());

        return result;
    }


    public void setPassword(String password) {
        this.password = password;
    }
}
