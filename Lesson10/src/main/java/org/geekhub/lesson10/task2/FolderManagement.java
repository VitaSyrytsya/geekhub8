package org.geekhub.lesson10.task2;
//
import org.geekhub.lesson10.task1.DifferentOperationsWithFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FolderManagement {

    private static final Logger log = Logger.getLogger(DifferentOperationsWithFile.class.getName());

    public boolean checkIfFolderDoesNotExist(String name, String currentPath) {
        Path folder = Paths.get(currentPath + "\\" + name);
        return !Files.exists(folder);
    }

    public Path renameFolder(Path fileToRename, String newName) {
        Path folderAfterRenaming = Paths.get(fileToRename.getParent() + "\\" + newName);
        try {
            Files.move(fileToRename, folderAfterRenaming);
        } catch (IOException e) {
          log.log(Level.SEVERE, "File not found", e);
        }
        return folderAfterRenaming;
    }

    public Path addFolder(String name, String currentPath) {
        Path newFolder = null;
        try {
            newFolder = Files.createDirectories(Paths.get(currentPath + "\\" + name));
        } catch (IOException e) {
           log.log(Level.SEVERE, "Smth wrong during creating directory");
        }
        return newFolder;
    }

    public String deleteFolder(String name, String currentPath) {
        String messageDialog = null;
        Path folderToDelete = Paths.get(currentPath + "\\" + name);
        try {
            Files.delete(folderToDelete);
            messageDialog = "DELETED";
        } catch (IOException e) {
           log.log(Level.SEVERE, "File not found by this Path", e);
        }
        return messageDialog;
    }



}
