package org.geekhub.lesson10.task1;
//
import org.geekhub.lesson9.User;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class DifferentOperationsWithFile {

    private static final Logger log = Logger.getLogger(DifferentOperationsWithFile.class.getName());

    public Path writeUserNamesIntoFile(List<User> users, String filename)
            throws IOException {


        Path file = Paths.get(filename + ".tmp");
        Function<User, String> userFilter = User::getName;
        Files.write(file, (users.stream().map(userFilter).collect(Collectors.toList())));
        return file;
    }

    public List<User> getListOfUsersFromFile(File dataFile) throws IOException {
        List<User> users = new ArrayList<>();
        try (
                FileInputStream fis = new FileInputStream(dataFile);
                ObjectInputStream ois = new ObjectInputStream(fis)
        ) {
            while (true) {
                Object user = ois.readObject();
                if (user instanceof User) {
                    users.add((User) user);
                }
            }
        } catch (EOFException e) {

        } catch (FileNotFoundException e) {
            log.log(Level.SEVERE, "File" + dataFile + "has not been found", e);

        } catch (ClassNotFoundException e) {
            log.log(Level.SEVERE, "Class has not been found", e);
        }
        return users;
    }

    public int averageUserId(List<User> users) {

        List<User> usersWithBiggestAverageAcessLevel = findUsersWithBiggerThanAverageAccessLevel(users);

        return findUserWithBiggestNameId(usersWithBiggestAverageAcessLevel).getId();
    }

    public User findUserWithBiggestNameId(List<User> users) {
        Function<User, String> getName = User::getName;
        Comparator<String> compareNameLength = (x, y) -> {
            int n = 0;
            if (x.length() > y.length()) {
                n = 1;
            } else if (y.length() > x.length()) {
                n = -1;
            }
            return n;
        };
        String userBiggestNameId = users.stream().map(getName).max(compareNameLength).get();
        Predicate<User> predicate1 = x -> x.getName().equals(userBiggestNameId);

        return users.stream().filter(predicate1).collect(Collectors.toList()).get(0);
    }

    public List<User> findUsersWithBiggerThanAverageAccessLevel(List<User> users) {
        Function<User, Integer> getAccessLevel = User::getAccessLevel;
        BinaryOperator<Integer> sumOfAccessLevels = (x, y) -> x + y;
        int seed = 0;
        int averageAccessLevel = users.stream().map(getAccessLevel)
                .reduce(seed, sumOfAccessLevels) / users.size();
        Predicate<User> predicate = x -> x.getAccessLevel() > averageAccessLevel;
        List<User> usersWithBiggestAverageAcessLevel = users.stream().filter(predicate)
                .collect(Collectors.toList());

        return usersWithBiggestAverageAcessLevel;

    }


    public void saveFileFromUrl(String expectedFileName, String fileUrl)
            throws IOException {
        Path file = Paths.get(expectedFileName);
        InputStream stream = new URL(fileUrl).openStream();
        Files.copy(stream, file);
    }


    public void unzip(String filename) {
        File scrFile = new File(filename);

        String zipPath = filename.substring(0, filename.length() - 4);
        File temp = new File(zipPath);
        temp.mkdir();

        ZipFile zipFile;
        try {
            zipFile = new ZipFile(scrFile);
            Enumeration<? extends ZipEntry> e = zipFile.entries();
            while (e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();

                File destinationPath = new File(zipPath, entry.getName());

                destinationPath.getParentFile().mkdirs();
                if (entry.isDirectory()) {
                } else {
                    System.out.println("Extracting file: " + destinationPath);
                    BufferedInputStream bis = new BufferedInputStream(zipFile.getInputStream(entry));
                    int b;
                    byte buffer[] = new byte[1024];
                    FileOutputStream fos = new FileOutputStream(destinationPath);
                    BufferedOutputStream bos = new BufferedOutputStream(fos, 1024);
                    while ((b = bis.read(buffer, 0, 1024)) != -1) {
                        bos.write(buffer, 0, b);
                    }
                    bos.close();
                    bis.close();
                }
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, "Some objects in file are not serializable", e);
        }
    }

}
