package org.geekhub.lesson10.task3;

import java.util.Scanner;

public class InputClass {

    public double estimateTimeForCopyingFiles(){

        CopyingFiles copyingFiles = new CopyingFiles();
        final int nanosecondsInSecond = 1000_000_000;

        Scanner i = new Scanner(System.in);
        System.out.println("Input path to your file:");
        String source = i.nextLine();
        System.out.println("Input path to your folder:");
        String target = i.nextLine();
        System.out.println("To choose buffered copying, print 'b' " +
                "simple copying - print 's'");
        String typeOfCopying = i.nextLine();

        long a = System.nanoTime();

        switch (typeOfCopying) {
            case "s":
                copyingFiles.simpleCopying(source, target);
                break;
            case "b":
                copyingFiles.copyingWithBuffer(source, target);
                break;
            default:
                System.out.println("You haven't chose any action!");
                break;
        }

        long b = System.nanoTime();
        long estimatedTimeInNanoseconds = b - a;

        return (double)estimatedTimeInNanoseconds / nanosecondsInSecond;
    }

}
