package org.geekhub.lesson10.task3;

import org.geekhub.lesson10.task1.DifferentOperationsWithFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CopyingFiles {

    private static final Logger log = Logger.getLogger(DifferentOperationsWithFile.class.getName());

    public void simpleCopying(String source, String dest) {
        Path sourceFile = Paths.get(source);
        Path destFile = Paths.get(dest);
        try {
            Files.copy(sourceFile, destFile);
        } catch (IOException e) {
            log.log(Level.SEVERE, "File" + source + "hasn't been found!", e);

        }
    }

    public void copyingWithBuffer(String source, String dest) {
        Path sourcePath = Paths.get(source);
        Path destinationPath = Paths.get(dest);
        try (
                BufferedReader reader = Files.newBufferedReader(sourcePath, Charset.forName("ISO-8859-1"));
                BufferedWriter writer = Files.newBufferedWriter(destinationPath,  Charset.forName("ISO-8859-1"))
        ) {
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                writer.write(currentLine);
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, "File" + source + "is not serializable!", e);
        }

    }
}
