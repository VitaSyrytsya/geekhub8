package org.geekhub.lesson9;
//
import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = -2018L;


    private String name;
    private int accessLevel;
    public String favouriteEncoding;
    private int id;


    public User(int id, String name, int accessLevel, String favouriteEncoding) {
        this.name = name;
        this.accessLevel = accessLevel;
        this.favouriteEncoding = favouriteEncoding;
        this.id = id;

    }


    public String getName() {
        return name;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    private String getFavouriteEncoding() {
        return favouriteEncoding;
    }

    public int getId() {
        return id;
    }

    public void setFavouriteEncoding(String favouriteEncoding) {
        this.favouriteEncoding = favouriteEncoding;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }


    @Override
    public String toString() {
        return id + " " + name + " " + accessLevel + " " + favouriteEncoding;
    }
}
