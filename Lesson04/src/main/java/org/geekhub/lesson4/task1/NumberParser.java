package org.geekhub.lesson4.task1;

public class NumberParser {

    public static int parse(String enteredString) {
        if (enteredString == null) {
            throw new NumberFormatException("Input smth");
        }
        char[] enteredStringChars = enteredString.toCharArray();
        int enteredStringCharsLength = enteredStringChars.length;
        int parsedInt = 0;

        if (enteredString.length() == 0) {
            throw new NumberFormatException("Input is invalid");
        }
        for (int i = 0, y = 0; i < enteredStringChars.length; i++, y++) {
            if (Character.isDigit(enteredStringChars[i])) {
                int[] numbers = new int[enteredStringChars.length];
                numbers[y] = Character.getNumericValue(enteredStringChars[i]);
                parsedInt = parsedInt + (numbers[y] * (int) Math.pow(10, enteredStringCharsLength - 1));
                enteredStringCharsLength--;
            }


            if (enteredStringChars[0] == '-' || enteredStringChars[0] == '+') {
                for (i = 1, y = 0; i < enteredStringChars.length; i++, y++) {
                    if (Character.isDigit(enteredStringChars[i])) {
                        int[] numbers = new int[enteredStringChars.length - 1];
                        numbers[y] = Character.getNumericValue(enteredStringChars[i]);
                        if (enteredStringChars[0] == '-') {
                            parsedInt = parsedInt + -((numbers[y] * (int) Math.pow(10, enteredStringCharsLength - 2)));
                            enteredStringCharsLength--;
                        }
                        if (enteredStringChars[0] == '+') {
                            parsedInt = parsedInt + (numbers[y] * (int) Math.pow(10, enteredStringCharsLength - 2));
                            enteredStringCharsLength--;
                        }
                    } else {
                        throw new NumberFormatException("Input is invalid");
                    }
                }
            }
        }
        for (int i = 0, y = 0; i < enteredStringChars.length; i++, y++) {
            if (!Character.isDigit(enteredStringChars[i]) && enteredStringChars[0] != '-' && enteredStringChars[0] != '+') {
                throw new NumberFormatException("Input is invalid");
            }
        }
        return parsedInt;
    }


}

