package org.geekhub.lesson4.task2.authentication;

import org.geekhub.lesson4.task2.user.InMemoryUserSource;
import org.geekhub.lesson4.task2.user.User;

public class AuthenticationService {
    private InMemoryUserSource inMemoryUserSource;

    public AuthenticationService(InMemoryUserSource inMemoryUserSource) {
        this.inMemoryUserSource = inMemoryUserSource;
    }

    public User auth(String username, String password) {
        User foundUser;
        if (username == null || username.isEmpty() || username.isBlank() ||
                password == null || password.isBlank() || password.isEmpty()) {
            throw new WrongCredentialsException();
        }else if(!inMemoryUserSource.find(username).isPresent()){
            throw new UserNotFoundException();
        }else{
            User user = inMemoryUserSource.find(username).get();
            if(user.password.equals(password) ){
                foundUser = user;
            }else{
                throw new WrongPasswordException();
            }
        }
        return foundUser;
    }
}
