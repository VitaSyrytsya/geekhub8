package org.geekhub.lesson4.task2.user;

import java.util.Optional;

public class InMemoryUserSource {

    private User[] user;

    public InMemoryUserSource(User... user) {
        this.user = user;
    }


    public Optional<User> find(String username) {
        User resultUser = null;
        for (int i = 0; i < user.length; i++) {
            if (user[i].username.equals(username)) {
                resultUser = user[i];
            }
        }
        return Optional.ofNullable(resultUser);
    }
}
