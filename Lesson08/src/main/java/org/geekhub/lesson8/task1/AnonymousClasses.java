package org.geekhub.lesson8.task1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;

public class AnonymousClasses {

    public static void main(String[] args) {
        CollectionOperationsImpl collectionOperations = new CollectionOperationsImpl();

        List<String> fruits = new ArrayList<>();
        fruits.add("Orange");


        fruits.add("Lemon");
        fruits.add("Apple");
        fruits.add("Avocado");
        fruits.add("Banana");
        fruits.add("Apple");
        fruits.add("Garnet");

        Supplier producer = new Supplier() {
            @Override
            public Object get() {
                return new User();
            }
        };

        Predicate filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                if (o.toString().equals("Orange")) {
                    return true;
                }
                return false;
            }
        };
        Consumer consumer = new Consumer() {
            @Override
            public void accept(Object o) {
                System.out.print(o.toString().concat("Fruit "));
            }
        };

        Function function = new Function() {
            @Override
            public Object apply(Object o) {
                Object x = String.valueOf(o).toUpperCase();
                return x;
            }
        };
        Function keyFunction = new Function() {
            @Override
            public Object apply(Object o) {
                Object x = "Green";
                Object y = "Yellow";
                Object z = "Orange";
                Object c = "Red";
                if (o.toString().equals("Banana") || o.toString().equals("Lemon")) {
                    return y;
                } else if (o.toString().equals("Apple") || o.toString().equals("Avocado")) {
                    return x;
                } else if (o.toString().equals("Garnet")) {
                    return c;
                } else {
                    return z;
                }


            }
        };

        Function function1 = new Function() {
            @Override
            public Object apply(Object o) {
                Object x = "Fruits";
                Object y = "Vegetables";
                if (o.toString().equals("Orange") || o.toString().equals("Apple") || o.toString().equals("Lemon") ||
                        o.toString().equals("Banana")) {
                    return x;
                } else {
                    return y;
                }

            }
        };
        Comparator comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                int n = o1.toString().compareTo(o2.toString());
                return n;
            }
        };
        BinaryOperator binaryOperator = new BinaryOperator() {
            @Override
            public Object apply(Object o, Object o2) {
                Object o3 = o.toString().concat(o2.toString());
                return o3;
            }
        };


        System.out.println(collectionOperations.fill(producer, 5));
        System.out.println(collectionOperations.filter(fruits, filter));
        System.out.println(collectionOperations.anyMatch(fruits, filter));
        System.out.println(collectionOperations.allMatch(fruits, filter));
        collectionOperations.forEach(fruits, consumer);
        System.out.println(collectionOperations.noneMatch(fruits, filter));
        System.out.println(collectionOperations.map(fruits, function));
        System.out.println(collectionOperations.max(fruits, comparator));
        System.out.println(collectionOperations.min(fruits, comparator));
        System.out.println(collectionOperations.distinct(fruits));
        System.out.println(collectionOperations.reduce(fruits, binaryOperator));
        System.out.println(collectionOperations.partitionBy(fruits, filter));
        System.out.println(collectionOperations.groupBy(fruits, function1));
        System.out.println(collectionOperations.toMap(fruits, keyFunction, function, binaryOperator));

    }


}
