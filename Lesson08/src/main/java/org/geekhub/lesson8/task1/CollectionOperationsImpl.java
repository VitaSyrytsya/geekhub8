package org.geekhub.lesson8.task1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class CollectionOperationsImpl implements CollectionOperations {


    @Override
    public <E> List<E> fill(Supplier<E> producer, int count) {
        List<E> expectedList = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            expectedList.add(producer.get());
        }
        return expectedList;
    }

    @Override
    public <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> userList = new ArrayList<>();
        for (E x : elements) {
            if (filter.test(x)) {
                userList.add(x);
            }
        }
        return userList;
    }

    @Override
    public <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E x : elements) {
            if (predicate.test(x)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E x : elements) {
            if (!predicate.test(x)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <E> void forEach(List<E> elements, Consumer<E> consumer) {
        elements.forEach(consumer);

    }

    @Override
    public <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E x : elements) {
            if (predicate.test(x)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> newList = new ArrayList<>();
        for (T t : elements) {
            newList.add(mappingFunction.apply(t));
        }
        return newList;
    }

    @Override
    public <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        if(elements.size()==0){
            return Optional.empty();
        }
        elements.sort(comparator);
        if (elements.get(0).equals(elements.get(elements.size() - 1))) {
            return Optional.empty();
        }
        return Optional.of(elements.get(0));
    }

    @Override
    public <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        if(elements.size()==0){
            return Optional.empty();
        }
        elements.sort(comparator);
        if (elements.get(0).equals(elements.get(elements.size() - 1))) {
            return Optional.empty();
        }
        return Optional.of(elements.get(elements.size() - 1));
    }

    @Override
    public <E> List<E> distinct(List<E> elements) {
        Set<E> uniqueValues = new HashSet<>(elements);
        return new ArrayList<>(uniqueValues);
    }

    @Override
    public <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.size() < 2) {
            return Optional.empty();
        }
        E x;
        x = elements.get(0);
        for (int i = 0; i < elements.size() - 1; i++) {
            x = accumulator.apply(x, elements.get(i + 1));
        }
        return Optional.ofNullable(x);
    }

    @Override
    public <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        if(elements.size()==0){
            return seed;
        }
        E x;
        x = elements.get(0);
        for (int i = 0; i < elements.size() - 1; i++) {
            x = accumulator.apply(x, elements.get(i + 1));
        }
        return accumulator.apply(x, seed);
    }

    @Override
    public <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        Map<Boolean, List<E>> resultMap = new HashMap<>();
        List<E> trueElements = new ArrayList<>();
        List<E> falseElements = new ArrayList<>();
        for (E x : elements) {
            if (predicate.test(x)) {
                trueElements.add(x);
            } else {
                falseElements.add(x);
            }
        }
        resultMap.put(true, trueElements);
        resultMap.put(false, falseElements);
        return resultMap;
    }

    @Override
    public <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        Map<K, List<T>> resultMap = new HashMap<>();
        List<K> keys = new LinkedList<>();

        for (T x : elements) {
            keys.add(classifier.apply(x));
        }
        for (K key : keys) {
            List<T>typeList = new ArrayList<>();
            for (T element : elements) {
                if (key.equals(classifier.apply(element))) {
                    typeList.add(element);
                }
            }
            resultMap.put(key,typeList);
        }
        return resultMap;
    }

    @Override
    public <T, K, U> Map<K, U> toMap(List<T> elements, Function<T, K> keyFunction,
                                     Function<T, U> valueFunction, BinaryOperator<U> mergeFunction) {
        Map<K, U> resultMap = new HashMap<>();
        List<K> keys = new LinkedList<>();
        List<U> modifiedElements = new ArrayList<>();
        U Z;
        for (T x : elements) {
            keys.add(keyFunction.apply(x));
        }
        Set<K> keys2 = new HashSet<>(keys);
        List<K> keys3 = new ArrayList<>(keys2);

        for (K key : keys3) {
            for (T element : elements) {
                if (key.equals(keyFunction.apply(element))) {
                    modifiedElements.add(valueFunction.apply(element));
                }
            }
            Z = modifiedElements.get(0);


            for (int z = 0; z < modifiedElements.size() - 1; z++) {
                Z = mergeFunction.apply(Z, modifiedElements.get(z + 1));
            }

            resultMap.put(key, Z);
            modifiedElements.removeAll(modifiedElements);
        }

        return resultMap;
    }


}
