package org.geekhub.lesson8.task1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;

public class MethodReferences {
    public static void main(String[] args) {
        CollectionOperationsImpl collectionOperations = new CollectionOperationsImpl();

        List<String> fruits = new ArrayList<>();
        fruits.add("Apple");
        fruits.add("Orange");
        fruits.add("Lemon");
        fruits.add("Apple");
        fruits.add("Carrot");


        Supplier<User> producer = User::new;
        Predicate<String> filter = MethodReferences::test;
        Consumer<String> consumer = MethodReferences::accept;
        Function<String, String> function = String::toUpperCase;
        Comparator<String> comparator = String::compareTo;
        BinaryOperator<String> binaryOperator = String::concat;
        Function<String, String> function1 = MethodReferences::define;
        Function<String, String> keyFunction = MethodReferences::define2;


        System.out.println(collectionOperations.fill(producer, 5));
        System.out.println(collectionOperations.filter(fruits, filter));
        System.out.println(collectionOperations.anyMatch(fruits, filter));
        System.out.println(collectionOperations.allMatch(fruits, filter));
        collectionOperations.forEach(fruits, consumer);
        System.out.println(collectionOperations.noneMatch(fruits, filter));
        System.out.println(collectionOperations.map(fruits, function));
        System.out.println(collectionOperations.max(fruits, comparator));
        System.out.println(collectionOperations.min(fruits, comparator));
        System.out.println(collectionOperations.distinct(fruits));
        System.out.println(collectionOperations.reduce(fruits, binaryOperator));
        System.out.println(collectionOperations.partitionBy(fruits, filter));
        System.out.println(collectionOperations.groupBy(fruits, function1));
        System.out.println(collectionOperations.toMap(fruits, keyFunction, function, binaryOperator));

    }

    private static String define2(String s) {
        switch (s) {
            case "Banana":
            case "Lemon":
                return "Yellow";
            case "Apple":
            case "Avocado":
                return "Green";
            case "Garnet":
                return "Red";
            default:
                return "Orange";
        }
    }

    private static void accept(String s) {
        System.out.print(s.concat("Fruit "));

    }


    private static boolean test(String string) {
        return string.equals("Apple");
    }

    private static String define(String s) {
        if (s.equals("Orange") || s.equals("Apple") || s.equals("Lemon") ||
                s.equals("Banana")) {
            return "Fruits";
        } else {
            return "Vegetables";


        }

    }

}