package org.geekhub.lesson8.task1;


import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.testng.Assert.*;

public class CollectionOperationsMReferenceTest {
    private CollectionOperations collectionOperations;

    private static boolean test4(String string) {
        return string.equals("Apple");
    }


    @BeforeMethod
    public void setUp() {
        collectionOperations = new CollectionOperationsImpl();
    }


    @Test
    public void shouldCreateListOfObjects() {
        Supplier<User> producer = User::new;
        List<User> resultList = collectionOperations.fill(producer, 3);

        Assert.assertEquals(resultList.size(), 3);
        Assert.assertEquals(resultList.get(0).toString(), "USER");
        Assert.assertEquals(resultList.get(1).toString(), "USER");
        Assert.assertEquals(resultList.get(2).toString(), "USER");
    }

    @Test
    public void shouldCreateEmptyListOfObjects() {
        Supplier<User> producer = User::new;
        List<User> resultList = collectionOperations.fill(producer, 0);

        Assert.assertEquals(resultList.size(), 0);

    }

    @Test
    public void shouldFilterListBySomeRule() {
        List<String> fruits = Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet");
        Predicate<String> filter = CollectionOperationsMReferenceTest::test;

        List<String> resultList = collectionOperations.filter(fruits, filter);
        List<String> expectedList = Collections.singletonList("Orange");

        assertCollectionEqualsIgnoringOrder(resultList, expectedList);
    }

    private static boolean test(String s) {
        return s.equals("Orange");
    }

    @Test
    public void shouldCreateEmptyListWhenFilterReturnsEmptyData() {
        List<String> fruits = Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet");
        Predicate<String> filter = CollectionOperationsMReferenceTest::test2;

        List<String> resultList = collectionOperations.filter(fruits, filter);
        List<String> expectedList = new ArrayList<>();

        assertEquals(resultList, expectedList);
    }

    private static boolean test2(String s) {
        return s.equals("Data");
    }


    @Test
    public void shouldReturnTrueIfAtLeastOneObjMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = CollectionOperationsMReferenceTest::test;

        boolean result = collectionOperations.anyMatch(fruits, filter);

        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnFalseIfNoneObjMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple",
                "Avocado", "Banana", "Apple", "Garnet"));
        Predicate<String> filter = CollectionOperationsMReferenceTest::test2;
        boolean result = collectionOperations.anyMatch(fruits, filter);

        Assert.assertFalse(result);
    }

    @Test
    public void shouldReturnFalseIfAtLeastOneObjDoesNotMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = CollectionOperationsMReferenceTest::test;
        boolean result = collectionOperations.allMatch(fruits, filter);

        Assert.assertFalse(result);
    }

    @Test
    public void shouldReturnTrueIfAllObjMatchTheRule() {
        Predicate<String> filter = CollectionOperationsMReferenceTest::test;
        List<String> testList = Arrays.asList("Orange", "Orange", "Orange");
        boolean result = collectionOperations.allMatch(testList, filter);

        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnFalseIfAtLeastOneObjectMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = CollectionOperationsMReferenceTest::test;
        boolean result = collectionOperations.noneMatch(fruits, filter);

        Assert.assertFalse(result);
    }

    @Test
    public void shouldReturnTrueIfNoneObjectMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = CollectionOperationsMReferenceTest::test3;
        boolean result = collectionOperations.noneMatch(fruits, filter);

        Assert.assertTrue(result);
    }

    private static boolean test3(String s) {
        return s.equals("Computer");
    }


    @Test
    public void shouldTransformAllObjInListFromIntegerToString() {
        Function<Integer, String> Function = CollectionOperationsMReferenceTest::apply;

        List<Integer> testList = Arrays.asList(1, 2, 3, 4, 5, 6);
        List<String> resultList = collectionOperations.map(testList, Function);
        List<String> expectedList = Arrays.asList("1", "2", "3", "4", "5", "6");

        assertCollectionEqualsIgnoringOrder(resultList, expectedList);
    }

    private static String apply(Integer integer) {
        return integer.toString();
    }

    @Test
    public void shouldReturnEmptyListForEmptyInputToMap() {
        Function<Integer, String> Function = CollectionOperationsMReferenceTest::apply;

        List<Integer> testList = new ArrayList<>();
        List<String> resultList = collectionOperations.map(testList, Function);
        List<String> expectedList = new ArrayList<>();

        assertEquals(resultList, expectedList);
    }


    @Test
    public void shouldFindMaxElementInList() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Comparator<String> comparator = String::compareTo;

        Optional<String> result = collectionOperations.max(fruits, comparator);
        Optional<String> expectedResult = Optional.of("Apple");

        Assert.assertEquals(result, expectedResult);

    }

    @Test
    public void shouldReturnOptionalEmptyForEmptyMaxInput() {
        Comparator<String> comparator = String::compareTo;
        List<String> fruitsTest = new ArrayList<>();

        Optional<String> result1 = collectionOperations.max(fruitsTest, comparator);

        Assert.assertTrue(result1.isEmpty());
    }

    @Test
    public void shouldFindMinElementInList() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Comparator<String> comparator = String::compareTo;

        Optional<String> result = collectionOperations.min(fruits, comparator);
        Optional<String> expectedResult = Optional.of("Orange");

        Assert.assertEquals(result, expectedResult);

    }

    @Test
    public void shouldReturnOptionalEmptyForEmptyMinInput() {
        Comparator<String> comparator = String::compareTo;
        List<String> fruitsTest = new ArrayList<>();

        Optional<String> result1 = collectionOperations.min(fruitsTest, comparator);

        Assert.assertTrue(result1.isEmpty());
    }

    @Test
    public void shouldReturnUniqueObjFromList() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        List<String> result = collectionOperations.distinct(fruits);
        List<String> expectedResult = Arrays.asList("Garnet", "Apple", "Avocado", "Orange", "Lemon", "Banana");

        assertCollectionEqualsIgnoringOrder(result, expectedResult);
    }

    @Test
    public void shouldReturnEmptyListIfNoInputPresent() {
        List<String> testList = new ArrayList<>();
        List<String> result = collectionOperations.distinct(testList);
        List<String> expectedResult = new ArrayList<>();

        assertCollectionEqualsIgnoringOrder(result, expectedResult);
    }

    @Test
    public void shouldReduceAllObjectsFromList() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        BinaryOperator<String> binaryOperator = String::concat;
        Optional<String> result = collectionOperations.reduce(fruits, binaryOperator);
        Optional<String> expectedResult = Optional.of("OrangeLemonAppleAvocadoBananaAppleGarnet");

        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void shouldReduceObjectsWithSpecifiedObject() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        BinaryOperator<String> binaryOperator = String::concat;
        String result = collectionOperations.reduce("Summer", fruits, binaryOperator);
        String expectedResult = "OrangeLemonAppleAvocadoBananaAppleGarnetSummer";
        Assert.assertEquals(result, expectedResult);

    }

    @Test
    public void shouldReturnSpecifiedObjWhenListOfObjectsIsEmpty() {
        List<String> emptyList = new ArrayList<>();
        BinaryOperator<String> binaryOperator = String::concat;
        String result = collectionOperations.reduce("Summer", emptyList, binaryOperator);

        Assert.assertEquals(result, "Summer");

    }

    @Test
    public void objectShouldBePartitionedBySomeRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = CollectionOperationsMReferenceTest::test4;
        List<String> listFalse = Arrays.asList("Lemon", "Avocado", "Banana", "Garnet", "Orange");
        List<String> listTrue = Arrays.asList("Apple", "Apple");
        Map<Boolean, List<String>> result = collectionOperations.partitionBy(fruits, filter);

        assertCollectionEqualsIgnoringOrder(result.get(true), listTrue);
        assertCollectionEqualsIgnoringOrder(result.get(false), listFalse);

    }

    @Test
    public void objectShouldBePartitionedBySomeRuleTestForEmptyInput() {
        Predicate<String> filter = CollectionOperationsMReferenceTest::test;
        List<String> emptyList = new ArrayList<>();

        Map<Boolean, List<String>> resultMap = collectionOperations.partitionBy(emptyList, filter);

        assertCollectionEqualsIgnoringOrder(resultMap.get(true), emptyList);
        assertCollectionEqualsIgnoringOrder(resultMap.get(false), emptyList);
    }

    @Test
    public void objectShouldBeGroupedInMapBySomeRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));

        Function<String, String> functionForMapping = CollectionOperationsMReferenceTest::define;

        Map<String, List<String>> result = collectionOperations.groupBy(fruits, functionForMapping);
        List<String> listWithFruits = Arrays.asList("Orange", "Lemon", "Apple", "Banana", "Apple");
        List<String> listWithVegetables = Arrays.asList("Avocado", "Garnet");


        assertCollectionEqualsIgnoringOrder(result.get("Fruits"), listWithFruits);
        assertCollectionEqualsIgnoringOrder(result.get("Vegetables"), listWithVegetables);

    }

    private static String define(String s) {
        if (s.equals("Orange") || s.equals("Apple") || s.equals("Lemon") ||
                s.equals("Banana")) {
            return "Fruits";
        } else {
            return "Vegetables";


        }
    }

    @Test
    public void shouldAccumulateObjectsIntoMap() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));

        BinaryOperator<String> binaryOperator = String::concat;

        Function<String, String> valueFunction = String::toUpperCase;

        Function<String, String> keyFunction = CollectionOperationsMReferenceTest::define2;

        Map<String, String> result = collectionOperations.toMap(fruits, keyFunction, valueFunction, binaryOperator);

        Map<String, String> expectedResult = new HashMap<>();
        expectedResult.put("Red", "GARNET");
        expectedResult.put("Green", "APPLEAVOCADOAPPLE");
        expectedResult.put("Orange", "ORANGE");
        expectedResult.put("Yellow", "LEMONBANANA");


        Assert.assertEquals(result, expectedResult);

    }

    private static String define2(String s) {

        switch (s) {
            case "Banana":
            case "Lemon":
                return "Yellow";
            case "Apple":
            case "Avocado":
                return "Green";
            case "Garnet":
                return "Red";
            default:
                return "Orange";
        }
    }


    private <T> void assertCollectionEqualsIgnoringOrder(Collection<T> expected, Collection<T> actual) {
        if (actual == expected) {
            return;
        }
        assertNotNull(actual);
        assertNotNull(expected);

        assertEquals(actual.size(), expected.size(), "Collections are different in size.");
        for (T item : expected) {
            assertTrue(actual.contains(item), "Result collection does not contain expected value.");
        }
    }


}




