package org.geekhub.task1.controllers;

import org.geekhub.task1.DAO;
import org.geekhub.task1.EncryptPassword;
import org.geekhub.task1.exceptions.InvalidUserException;
import org.geekhub.task1.service.User;
import org.geekhub.task1.service.UserService;
import org.junit.Assert;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class UserServiceTest extends AbstractTestNGSpringContextTests{
    @Autowired
    private UserService userService;
    @MockBean
    private DAO dao;
    @MockBean
    private EncryptPassword encryptor;

    @Test
    public void checkIfUserWouldBeAdded() {
        User user = new User("user", "password");
        user.setFirstName("user");

        userService.add(user);

        Mockito.verify(encryptor, Mockito.atLeastOnce()).encrypt("password");
        Mockito.verify(dao, Mockito.atLeastOnce()).save(user);
    }

    @Test
    public void checkIfUserServiceWouldGetAllUsers() {
        userService.getAllUsers();

        Mockito.verify(dao, Mockito.atLeastOnce()).getAllUsers();
    }

    @Test
    public void checkIfUserServiceWouldRemoveUser() {
        User user = new User();

        userService.delete(user);

        Mockito.verify(dao, Mockito.atLeastOnce()).delete(user);
    }

    @Test
    public void checkIfUserServiceWouldUpdateUser() {
        User user = new User("user", "password");
        user.setFirstName("user");

        userService.update(user);

        Mockito.verify(encryptor, Mockito.atLeastOnce()).encrypt("password");
        Mockito.verify(dao, Mockito.atLeastOnce()).update(user);
    }

    @Test
    public void checkIfUserServiceWouldFindUser() {
        List<User> userList = new ArrayList<>(Arrays.asList(new User("user1", "password1"),
                new User("user2", "password2"), new User("user3", "password3")));
        User actualUser = new User("user2", "password2");
        User expectedUser = null;

        when(dao.getAllUsers()).thenReturn(userList);
        if(userService.find("user2").isPresent()){
            expectedUser = userService.find("user2").get();
        }
        assert expectedUser != null;

        Assert.assertEquals(actualUser.getLogin(), expectedUser.getLogin());
    }

    @Test(expectedExceptions = InvalidUserException.class)
    public void userServiceShouldThrowExceptionWhenTryToSaveInvalidUser() {
        User user = new User("user", "password");
        user.setFirstName("");

        userService.add(user);
    }
}
