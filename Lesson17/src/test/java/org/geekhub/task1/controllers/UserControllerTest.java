package org.geekhub.task1.controllers;

import com.google.gson.Gson;
import org.geekhub.task1.EncryptPassword;
import org.geekhub.task1.service.User;
import org.geekhub.task1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class UserControllerTest extends AbstractTestNGSpringContextTests {
    private Gson gson;
    @Autowired private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private EncryptPassword encryptor;

    @BeforeMethod
    public void setUp() {
        gson = new Gson().newBuilder().serializeNulls().create();
    }

    @Test
    public void listAsJsonReturned_when_callRestEndpoint() throws Exception {
        final String expected = gson.toJson( List.of(1, 2, 3));
        mockMvc.perform(get("/test"))
                .andExpect(status().isOk())
                .andExpect(content().json(expected, true));
    }

    @Test
    public void checkIfUserWouldBePutToSessionWhenExists() throws Exception {
        User testUser = new User("user", "user");
        final MockHttpSession session = new MockHttpSession();


        when(userService.find("user")).thenReturn(Optional.of(testUser));
        when(encryptor.encrypt("user")).thenReturn("user");
        mockMvc.perform(post("/check")
                .flashAttr("personForm", testUser)
                .session(session)
        );


        assertEquals(session.getAttribute("user"), testUser);
    }

    @Test
    public void checkIfUserWouldNOTBePutToSessionWhenPasswordDoesNotMatch() throws Exception {
        User testUser = new User("user", "user");
        final MockHttpSession session = new MockHttpSession();


        when(userService.find("user")).thenReturn(Optional.of(testUser));
        when(encryptor.encrypt("user")).thenReturn("pass");
        mockMvc.perform(post("/check")
                .flashAttr("personForm", testUser)
                .session(session)
        );

        assertNull(session.getAttribute("user"));
    }

    @Test
    public void checkIfLoginWouldBePutToSession() throws Exception {
        User testUser = new User("user", "user");
        final MockHttpSession session = new MockHttpSession();

        mockMvc.perform(get("/update")
                .flashAttr("userToChange", testUser)
                .session(session)
        );

        assertEquals(session.getAttribute("login"), "user");
    }



    @Test
    public void checkIfUserWouldBeRemovedFromService() throws Exception {
        User user = new User();

        mockMvc.perform(post("/delete")
                .flashAttr("userToChange", user));

        verify(userService, atLeastOnce()).delete(user);
    }

    @Test
    public void checkIfUserWouldBeAddedToService() throws Exception {
        User user = new User();

        mockMvc.perform(post("/add")
                .flashAttr("userToAdd", user));

        verify(userService, atLeastOnce()).add(user);
    }

    @Test
    public void dataFromSessionReturned_when_requestDataViaSessionAttribute() throws Exception {
        final MockHttpSession session = new MockHttpSession();
        session.setAttribute("login", "user");

        User user = new User();
        mockMvc.perform(post("/update")
                .flashAttr("userToChange", user)
                .param("login", "user"));
        when(userService.find("user")).thenReturn(Optional.of(user));

        Assert.assertEquals(user.getLogin(), "user");
    }

    @Test
    public void shouldReturnMessageWhenThrowCustomException() throws Exception {
        mockMvc.perform(get("/exception")).andExpect(content().string("Test invalid user exception!!!"));
    }


}

