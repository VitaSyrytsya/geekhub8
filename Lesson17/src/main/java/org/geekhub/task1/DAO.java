package org.geekhub.task1;

import org.geekhub.task1.exceptions.DaoException;
import org.geekhub.task1.service.User;
import org.springframework.beans.factory.annotation.Autowired;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class DAO {

    private final static String SQL_INSERT = "INSERT INTO Users (login, firstName, lastName, password, isAdmin) VALUES (?, ?, ?, ?, ?);";
    private final static String SQL_DELETE = "DELETE FROM Users WHERE Login =?";
    private final static String SQL_UPDATE = " UPDATE Users SET firstName = ?, lastName = ?, password = ?, isAdmin = ? WHERE login =?";
    private final static String SQL_AllUSERS = "SELECT * FROM USERS ORDER by firstName, login";
    private final static String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS USERS " +
            "(id int auto_increment, " +
            " login VARCHAR(255), " +
            " firstName VARCHAR(255), " +
            " lastName VARCHAR(255), " +
            " password VARCHAR(128), " +
            " isAdmin boolean, " +
            " PRIMARY KEY ( id ))";


    @Autowired
    private ConnectionPool connectionPool;

    public void save(User user) {
        Object[] values = {
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.isAdmin()
        };
        try (
                Connection connection = connectionPool.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_INSERT, true, values)
        ) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can not add user "+ e.getMessage());
        }
    }

    public void update(User user) {
        Object[] values = {
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.isAdmin(),
                user.getLogin()
        };
        try (
                Connection connection = connectionPool.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_UPDATE, false, values)
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DaoException("Updating user failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DaoException("Can not update user "+ e.getMessage());
        }
    }

    public void delete(User user) {
        Object[] values = {
                user.getLogin()
        };
        try (
                Connection connection = connectionPool.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_DELETE, false, values)
        ) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        }
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_AllUSERS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                users.add(buildUpUser(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        }
        return users;
    }

    public void createTableForDatabase(){
        try( Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_CREATE_TABLE)
       ) {
           statement.executeUpdate();
       } catch (SQLException e) {
          throw new DaoException(e.getMessage());
       }

    }

    private static User buildUpUser(ResultSet resultSet) throws SQLException {
        User user;
        int id = resultSet.getInt("id");
        String login = resultSet.getString("login");
        String firstName = resultSet.getString("firstName");
        String lastName = resultSet.getString("lastName");
        String password = resultSet.getString("password");
        boolean isAdmin = resultSet.getBoolean("isAdmin");

        user = new User(login, password);
        user.setAdmin(isAdmin);
        user.setFirstName(firstName);
        user.setLogin(login);
        user.setId(id);
        user.setLastName(lastName);
        return user;
    }

    private static PreparedStatement prepareStatement
            (Connection connection, String sql, boolean returnGeneratedKeys, Object... values)
            throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sql,
                returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        setValues(statement, values);
        return statement;
    }

    private static void setValues(PreparedStatement statement, Object... values)
            throws SQLException {
        for (int i = 0; i < values.length; i++) {
            statement.setObject(i + 1, values[i]);
        }
    }


}
