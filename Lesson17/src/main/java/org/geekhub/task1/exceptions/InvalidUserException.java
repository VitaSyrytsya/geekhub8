package org.geekhub.task1.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NO_CONTENT)
public class InvalidUserException extends RuntimeException {
    public InvalidUserException(String s){
        super(s);
    }
}
