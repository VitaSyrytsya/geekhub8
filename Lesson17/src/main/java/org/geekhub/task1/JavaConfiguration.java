package org.geekhub.task1;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;




@Configuration
@EnableWebMvc
@ComponentScan({"org.geekhub.task1"})
public class JavaConfiguration implements WebMvcConfigurer {

    @Bean(name = "databaseFiller")
    public DatabaseFillerOnStartUp getDatabase() {
        return new DatabaseFillerOnStartUp();
    }

    @Bean(name = "connectionPool")
    public ConnectionPool getConnectionPool() {
        return new ConnectionPool();
    }

    @Bean(name = "DAO")
    public DAO getDAO() {
        return new DAO();
    }

    @Bean(name = "passwordEncryptor")
    public EncryptPassword getPasswordEncryptor() {
        return new EncryptPassword();
    }








}

