package org.geekhub.task1;

import org.geekhub.task1.exceptions.DaoException;
import org.geekhub.task1.exceptions.InvalidUserException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;


@ControllerAdvice
@RestController
public class CustomExceptionHandler {
    @ExceptionHandler(value = {InvalidUserException.class, DaoException.class})
    public ResponseEntity<Object> showMessage(Exception userException) {
        return new ResponseEntity<>(userException.getMessage()+ "!!!", HttpStatus.BAD_REQUEST);
    }
}