package org.geekhub.task1.service;

public class User {

    private int id;
    private String login;
    private String firstName;
    private String lastName;
    private String password;
    private boolean isAdmin;

    public User() {

    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    @Override
    public String toString() {
        return "ID:" + id + " LOGIN: " + getLogin() +  " FIRST NAME: " + firstName + " LAST NAME : " + getLastName() +
                " IS ADMIN: " + isAdmin;
    }


}
