package org.geekhub.task1.service;

import org.geekhub.task1.DAO;
import org.geekhub.task1.EncryptPassword;
import org.geekhub.task1.exceptions.InvalidUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserServiceImpl implements UserService {

    private final DAO dao;
    private final EncryptPassword encryptor;

    @Autowired
    public UserServiceImpl(DAO dao, @Qualifier("passwordEncryptor") EncryptPassword encryptor) {
        this.dao = dao;
        this.encryptor = encryptor;
    }

    @Override
    public List<User> getAllUsers() {
        return dao.getAllUsers();
    }

    @Override
    public Optional<User> find(String login) {
        User userToFind = null;
        for (User user : dao.getAllUsers()) {
            if (login.equals(user.getLogin())) {
                userToFind = user;
            }
        }
        return Optional.ofNullable(userToFind);
    }

    @Override
    public void add(User user) {
        checkIfUserIsValid(user);
        user.setPassword(encryptor.encrypt(user.getPassword()));
        dao.save(user);
    }

    @Override
    public void delete(User user) {
        dao.delete(user);
    }

    @Override
    public void update(User user) {
        checkIfUserIsValid(user);
        user.setPassword(encryptor.encrypt(user.getPassword()));
        dao.update(user);
    }

    private void checkIfUserIsValid(User user) {
        if (user.getFirstName().isEmpty() || user.getLogin().isEmpty() ||
                user.getPassword().isEmpty()) {
            throw new InvalidUserException("User has got empty fields!");
        }
    }
}
