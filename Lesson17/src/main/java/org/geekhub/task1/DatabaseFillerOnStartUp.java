package org.geekhub.task1;

import org.geekhub.task1.service.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

public class DatabaseFillerOnStartUp implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private DAO dao;

    @Qualifier("passwordEncryptor")
    @Autowired
    private EncryptPassword encryptor;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        dao.createTableForDatabase();
        if (dao.getAllUsers().isEmpty()){
            String password = encryptor.encrypt("qwerty");
            User adminUser = new User("Carrie", password);
            adminUser.setAdmin(true);
            adminUser.setFirstName("Carrie");
            adminUser.setLastName("Bradshaw");

            dao.save(adminUser);
        }

    }
}
