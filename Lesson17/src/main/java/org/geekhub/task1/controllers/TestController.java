package org.geekhub.task1.controllers;

import org.geekhub.task1.exceptions.DaoException;
import org.geekhub.task1.exceptions.InvalidUserException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.List;

@Controller
public class TestController {

    @ResponseBody
    @GetMapping("test")
    public List<Integer> test() {
        return List.of(1, 2, 3);
    }

    @ResponseBody
    @GetMapping("exception")
    public void throwCustomException() {
        throw new DaoException("Test invalid user exception");
    }


}
