package org.geekhub.lesson5.task1;

import java.awt.*;
import java.util.Objects;

public class Car extends CarFactory {
    private String name;
    private int power;
    private String model;
    private Color color;

    public Car(String name, int power, String model, Color color) {
        this.name = name;
        this.power = power;
        this.model = model;
        this.color = color;
    }


    @Override
    public boolean equals(Object obj) {
        Car carr = (Car) obj;
        if (this == obj) {
            return true;

        }
        if (obj == null || getClass() != obj.getClass() ||
                power != carr.power || !color.equals(carr.color) || !model.equals(carr.model)) {
            return false;
        }

        return Objects.equals(name, carr.name) ||
                Objects.equals(model, carr.model) &&
                        Objects.equals(color, carr.color) &&
                        power == carr.power;
    }


    @Override
    public int hashCode() {
        int result;
        result = color != null ? color.hashCode() : 0;
        result = 31 * result + (model != null ? model.hashCode() : 0);
        return result;

    }
}
