package org.geekhub.lesson5.task2;

import java.util.Set;

public interface SetOperations<T> {
    boolean equals(T set1, T set2);

    Set<T> union(T set1, T set2);

    Set<T> subtract(T set1, T set2);

    Set<T> intersect(T set1, T set2);

    Set<T> symmetricSubtract(T set1, T set2);
}
