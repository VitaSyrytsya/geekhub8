package org.geekhub.lesson5.task3;

import java.util.Collection;
import java.util.Iterator;

public class VotingBox implements Collection<Vote> {

    private Vote[] candidate = new Vote[0];

    public VotingBox(int... n) {

    }

    @Override
    public int size() {
        return candidate.length;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof Vote) {
            for (Vote aCandidate : candidate) {
                if (aCandidate.x == (((Vote) o).x)) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    @Override
    public Iterator<Vote> iterator() {
        Iterator<Vote> vote = new Iterator<Vote>() {

            private int currentIndex = 0;
            private int currentSize = candidate.length;

            @Override
            public boolean hasNext() {
                return currentIndex < currentSize && candidate[currentIndex] != null;
            }

            @Override
            public Vote next() {
                return candidate[currentIndex++];
            }

            @Override
            public void remove() {
            }
        };
        return vote;
    }

    @Override
    public Object[] toArray() {
        return this.candidate;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Vote vote) {
        if (vote.x == 1) {
            int newArraySize = candidate.length + 2;
            Vote[] newArray = new Vote[newArraySize];
            System.arraycopy(candidate, 0, newArray, 0, candidate.length);
            newArray[candidate.length] = vote;
            newArray[candidate.length + 1] = vote;
            candidate = newArray;
            return true;
        } else if (vote.x == 0) {
            return false;
        } else {
            int newArraySize = candidate.length + 1;
            Vote[] newArray = new Vote[newArraySize];
            System.arraycopy(candidate, 0, newArray, 0, candidate.length);
            newArray[candidate.length] = vote;
            candidate = newArray;
            return true;
        }
    }

    @Override
    public boolean remove(Object o) {
        Vote vote = null;
        if (o instanceof Vote) {
            vote = (Vote) o;
        }
        for (int i = 0; i < candidate.length; i++) {
            if (candidate[i].x == vote.x) {
                Vote[] resultArray = new Vote[candidate.length - 1];
                for (int y = 0; y < resultArray.length; y++) {
                    for (int h = 0; h < candidate.length; h++) {
                        if (!candidate[h].equals(vote)) {
                            resultArray[y] = candidate[h];

                        }
                    }
                }
                candidate = resultArray;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Vote> c) {
        for (Vote vote : c) {
            if (vote.x == 1) {
                add(vote);
            } else if (vote.x == 0) {
            } else {
                add(vote);
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {


        Iterator iter = this.iterator();
        while (iter.hasNext()){
            Vote a = (Vote) iter.next();
        for(Vote vote: this){
            Iterator iter2 = c.iterator();
            while (iter2.hasNext()) {
                Object o = iter2.next();
                if(o instanceof Vote){
                    iter.remove();
            }
            }

            }

        }




        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Object[] collectionArray = c.toArray();
        Vote[] resultArray = new Vote[c.size()];
        for (int i = 0; i < c.size(); i++) {
            if (collectionArray[i] instanceof Vote) {
                resultArray[i] = (Vote) collectionArray[i];
            } else {
                return false;
            }
        }
        candidate = resultArray;
        return true;
    }

    @Override
    public void clear() {
        this.candidate = new Vote[0];

    }
}

