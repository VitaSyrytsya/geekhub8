package org.geekhub.lesson5.task4;

import java.util.Collections;
import java.util.LinkedList;


public class LinkedListCreatorImpl implements ListImplementation {
    LinkedList a;
    int size;

    public LinkedList create(int n) {
        this.size = n;
        int args[] = new int[size];
        this.a = new LinkedList(Collections.singleton(args));
        return a;
    }

    public long addToFirstElementIntoLinkedList(LinkedList b) {
        b = a;
        long nano_startTime = System.nanoTime();
        b.add(0, 1);
        long nano_endTime = System.nanoTime();
        long link1 = nano_endTime - nano_startTime;
        return link1;
    }

    public long addToTheCentreIntoLinkedList(LinkedList a) {
        this.a = a;
        long nano_startTime = System.nanoTime();
        a.add(0, 1);
        a.add(1, 1);
        a.add(2, 1);
        a.add(3, 1);
        a.add(4, 1);
        long nano_endTime = System.nanoTime();
        long arr2 = nano_endTime - nano_startTime;
        return arr2;
    }

    public long addToTheEndOfLinkedList(LinkedList a) {
        this.a = a;
        long nano_startTime = System.nanoTime();
        a.add(0, 1);
        a.add(1, 1);
        a.add(2, 1);
        a.add(3, 1);
        a.add(4, 1);
        a.add(5, 1);
        a.add(6, 1);
        a.add(7, 1);
        a.add(8, 1);
        a.add(9, 1);
        long nano_endTime = System.nanoTime();
        long arr2 = nano_endTime - nano_startTime;
        return arr2;
    }


    @Override
    public int addElement(int indexit, int value) {
        a.add(indexit, value);
        return (int) a.get(indexit);
    }


    @Override
    public int setElement(int indexit, int value) {

        a.set(indexit, value);
        return (int) a.get(indexit);
    }

    @Override
    public int getElement(int indexit, int value) {
        value = (int) a.get(indexit);
        return value;
    }

    @Override
    public int removeElement(int indexit, int value) {
        return 0;
    }
}
