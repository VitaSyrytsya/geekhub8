package org.geekhub.lesson5.task4;

import java.util.ArrayList;

public class ArrayListCreatorImpl extends ArrayList implements ListImplementation {
    int size;
    ArrayList arrayList;

    public ArrayList create(int size) {
        this.arrayList = new ArrayList(size);
        return arrayList;
    }

    public long addToFirstElementIntoArrayList(ArrayList a) {
        a = arrayList;
        long nano_startTime = System.nanoTime();
        a.add(0, 1);
        long nano_endTime = System.nanoTime();
        long arr1 = nano_endTime - nano_startTime;
        return arr1;
    }

    public long addToTheCentreIntoArrayList(ArrayList a) {
        a = arrayList;
        long nano_startTime = System.nanoTime();
        a.add(0, 1);
        a.add(1, 1);
        a.add(2, 1);
        a.add(3, 1);
        a.add(4, 1);
        long nano_endTime = System.nanoTime();
        long arr2 = nano_endTime - nano_startTime;
        return arr2;
    }

    public long addToTheEndOfArrayList(ArrayList a) {
        a = arrayList;
        long nano_startTime = System.nanoTime();
        a.add(0, 1);
        a.add(1, 1);
        a.add(2, 1);
        a.add(3, 1);
        a.add(4, 1);
        a.add(5, 1);
        a.add(6, 1);
        a.add(7, 1);
        a.add(8, 1);
        a.add(9, 1);

        long nano_endTime = System.nanoTime();
        long arr2 = nano_endTime - nano_startTime;
        return arr2;

    }

    public int addElement(int indexit, int value) {
        arrayList.add(indexit, value);
        return (int) arrayList.get(indexit);
    }


    @Override
    public int setElement(int indexit, int value) {
        arrayList.set(indexit, value);
        return (int) arrayList.get(indexit);
    }

    @Override
    public int getElement(int indexit, int value) {
        value = (int) arrayList.get(indexit);
        return value;
    }

    @Override
    public int removeElement(int placeIndex, int value) {
        return 0;
    }


}
