package org.geekhub.lesson5.task4;


public interface ListImplementation {
    int addElement(int indexit, int value);

    int setElement(int indexit, int value);

    int getElement(int indexit, int value);

    int removeElement(int indexit, int value);
}
