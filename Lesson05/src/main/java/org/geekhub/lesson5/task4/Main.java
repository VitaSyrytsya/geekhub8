package org.geekhub.lesson5.task4;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

public class Main {
    public static void main(String[] args) {
        System.out.println("Adding elements into Array list:");

        ArrayListCreatorImpl a = new ArrayListCreatorImpl();
        ArrayList b;
        b = a.create(10);
        System.out.println("Time for adding 1st element into ArrayList:" + a.addToFirstElementIntoArrayList(b));
        System.out.println("Time for adding 5 element into ArrayList:" + a.addToTheCentreIntoArrayList(b));
        System.out.println("Time for adding last element into ArrayList:" + a.addToTheEndOfArrayList(b));
        System.out.println();


        System.out.println("Adding elements into Linked list:");
        LinkedListCreatorImpl h = new LinkedListCreatorImpl();
        LinkedList c;
        c = h.create(10);
        System.out.println("Time for adding 1st element into LinkedList:" + h.addToFirstElementIntoLinkedList(c));
        System.out.println("Time for adding 5 element into LinkedList:" + h.addToTheCentreIntoLinkedList(c));
        System.out.println("Time for adding last element into LinkedList:" + h.addToTheEndOfLinkedList(c));
        System.out.println();


        System.out.println("Adding elements into Vector:");
        VectorCreatorImpl z = new VectorCreatorImpl();
        Vector zz;
        zz = z.create(10);
        System.out.println("Time for adding 1st element into Vector:" + z.addToFirstElementIntoVector(zz));
        System.out.println("Time for adding 5 element into Vector:" + z.addToTheCentreIntoVector(zz));
        System.out.println("Time for adding last element into Vector:" + z.addToTheEndOfVector(zz));
        System.out.println();


        System.out.println("Getting elements from Array list:");
        a.addToTheEndOfArrayList(b);
        long arr = System.nanoTime();
        b.get(0);
        long arr2 = System.nanoTime();
        System.out.println("Time for getting first element of arrayList:" + (arr2 - arr));
        b.get(5);
        long arr3 = System.nanoTime();
        System.out.println("Time for getting 5 element of arrayList:" + (arr3 - arr2));
        b.get(9);
        long arr4 = System.nanoTime();
        System.out.println("Time for getting last element of arrayList:" + (arr4 - arr3));
        System.out.println();


        System.out.println("Getting elements from Linked list:");
        h.addToTheEndOfLinkedList(c);
        long link = System.nanoTime();
        c.get(0);
        long link2 = System.nanoTime();
        System.out.println("Time for getting first element of linkedList:" + (link2 - link));
        c.get(5);
        long link3 = System.nanoTime();
        System.out.println("Time for getting 5 element of linkedList:" + (link3 - link2));
        c.get(9);
        long link4 = System.nanoTime();
        System.out.println("Time for getting last element of linkedList:" + (link4 - link3));
        System.out.println();


        System.out.println("Getting elements from Vector:");
        z.addToTheEndOfVector(zz);
        long vect = System.nanoTime();
        zz.get(0);
        long vect2 = System.nanoTime();
        System.out.println("Time for getting first element of Vector:" + (vect2 - vect));
        zz.get(5);
        long vect3 = System.nanoTime();
        System.out.println("Time for getting 5 element of Vector:" + (vect3 - vect2));
        zz.get(9);
        long vect4 = System.nanoTime();
        System.out.println("Time for getting last element of Vector:" + (vect4 - vect3));


    }

}




