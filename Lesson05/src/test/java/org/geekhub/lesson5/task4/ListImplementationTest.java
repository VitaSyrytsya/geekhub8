package org.geekhub.lesson5.task4;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.DataProvider;


import java.util.*;

public class ListImplementationTest {
    @BeforeMethod
    public void setUp() {
        ListImplementation ListImplementation = new ArrayListCreatorImpl();

    }

    @Test(dataProvider = "arrayListTestData")
    public void addToArrayList(int indexit, int indexit1, int value, int expectedResult) {
        ArrayListCreatorImpl a = new ArrayListCreatorImpl();
        ArrayList test = a.create(10);
        int result = a.addElement(indexit, value);
        int result1 = a.addElement(indexit1, value);
        Assert.assertEquals(result, expectedResult);
        Assert.assertEquals(result1, expectedResult);
    }

    @Test(dataProvider = "arrayListTestData2")
    public void
    setToArrayList(int indexit, int value, int value1, int expectedResult) {
        ArrayListCreatorImpl a = new ArrayListCreatorImpl();
        ArrayList test = a.create(10);
        a.addElement(indexit, value);
        int result = a.setElement(indexit, value1);
        Assert.assertEquals(result, expectedResult);
    }

    @Test(dataProvider = "arrayListTestData")
    public void getFromArrayList(int indexit, int value, int value1, int expectedResult) {
        ArrayListCreatorImpl a = new ArrayListCreatorImpl();
        ArrayList test = a.create(10);
        a.addElement(indexit, value);
        int result = a.getElement(indexit, value);
        Assert.assertEquals(result, expectedResult);
    }

    @Test(dataProvider = "arrayListTestData3")
    public void removeFromArrayList(int indexit, int indexit1, int value, int expectedResult) {
        ArrayListCreatorImpl a = new ArrayListCreatorImpl();
        ArrayList test = a.create(10);
        a.addElement(indexit, value);
        int result = a.removeElement(indexit, value);
        Assert.assertEquals(result, expectedResult);
    }

    @Test(dataProvider = "arrayListTestData2")
    public void addToLinkedList(int indexit, int indexit1, int value, int expectedResult) {
        LinkedListCreatorImpl a = new LinkedListCreatorImpl();
        LinkedList test = a.create(10);
        int result = a.addElement(indexit, value);
        int result1 = a.addElement(indexit1, value);
        Assert.assertEquals(result, expectedResult);
        Assert.assertEquals(result1, expectedResult);
    }

    @Test(dataProvider = "arrayListTestData2")
    public void setToLinkedList(int indexit, int value, int value1, int expectedResult) {
        LinkedListCreatorImpl a = new LinkedListCreatorImpl();
        LinkedList test = a.create(10);
        a.addElement(indexit, value);
        int result = a.setElement(indexit, value1);
        Assert.assertEquals(result, expectedResult);
    }

    @Test(dataProvider = "arrayListTestData")
    public void getFromLinkedList(int indexit, int value, int value1, int expectedResult) {
        LinkedListCreatorImpl a = new LinkedListCreatorImpl();
        LinkedList test = a.create(10);
        a.addElement(indexit, value);
        int result = a.getElement(indexit, value);
        Assert.assertEquals(result, expectedResult);
    }
    @Test(dataProvider = "arrayListTestData3")
    public void removeFromLinkedList(int indexit, int indexit1, int value, int expectedResult) {
        LinkedListCreatorImpl a = new LinkedListCreatorImpl();
        LinkedList test = a.create(10);
        a.addElement(indexit, value);
        int result = a.removeElement(indexit, value);
        Assert.assertEquals(result, expectedResult);
    }
    @Test(dataProvider = "arrayListTestData")
    public void addToVector(int indexit, int indexit1, int value, int expectedResult) {
       VectorCreatorImpl a = new VectorCreatorImpl();
        Vector test = a.create(10);
        int result = a.addElement(indexit, value);
        int result1 = a.addElement(indexit1, value);
        Assert.assertEquals(result, expectedResult);
        Assert.assertEquals(result1, expectedResult);
    }

    @Test(dataProvider = "arrayListTestData2")
    public void setToVector(int indexit, int value, int value1, int expectedResult) {
        VectorCreatorImpl a = new VectorCreatorImpl();
        Vector test = a.create(10);
        a.addElement(indexit, value);
        int result = a.setElement(indexit, value1);
        Assert.assertEquals(result, expectedResult);
    }
    @Test(dataProvider = "arrayListTestData")
    public void getFromVector(int indexit, int value, int value1, int expectedResult) {
       VectorCreatorImpl a = new VectorCreatorImpl();
        Vector test = a.create(10);
        a.addElement(indexit, value);
        int result = a.getElement(indexit, value);
        Assert.assertEquals(result, expectedResult);
    }
    @Test(dataProvider = "arrayListTestData3")
    public void removeFromVector(int indexit, int indexit1, int value, int expectedResult) {
        VectorCreatorImpl a = new VectorCreatorImpl();
        Vector test = a.create(10);
        a.addElement(indexit, value);
        int result = a.removeElement(indexit, value);
        Assert.assertEquals(result, expectedResult);
    }

    @DataProvider
    private static Object[][] arrayListTestData() {
        return new Object[][]{
                {0, 1, 1, 1},
        };
    }

    @DataProvider
    private static Object[][] arrayListTestData2() {
        return new Object[][]{
                {0, 2, 14, 14},
        };
    }

    @DataProvider
    private static Object[][] arrayListTestData3() {
        return new Object[][]{
                {0, 1, 3, 0},
        };
    }

}
