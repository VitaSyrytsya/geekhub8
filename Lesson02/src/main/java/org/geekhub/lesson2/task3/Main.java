package org.geekhub.lesson2.task3;

public class Main {
    public static void main(String[]args){
        SystemInfoProvider prov = new SystemInfoProvider();

        System.out.println("Amount Of CPU cores: " + prov.getProcessorsCount());
        System.out.println("Free Ram: " +  prov.getFreeRam()/prov.Bytes_In_Megabyte + " megabytes");
        System.out.println("Total Ram: " +  prov.getTotalRam()/prov.Bytes_In_Megabyte + " megabytes");
        System.out.println("Version of Java: " +  prov.getJavaVersion() );
        System.out.println("User Name: " +  prov.getUserName() );
        System.out.println("User home directory: " +  prov.getUserHomeDirectory());
        System.out.println("OS name: " +  prov.getOSName());
        System.out.println("Hard drive total space: " +  prov.hardDriveMemoryTotal());
        System.out.print("Hard drive free space: " +  prov.hardDriveMemoryFree());
    }

}
