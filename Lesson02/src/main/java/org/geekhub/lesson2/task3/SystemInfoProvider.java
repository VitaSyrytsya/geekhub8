package org.geekhub.lesson2.task3;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.System;

public class SystemInfoProvider {
    public static final int Bytes_In_Megabyte = 1024 * 1024;


    public int getProcessorsCount() {
        int cores = Runtime.getRuntime().availableProcessors();
        return cores;
    }

    public long getFreeRam() {
        long freeRam = ((com.sun.management.OperatingSystemMXBean) ManagementFactory
                .getOperatingSystemMXBean()).getFreePhysicalMemorySize();
        return freeRam;
    }


    public long getTotalRam() {
        long totalRam = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
                .getTotalPhysicalMemorySize();
        return totalRam;
    }


    public int getJavaVersion() {
        String u = System.getProperty("java.version");
        int javaVersion = Integer.parseInt(u);
        return javaVersion;
    }

    public String getUserName() {
        String userName = System.getProperty("user.name");
        return userName;
    }

    public String getUserHomeDirectory() {
        String d = System.getProperty("user.home");
        return d;
    }

    public String getOSName() {
        String k = System.getProperty("os.name");
        return k;
    }

    public File[] hardDriveMemoryTotal() {
        File file = new File("c:");
        File[] totalSpace = file.listRoots();
        return totalSpace;
    }

    public long hardDriveMemoryFree() {
        File file = new File("c:");
        long freeSpace = file.getFreeSpace();
        return freeSpace / Bytes_In_Megabyte;
    }

}
