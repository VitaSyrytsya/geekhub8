package org.geekhub.lesson2.task4;
//import com.sun.jdi.IntegerValue;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class PolynomialSolver {

    public static Optional<Set<Integer>> findRoots(int i, int i1, int i2)
    {
        Set<Integer> roots = new HashSet<Integer>();
        double discriminator = Math.pow(i1,2) - 4*i*i2;
        if(discriminator<0)
        {
            return(Optional.empty());
        }
        if(discriminator>0)
        {
            int x1 = (int)(-i1 + Math.sqrt(discriminator))/2*i;
            int x2 =  (int)(-i1 - Math.sqrt(discriminator))/2*i;
            roots.add(x1);
            roots.add(x2);
            return Optional.of(roots);
        }
        if(discriminator ==0)
        {
            int x = -i1/(2*i);
            roots.add(x);
        }
        return Optional.of(roots);
    }

}
