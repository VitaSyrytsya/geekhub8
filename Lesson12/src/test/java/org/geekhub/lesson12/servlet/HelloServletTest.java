package org.geekhub.lesson12.servlet;



import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.mockito.Mockito;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;


public class HelloServletTest extends Mockito {
    private HelloServlet helloServlet;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;
    private PrintWriter printWriter;

    @BeforeMethod
    public void setUp() {
        helloServlet = new HelloServlet();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        StringWriter stringWriter = new StringWriter();
        printWriter = new PrintWriter(stringWriter);
    }


    @Test
    public void addAttribute() throws IOException {
        when(response.getWriter()).thenReturn(printWriter);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("Add");
        when(request.getParameter("username")).thenReturn("someAttribute");
        when(request.getParameter("value")).thenReturn("someValue");
        helloServlet.doPost(request, response);

        verify(session, atLeast(1)).setAttribute("someAttribute", "someValue");
    }

    @Test
    public void updateAttribute() throws IOException {
        when(response.getWriter()).thenReturn(printWriter);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("Update");
        when(request.getParameter("username")).thenReturn("someAttribute");
        when(request.getParameter("value")).thenReturn("value");
        helloServlet.doPost(request, response);

        verify(session, atLeast(1)).removeAttribute("someAttribute");
        verify(session, atLeast(1)).setAttribute("someAttribute", "value");
    }

    @Test
    public void removeAttribute() throws IOException {
        when(response.getWriter()).thenReturn(printWriter);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("Remove");
        when(request.getParameter("username")).thenReturn("someAttribute");
        when(request.getParameter("value")).thenReturn("value");
        helloServlet.doPost(request, response);

        verify(session, atLeast(1)).removeAttribute("someAttribute");
    }

    @Test
    public void invalidateSession() throws IOException {
        when(response.getWriter()).thenReturn(printWriter);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("Invalidate");
        helloServlet.doPost(request, response);

        verify(session, atLeast(1)).invalidate();

    }


}
