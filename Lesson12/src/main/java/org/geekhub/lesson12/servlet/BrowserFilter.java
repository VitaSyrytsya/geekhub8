package org.geekhub.lesson12.servlet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@WebFilter(urlPatterns = {"/*"})
public class BrowserFilter implements Filter {


    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;

        String userAgent = httpServletRequest.
                getHeader("User-Agent").
                toLowerCase();
        if (userAgent.contains("edge")) {
            throw new RuntimeException("Your browser does not support this operation!");
        }
        chain.doFilter(req, resp);
    }




}
