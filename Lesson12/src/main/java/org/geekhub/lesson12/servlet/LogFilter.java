package org.geekhub.lesson12.servlet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.logging.Logger;

@WebFilter(urlPatterns = {"/*"})
public class LogFilter implements Filter {
    private double time =  System.nanoTime();
    private final static Logger log = Logger.getLogger(LogFilter.class.getName());
    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        log.info(String.valueOf(time- System.nanoTime()));
        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }
}
