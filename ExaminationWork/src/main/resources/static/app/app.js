
angular.module('ExaminationWork', [ 'ui.router', 'ngCookies', 'ngStorage' ])

	.run(function(AuthService, $rootScope, $state, $cookies, $http) {

	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

		if ($cookies.get('token')==null) {

			if (toState.name !== 'login' && toState.name !== 'register'&&
			toState.name !=='forgotPassword'&&toState.name !=='chooseDoctor') {
				event.preventDefault();
				$state.go('login');
			}
		} else {
			$http({
				method: 'GET',
				url: 'getUser',
				headers: {
					'Authorization': 'Bearer ' + $cookies.get('token')
				}
			}).success(function (response) {
				$rootScope.user = response;
			}).error(function () {
				$cookies.remove('token');
				$rootScope.user = null;
			});


			if (toState.data && toState.data.role) {
				var hasAccess = false;
				for (var i = 0; i < $rootScope.user.roles.length; i++) {

					var role = $rootScope.user.roles[i];
					if (toState.data.role === role) {
						hasAccess = true;
						break;
					}
				}
				if (!hasAccess) {
					event.preventDefault();
					$state.go('access-denied');
				}

			}
		}
	});
});