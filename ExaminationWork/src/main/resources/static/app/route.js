angular.module('ExaminationWork').config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/page-not-found');

    $stateProvider.state('nav', {
        abstract: true,
        url: '',
        views: {
            'nav@': {
                templateUrl: 'app/views/nav.html',
                controller: 'NavController'
            }
        }
    }).state('login', {
        parent: 'nav',
        url: '/login',
        views: {
            'content@': {
                templateUrl: 'app/views/login.html',
                controller: 'LoginController'
            }
        }
    }).state('users', {
        parent: 'nav',
        url: '/users',
        data: {
            role: 'ROLE_ADMIN'
        },
        views: {
            'content@': {
                templateUrl: 'app/views/users.html',
                controller: 'UsersController',
            }
        }
    }).state('home', {
        parent: 'nav',
        url: '/',
        views: {
            'content@': {
                templateUrl: 'app/views/home.html',
                controller: 'HomeController'
            }
        }
    }).state('page-not-found', {
        parent: 'nav',
        url: '/page-not-found',
        views: {
            'content@': {
                templateUrl: 'app/views/page-not-found.html',
                controller: 'PageNotFoundController'
            }
        }
    }).state('access-denied', {
        parent: 'nav',
        url: '/access-denied',
        views: {
            'content@': {
                templateUrl: 'app/views/access-denied.html',
                controller: 'AccessDeniedController'
            }
        }
    }).state('dialogs', {
        parent: 'nav',
        url: '/dialogs',
        views: {
            'content@': {
                templateUrl: 'app/views/dialogs.html',
                controller: 'DialogController'
            }
        }
    }).state('messages', {
        parent: 'nav',
        url: '/messages',
        params: {
            dialog: null,
        },
        views: {
            'content@': {
                templateUrl: 'app/views/messages.html',
                controller: 'MessagingController'
            }
        }
    }).state('create-doctor', {
        parent: 'nav',
        url: '/createDoctor',
        data: {
            role: 'ROLE_ADMIN'
        },
        views: {
            'content@': {
                templateUrl: 'app/views/create-doctor.html',
                controller: 'CreateDoctorController'
            }
        }
    }).state('forgotPassword', {
            parent: 'nav',
            url: '/forgotPassword',
            views: {
                'content@': {
                    templateUrl: 'app/views/forgot-password.html',
                    controller: 'ForgotPasswordController'
                }
            }
        }).state('chooseDoctor', {
        parent: 'nav',
        url: '/choose',
        views: {
            'content@': {
                templateUrl: 'app/views/choose-doctor.html',
                controller: 'ChooseDoctorController'
            }
        }
    })
        .state('viewProfile', {
            parent: 'nav',
            url: '/profile',
            params: {
               id: null,
            },
            views: {
                'content@': {
                    templateUrl: 'app/views/home.html',
                    controller: 'ProfileController'
                }
            }
        })
        .state('register', {
        parent: 'nav',
        url: '/register',
        views: {
            'content@': {
                templateUrl: 'app/views/register.html',
                controller: 'RegisterController'
            }
        }
    });
});
