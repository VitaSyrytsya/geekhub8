angular.module('ExaminationWork')
    .controller('ChooseDoctorController', function($http, $scope, $cookies, $rootScope, $state, $localStorage) {



     var init = function(){
          $http({
              method: 'GET',
              url: 'doctor/allDoctors',
              headers: {
                  'Authorization': 'Bearer ' + $cookies.get('token')
              }
          }).success(function (res) {
              $scope.users = res;
              $scope.order=null;
              $scope.speciality= null;
          });
      };

        init();

     var sortAllDoctors = function(){
         $http({
             method: 'GET',
             url: 'doctor/allDoctorsOrdered',
             headers: {
                 'Authorization': 'Bearer ' + $cookies.get('token')
             }
         }).success(function(res) {
             $scope.users = res;
             $scope.order=null;
             $scope.speciality= null;
         });
     };

        $scope.specialities = [
            {speciality: "ALLERGY"},
            {speciality:  "IMMUNOLOGY"},
            {speciality : "ANESTHESIOLOGY"},
            {speciality:  "DERMATOLOGY"},
            {speciality:  "DIAGNOSTIC_RADIOLOGY"},
            {speciality:  "EMERGENCY_MEDICINE"},
            {speciality:  "MEDICAL_GENETICS"},
            {speciality:  "RADIATION_ONCOLOGY"}
        ];

        $scope.send = function(){
            if($scope.speciality ==null && $scope.order!=null ){
                sortAllDoctors();
            }else if($scope.speciality==='ALL DOCTORS'&&$scope.order==null){
               init();
            }else if($scope.order ==null&&$scope.speciality !=null){
                $http({
                    method: 'GET',
                    url: 'doctor/allDoctors/'+ $scope.speciality,
                    headers: {
                        'Authorization': 'Bearer ' + $cookies.get('token')
                    }
                }).success(function(res) {
                    $scope.users = res;
                    $scope.order=null;
                    $scope.speciality= null;
                });
            }else if($scope.order!=null && $scope.speciality!==null
                &&$scope.speciality !=='ALL DOCTORS'){
                $http({
                    method: 'GET',
                    url: 'doctor/allDoctorsOrdered/'+ $scope.speciality,
                    headers: {
                        'Authorization': 'Bearer ' + $cookies.get('token')
                    }
                }).success(function(res) {
                    $scope.users = res;
                    $scope.order=null;
                    $scope.speciality= null;
                });
            }else if($scope.order!=null&& $scope.speciality ==='ALL DOCTORS'){
                sortAllDoctors();
            }
        };

        $scope.viewProfile = function(id){
            $localStorage.id = id;
            $state.go('viewProfile', {id:id});
        }

    });
