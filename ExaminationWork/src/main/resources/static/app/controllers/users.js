angular.module('ExaminationWork')

    .controller('UsersController', function ($http, $scope, $cookies) {

        var edit = false;

        $scope.buttonText = 'Create';

        var init = function () {
            $http({
                method: 'GET',
                url: 'user/getNoDoctors',
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('token')
                }
            }).success(function (res) {
                $scope.users = res;
                $scope.userForm.$setPristine();
                $scope.message = '';
                $scope.appUser = null;
                $scope.buttonText = 'Create';

            }).error(function (error) {
                $scope.message = error.message;
            });
        };
        $scope.initEdit = function (appUser) {
            edit = true;
            $scope.appUser = appUser;

            $scope.message = '';
            $scope.buttonText = 'Update';
        };
        $scope.initAddUser = function () {
            edit = false;
            $scope.appUser = null;
            $scope.userForm.$setPristine();
            $scope.message = '';
            $scope.buttonText = 'Create';
        };
        $scope.deleteUser = function (appUser) {
            $http.delete('user/delete/' + appUser.id, {
                withCredentials: true,
                headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
            }).success(function (res) {
                $scope.deleteMessage = "Success!";
                init();
            }).error(function (error) {
                $scope.deleteMessage = "You cannot delete default user account!";
            });
        };
        var editUser = function () {
            $http.put('user/update', $scope.appUser, {
                withCredentials: true,
                headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
            }).success(function (res) {
                $scope.appUser = null;
                $scope.confirmPassword = null;
                $scope.userForm.$setPristine();
                $scope.message = "Successfully updated!";
                init();
            }).error(function (error) {
                $scope.message = error.message;
            });
        };
        var addUser = function () {
            $http.post('user/create', $scope.appUser, {
                withCredentials: true,
                headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
            }).success(function () {
                $scope.appUser = null;
                $scope.confirmPassword = null;
                $scope.userForm.$setPristine();
                $scope.message = "User Successfully Created!";
                init();
            }).error(function (error) {
                $scope.message = error.message;
            });
        };
        $scope.submit = function () {
            if (edit) {
                editUser();
            } else {
                addUser();
            }
        };
        init();
    });
