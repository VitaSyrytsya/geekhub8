angular.module('ExaminationWork')

    .controller('ProfileController', function ($cookies, $http, $scope, $state, $localStorage, $rootScope) {
        const commentArea = document.querySelector('#commentArea');
        const commentInput = document.querySelector('#comment');
        const commentForm = document.querySelector('#commentForm');


        refreshComments();


        function sendComment() {
            var commentContent = document.getElementById("comment").value;
            var comment = {
                content: commentContent
            };
            $http.post('comment/save/' + $localStorage.id, comment,
                {
                    withCredentials: true,
                    headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
                }
            ).success(function (res) {
                refreshComments();
            }).error(function (error) {

            });

        }

        $scope.deleteComment = function (id) {

            $http.delete('comment/delete/' + id,
                {
                    withCredentials: true,
                    headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
                }
            ).success(function (res) {
                refreshComments();
            }).error(function (error) {

            });
        };



        commentForm.addEventListener('submit', sendComment, true);


        function refreshComments() {
            document.getElementById("comment").value= '';
            $http({
                method: 'GET',
                url: 'comment/getAll/' + $localStorage.id,
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('token')
                }
            }).success(function (res) {
                $scope.comments = res;


            }).error(function () {
                    $state.go('login');
                }
            );
        }

        $http({
            method: 'GET',
            url: 'doctor/get/' + $localStorage.id,
            headers: {
                'Authorization': 'Bearer ' + $cookies.get('token')
            }
        }).success(function (res) {
            $scope.user = res;
        }).error(function () {
                $state.go('login');
            }
        );

        $http({
            method: 'GET',
            url: 'getUser',
            headers: {
                'Authorization': 'Bearer ' + $cookies.get('token')
            }
        }).success(function (res) {
            $scope.currentUser = res;
            $localStorage.currentUser = res;
        }).error(function () {
                $state.go('login');
            }
        );

        $scope.rating = 0;
        $http({
            method: 'GET',
            url: 'doctor/get/' + $localStorage.id,
            headers: {
                'Authorization': 'Bearer ' + $cookies.get('token')
            }
        }).success(function (res) {
            $cookies.put('currentRating',res.doctor.rating.averageValue );
            $scope.ratings = [{
                current: $cookies.get('currentRating'),
                max: 10
            }];

        }).error(function () {
                $state.go('login');
            }
        );

        $scope.sendMessage = function (id) {
            $http({
                method: 'GET',
                url: 'findDialog/' + id,
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('token')
                }
            }).success(function (res) {
                $cookies.put('dialog', res.id);
                $cookies.put('receiver', $scope.user.username);
                $cookies.put('sender', $scope.currentUser.username);
                $state.go('messages', {dialog: res});

            }).error(function () {
                $state.go('dialogs');
                }
            );
        };

        $scope.getSelectedRating = function (rating) {
            console.log(rating);
        }
    })
    .directive('starRating', function ($localStorage, $cookies, $http) {
            return {
                restrict: 'A',
                template: '<ul class="rating">' +
                    '<li ng-repeat="star in stars"   ng-class="star " ng-click="toggle($index)">' +
                    '\u2605' +

                    '</li>' +
                    '</ul>',
                scope: {
                    ratingValue: '=',
                    max: '=',
                    onRatingSelected: '&'
                },
                link: function (scope, elem, attrs) {

                    var updateStars = function (rating) {
                        scope.stars = [];
                        $http({
                            method: 'POST',
                            url: 'rating/set/' + $localStorage.id+ '/'+ $localStorage.currentUser.id,
                            params:{
                                value:  rating
                            },
                            headers: {
                                'Authorization': 'Bearer ' + $cookies.get('token')
                            }
                        }).success(function (res) {
                            for (var i = 0; i < scope.max; i++) {
                                scope.stars.push({
                                    filled: i < res.averageValue
                                });
                            }
                        }).error(function () {

                            }
                        );


                    };

                    var showStars = function () {
                        scope.stars = [];

                            for (var i = 0; i < scope.max; i++) {
                                scope.stars.push({
                                    filled: i < $cookies.get('currentRating')
                                });
                            }
                    };

                    scope.toggle = function (index) {
                        console.log('rating' + index+1);
                       var rating = index + 1;
                       updateStars(rating)

                    };



                    scope.$watch('ratingValue', function (oldVal, newVal) {
                        if (newVal) {
                            showStars();
                        }
                    });
                }
            }
        }
    );