angular.module('ExaminationWork')
    .controller('DialogController', function( $scope, $cookies, $http, $state,  $rootScope) {

        $scope.selectedRow = null;

        var init = function(){
            $http({
                method: 'GET',
                url: 'dialogs',
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('token')
                }
            }).success(function (response) {
                $scope.dialogs = response;
            }).error(function () {


            });
        };
        init();


        $scope.curPage = 0;
        $scope.pageSize = 7;
        $scope.numberOfPages = function() {
            return Math.ceil($scope.dialogs.length / $scope.pageSize);
        };

        $scope.setClickedRow = function(dialog){
           $rootScope.$broadcast('LoginSuccessful');
            $cookies.put('dialog', dialog.id);
            $cookies.put('receiver', dialog.lastMessage.receiver);
            $cookies.put('sender', dialog.lastMessage.sender);
            $state.go('messages', {dialog:dialog});
        }

    });

angular.module('ExaminationWork').filter('startPagination', function() {
    return function(input, start) {
        start = +start;
        return input.slice(start);
    };

});