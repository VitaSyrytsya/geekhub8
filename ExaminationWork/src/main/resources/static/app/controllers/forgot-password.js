angular.module('ExaminationWork')

    .controller('ForgotPasswordController', function($http, $scope, $state,$cookies, AuthService, $rootScope) {
        $scope.recoverUser = function() {
            $http({
                url: 'recover',
                method: "POST",
                params: {
                    passphrase: $scope.passphrase,
                    password: $scope.password
                }
            }).success(function () {
                $rootScope.$broadcast('LoginSuccessful');
                $state.go('home');

            }).error(function (error) {
                $scope.password = null;

            });
        };
    });

