angular.module('ExaminationWork')

	.controller('RegisterController', function($http, $scope, $state, $rootScope) {
		$scope.submit = function() {
			$http.post('register', $scope.appUser).success(function(res) {
				$scope.appUser = null;
				$scope.confirmPassword = null;
				$scope.register.$setPristine();
				$rootScope.$broadcast('LoginSuccessful');
				$state.go('home');
			}).error(function(error) {
				$scope.message = error.message;
			});
		};
	});
