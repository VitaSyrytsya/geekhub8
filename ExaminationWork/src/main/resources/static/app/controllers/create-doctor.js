angular.module('ExaminationWork')

    .controller('CreateDoctorController', function($http, $scope, $cookies, $rootScope) {
        var edit = false;
        $scope.buttonText = 'Create';
        var init = function() {
            $http({
                method: 'GET',
                url: 'doctor/allUsers',
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('token')
                }
            }).success(function(res) {
                $scope.users = res;
                $scope.userForm.$setPristine();
                $scope.message='';
                $scope.appUser = null;
                $scope.doctor = null;
                $scope.buttonText = 'Create';

            }).error(function(error) {
                $scope.message = error.message;
            });
        };
        $scope.initEdit = function(appUser) {
            edit = true;
            $scope.appUser = appUser;
            $scope.message='';
            $scope.doctor = null;
            $scope.buttonText = 'Update';
        };

        $scope.deleteUser = function(appUser) {
            $http.delete('doctor/delete/'+appUser.id,
                {
                    withCredentials: true,
                    headers:{ 'Authorization':  'Bearer ' + $cookies.get('token')}
                })
                .success(function(res) {
                $scope.deleteMessage ="Successfully deleted!";
                init();
            }).error(function(error) {
                $scope.deleteMessage = "Current user is not a doctor!";
            });
        };

        var addUser = function(){
            $http.post('doctor/create/'+ $scope.appUser.id , $scope.doctor,
                {
                    withCredentials: true,
                    headers:{ 'Authorization':  'Bearer ' + $cookies.get('token')}
                }
                ).success(function(res) {
                    $scope.appUser = null;
                $scope.confirmPassword = null;
                $scope.userForm.$setPristine();
                $scope.message = "User Updated to Doctor!";
                init();
            }).error(function(error) {
                $scope.message = error.message;
            });
        };
        $scope.submit = function() {
            if(edit){
                addUser();
            }
        };
        init();

    });
