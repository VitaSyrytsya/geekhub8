angular.module('ExaminationWork')
    .controller('MessagingController', function ($scope, $cookies, $http,$state, $rootScope, $stateParams,$window) {

        const messageForm = document.querySelector('#messageForm');
        const messageInput = document.querySelector('#message');
        const messageArea = document.querySelector('#messageArea');
        let stompClient = null;

        connect();

        $http({
            method: 'GET',
            url: 'getUser',
            headers: {
                'Authorization': 'Bearer ' + $cookies.get('token')
            }
        }).success(function (response) {
            $rootScope.username = response.username;
        }).error(function () {

        });

        const participants = [
            $cookies.get('receiver'),
            $cookies.get('sender'),
        ];


        function connect() {
            const socket = new SockJS('/ws');
            stompClient = Stomp.over(socket);
            stompClient.connect({}, onConnected, onError);
        }


        function onConnected() {
            participants.sort();
            stompClient.subscribe('/topic/' + participants[0] + '/' + participants[1], onMessageReceived);
            refreshMessages();
        }


        function onError(error) {
        }


        function sendMessage() {

            if (participants[0] === $rootScope.username) {
                $rootScope.receiver = participants[1];
            } else {
                $rootScope.receiver = participants[0];
            }

            var messageContent = messageInput.value.trim();

            if (messageContent && stompClient) {
                var chatMessage = {

                    sender: $rootScope.username,
                    receiver: $rootScope.receiver,
                    content: messageInput.value,
                    type: 'CHAT',

                };

                stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
                $http.post('saveMessage/'+ $cookies.get('dialog'), chatMessage,
                    {
                        withCredentials: true,
                        headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
                    }
                ).success(function (res) {

                }).error(function (error) {

                });
                messageInput.value = '';
            }

        }

        $scope.deleteDialog = function () {

            $http.delete('deleteDialog/' + $cookies.get('dialog'),
                {
                    withCredentials: true,
                    headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
                }
            ).success(function (res) {
               $state.go('dialogs');
            }).error(function (error) {

            });
        };

        function refreshMessages() {
            $http.get('getMessages/'+  $cookies.get("dialog"),
                {
                    withCredentials: true,
                    headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
                }
            ).success(function (res) {
                for (var i = 0; i < res.length; i++) {

                    var message = res[i];
                    $window.localStorage.setItem('dialog',message.dialog);
                    if (message.receiver === $rootScope.username) {
                        var el = document.createElement('li');
                        el.innerHTML = '<div class="incoming_msg">\n' +
                            '            <div class="incoming_msg_img"> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1xrVgo64_sw2v1Q2SuV8BGaoL9sBu0LecbWICLJG5BEKwgMMtJQ" alt="sunil"> </div>\n' +
                            '            <div class="received_msg">\n' +
                            '                <div class="received_withd_msg">\n' +
                            '                    <p>' + message.content +
                            '</p>\n' +
                            ' <span class="time_date">' + message.date + '</span></div>\n' +
                            '            </div>';
                    } else {
                        el = document.createElement('li');
                        el.innerHTML = '<div class="outgoing_msg">\n' +
                            '            <div class="sent_msg">\n' +
                            '                <p>' + message.content + '</p>\n' +
                            '                <span class="time_date">' + message.date +
                            '</span> </div>\n' +
                            '        </div>';

                    }
                    messageArea.appendChild(el);
                    messageArea.scrollTop = messageArea.scrollHeight;

                }
            }).error(function (error) {

            });




        }


        function onMessageReceived(payload) {
            var message = JSON.parse(payload.body);

            if (message.receiver === $rootScope.username) {
                var el = document.createElement('li');
                el.innerHTML = '<div class="incoming_msg">\n' +
                    '            <div class="incoming_msg_img"> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1xrVgo64_sw2v1Q2SuV8BGaoL9sBu0LecbWICLJG5BEKwgMMtJQ" alt="sunil"> </div>\n' +
                    '            <div class="received_msg">\n' +
                    '                <div class="received_withd_msg">\n' +
                    '                    <p>' + message.content +
                    '</p>\n' +
                    ' <span class="time_date">' + "now" + '</span></div>\n' +
                    '            </div>';
            } else {
                el = document.createElement('li');
                el.innerHTML = '<div class="outgoing_msg">\n' +
                    '            <div class="sent_msg">\n' +
                    '                <p>' + message.content + '</p>\n' +
                    '                <span class="time_date">' + "now" +
                    '</span> </div>\n' +
                    '        </div>';

            }
            messageArea.appendChild(el);
            messageArea.scrollTop = messageArea.scrollHeight;
        }


        messageForm.addEventListener('submit', sendMessage, true)
    });


