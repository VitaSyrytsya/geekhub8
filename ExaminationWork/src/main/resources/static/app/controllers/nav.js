angular.module('ExaminationWork')

.controller('NavController', function($http, $scope, AuthService, $state, $rootScope, $cookies) {

	$scope.$on('LoginSuccessful', function() {
		$http({
			method: 'GET',
			url: 'getUser',
			headers: {
				'Authorization': 'Bearer ' + $cookies.get('token')
			}
		}).success(function(res) {
			$scope.user = res;
		}).error(function() {
			logout();
		});
	});

	$scope.$on('LogoutSuccessful', function() {
		$scope.user = null;
	});

	$scope.logout = function() {
		$cookies.remove('token');
		$rootScope.$broadcast('LogoutSuccessful');
		$state.go('login');
	};
});
