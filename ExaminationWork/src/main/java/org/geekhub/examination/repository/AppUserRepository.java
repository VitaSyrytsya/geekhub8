package org.geekhub.examination.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.geekhub.examination.entity.AppUser;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
	 AppUser findOneByUsername(String username);
	 AppUser findUserByPassPhrase(String passphrase);
}
