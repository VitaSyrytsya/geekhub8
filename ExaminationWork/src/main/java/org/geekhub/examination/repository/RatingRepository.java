package org.geekhub.examination.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.geekhub.examination.entity.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

}
