package org.geekhub.examination.controller;

import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.service.doctor.service.DoctorService;
import org.geekhub.examination.service.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.service.doctor.service.DoctorComparatorByRatingDescending;

import java.util.List;

@RestController
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;
    private final UserService userService;

    @Autowired
    public DoctorController(DoctorService doctorService, UserService userService) {
        this.doctorService = doctorService;
        this.userService = userService;
    }

    @GetMapping(value = "/allUsers")
    public List<AppUserDto> getAllUsers() {
        return userService.findAllUsers();
    }


    @GetMapping(value = "/allDoctors")
    public List<AppUserDto> getAllDoctors() {
        return doctorService.findAllDoctors();
    }


    @GetMapping(value = "/allDoctorsOrdered")
    public List<AppUserDto> getAllDoctorsOrderedByRatingDescending() {
        return doctorService.findAllDoctorsOrderedByRatingDescending();
    }


    @GetMapping(value = "/allDoctors/{speciality}")
    public List<AppUserDto> getAllDoctorsBySpeciality(@PathVariable String speciality) {
        return doctorService.findAllDoctorsSpecifiedInOneSpeciality(speciality);
    }

    @GetMapping(value = "/allDoctorsOrdered/{speciality}")
    public List<AppUserDto> getDoctorsOfOneSpecialityOrderedByRating(@PathVariable String speciality) {
        List<AppUserDto> allDoctorsOfOneSpeciality =
                doctorService.findAllDoctorsSpecifiedInOneSpeciality(speciality);
        allDoctorsOfOneSpeciality.sort(new DoctorComparatorByRatingDescending());
        return allDoctorsOfOneSpeciality;
    }


    @GetMapping(value = "/get/{id}")
    public AppUserDto getDoctor(@PathVariable Long id) {

        return userService.findDtoUserById(id);
    }


    @PostMapping(value = "/create/{id}")
    public ResponseEntity createDoctor(@RequestBody Doctor doctor, @PathVariable Long id) {
        if (userService.findUserById(id).isPresent()) {
            AppUser userToMakeDoctor = userService.findUserById(id).get();
            if (doctorService.save(userToMakeDoctor, doctor)) {
                return new ResponseEntity<>(HttpStatus.CREATED);
            }
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }


    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteDoctor(@PathVariable Long id) {
        if (userService.findUserById(id).isPresent()) {
            AppUser user = userService.findUserById(id).get();
            if (doctorService.deleteDoctor(user)) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
