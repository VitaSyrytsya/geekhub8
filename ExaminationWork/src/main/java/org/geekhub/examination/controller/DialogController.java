package org.geekhub.examination.controller;

import io.jsonwebtoken.Claims;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.ChatMessage;
import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.dialog.service.DialogService;
import org.geekhub.examination.service.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.geekhub.examination.dto.ChatMessageDto;
import org.geekhub.examination.dto.DialogDto;
import org.geekhub.examination.exception.DialogException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class DialogController {

    private final DialogService dialogService;
    private final ObjectMapperToDto objectMapperToDto;
    private final UserService userService;


    public DialogController(DialogService dialogService, ObjectMapperToDto objectMapperToDto, UserService userService) {
        this.dialogService = dialogService;
        this.objectMapperToDto = objectMapperToDto;
        this.userService = userService;
    }

    @GetMapping(value = "/dialogs")
    public List<DialogDto> dialogs(HttpServletRequest request) {
        Claims claims = (Claims) request.getAttribute("claims");
        String currentUsername = claims.getSubject();
        return dialogService.getUserDialogsOrderedByLastMessageDateDescending(currentUsername);
    }

    @GetMapping(value = "/getMessages/{id}")
    public List<ChatMessageDto> getMessagesFromDialog(@PathVariable Long id) {
        List<ChatMessageDto> messageDtoList = new ArrayList<>();
        if (dialogService.findDialogById(id).isPresent()) {
            for (ChatMessage chatMessage : dialogService.findDialogById(id).get().getMessages()) {
                messageDtoList.add(objectMapperToDto.mapChatMessage(chatMessage));
            }
        }
        return messageDtoList;
    }

    @DeleteMapping(value = "/deleteDialog/{id}")
    public ResponseEntity deleteDialogById(@PathVariable Long id, HttpServletRequest request) {
        Claims claims = (Claims) request.getAttribute("claims");
        String currentUsername = claims.getSubject();

        if (dialogService.findDialogById(id).isPresent() &&
                userService.findUserByUsername(currentUsername).isPresent()) {

            Dialog dialog = dialogService.findDialogById(id).get();
            AppUser currentUser = userService.findUserByUsername(currentUsername).get();

            if (dialog.getParticipants().size() == 2&&dialog.getMessages().size()!=0) {
                dialog.getParticipants().remove(currentUser);
                dialogService.saveDialog(dialog);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                dialogService.delete(dialog);
                return new ResponseEntity(HttpStatus.OK);
            }
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/findDialog/{id}")
    public DialogDto findDialogByParticipants(@PathVariable Long id, HttpServletRequest request) {

        Claims claims = (Claims) request.getAttribute("claims");
        AppUser currentUser;
        AppUser userToSendMessage;

        if (userService.findUserByUsername(claims.getSubject()).isPresent()
                && userService.findUserById(id).isPresent()) {
            currentUser = userService.findUserByUsername(claims.getSubject()).get();
            userToSendMessage = userService.findUserById(id).get();

            Optional<Dialog> dialog = dialogService.findDialogByParticipants(currentUser, userToSendMessage);
            if (dialog.isPresent()) {
                return objectMapperToDto.mapDialog(dialog.get());

            } else if (!currentUser.getId().equals(userToSendMessage.getId())) {
                return objectMapperToDto.mapDialog(dialogService.createNewDialog(currentUser, userToSendMessage));

            } else {
                throw new DialogException("You cannot send messages to yourself!");
            }
        }
        throw new DialogException("You cannot send messages to user that does not exist!");
    }
}






