package org.geekhub.examination.controller;

import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.entity.Rating;
import org.geekhub.examination.exception.SetRatingToCurrentUserByCurrentUserException;
import org.geekhub.examination.service.rating.service.RatingService;
import org.geekhub.examination.service.user.service.UserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.geekhub.examination.dto.RatingDto;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/rating")
public class RatingController {

    private final RatingService ratingService;
    private final UserService userService;

    public RatingController(RatingService ratingService, UserService userService) {
        this.ratingService = ratingService;
        this.userService = userService;
    }

    @PostMapping(value = "/set/{idDoctor}/{idUser}")
    public RatingDto setCurrentRatingValueByDoctorId(@PathVariable Long idDoctor,
                                                     @PathVariable Long idUser,
                                                     @RequestParam Integer value) {

        if (idDoctor.equals(idUser)) {
            throw new SetRatingToCurrentUserByCurrentUserException("You're enable to set rating" +
                    "to yourself!!!");
        }
        Doctor currentDoctor = null;
        if (userService.findUserById(idDoctor).isPresent()) {
            currentDoctor = userService.findUserById(idDoctor).get().getDoctor();
        }

        assert currentDoctor != null;
        Rating rating = currentDoctor.getRating();
        Map<Long, Integer> currentUserEvalutation = new HashMap<>();
        currentUserEvalutation.put(idUser, value);

        return ratingService.save(rating, currentUserEvalutation);
    }
}
