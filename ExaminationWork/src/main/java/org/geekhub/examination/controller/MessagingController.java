package org.geekhub.examination.controller;

import org.geekhub.examination.entity.ChatMessage;
import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.service.chat.message.service.MessageService;
import org.geekhub.examination.service.dialog.service.DialogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessagingController {

    private final SimpMessagingTemplate messagingTemplate;
    private final MessageService messageService;
    private final DialogService dialogService;


    @Autowired
    public MessagingController(SimpMessagingTemplate messagingTemplate, MessageService messageService, DialogService dialogService) {
        this.messagingTemplate = messagingTemplate;
        this.messageService = messageService;
        this.dialogService = dialogService;
    }

    @MessageMapping("/chat.sendMessage")
    public void sendMessage(@Payload ChatMessage chatMessage) {

        if (chatMessage.getSender().compareTo(chatMessage.getReceiver()) > 0) {
            messagingTemplate.convertAndSend("/topic/" + chatMessage.getReceiver() + "/" + chatMessage.getSender(), chatMessage);
        } else {
            messagingTemplate.convertAndSend("/topic/" + chatMessage.getSender() + "/" + chatMessage.getReceiver(), chatMessage);
        }

    }

    @PostMapping("/saveMessage/{id}")
    public ResponseEntity saveMessage(@PathVariable Long id, @RequestBody ChatMessage chatMessage) {

        if(dialogService.findDialogById(id).isPresent()){
            Dialog currentDialog = dialogService.findDialogById(id).get();

            currentDialog.setLastMessage(chatMessage);
            chatMessage.setDialog(currentDialog);
            messageService.save(chatMessage);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
