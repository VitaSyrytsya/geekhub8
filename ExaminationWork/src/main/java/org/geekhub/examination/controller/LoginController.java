package org.geekhub.examination.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import io.jsonwebtoken.Claims;
import org.geekhub.examination.config.security.config.JwtTokenProvider;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    private final UserService userService;
    private final ObjectMapperToDto objectMapperToDto;
    private final JwtTokenProvider jwtTokenProvider;


    @Autowired
    public LoginController(UserService userService, ObjectMapperToDto objectMapperToDtoImpl, JwtTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.objectMapperToDto = objectMapperToDtoImpl;
        this.jwtTokenProvider = jwtTokenProvider;
    }


    @PostMapping(value = "/authenticate")
    public ResponseEntity<Map<String, Object>> login(@RequestParam String username, @RequestParam String password,
                                                     HttpServletRequest request,
                                                     HttpServletResponse response) {
        if (userService.getAuthenticatedUser(username, password).isPresent()) {
            AppUser currentUser = userService.getAuthenticatedUser(username, password).get();

            if (request.getParameter("rememberMe") != null) {
                jwtTokenProvider.createCurrentUserCookie(currentUser, true, response);
            } else {
                jwtTokenProvider.createCurrentUserCookie(currentUser, false, response);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping(value = "/recover")
    public ResponseEntity recoverUserCredentials(@RequestParam String passphrase,
                                                 @RequestParam String password,
                                                 HttpServletResponse response
    ) {
        AppUser currentUser = userService.findUserByPassPhrase(passphrase);

        if (currentUser != null) {
            currentUser.setPassword(password);
            userService.save(currentUser);
            jwtTokenProvider.createCurrentUserCookie(currentUser, false, response);

            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping(value = "/getUser")
    public AppUserDto confirmCurrentUser(HttpServletRequest request, HttpServletResponse response) {

        Claims claims = (Claims) request.getAttribute("claims");
        if (userService.findUserByUsername(claims.getSubject()).isPresent()) {
            AppUser currentUser = userService.findUserByUsername(claims.getSubject()).get();
            if (claims.get("rememberMe") == null) {
                jwtTokenProvider.createCurrentUserCookie(currentUser, false, response);
            } else {
                jwtTokenProvider.createCurrentUserCookie(currentUser, true, response);
            }
            return objectMapperToDto.mapUser(currentUser);
        }
        throw new UsernameNotFoundException("User not found!");
    }
}

