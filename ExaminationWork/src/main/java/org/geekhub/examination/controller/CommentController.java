package org.geekhub.examination.controller;

import io.jsonwebtoken.Claims;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Comment;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.service.comment.service.CommentService;
import org.geekhub.examination.service.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.geekhub.examination.dto.CommentDto;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/comment")
public class CommentController {

    private final UserService userService;
    private final CommentService commentService;


    public CommentController(UserService userService, CommentService commentService) {
        this.userService = userService;
        this.commentService = commentService;
    }

    @GetMapping(value = "/getAll/{id}")
    public List<CommentDto> getAllComments(@PathVariable Long id) {
        List<CommentDto> comments = new ArrayList<>();
        if (userService.findUserById(id).isPresent()) {
            comments = commentService.getAllCommentsByDoctor(userService.findUserById(id).get().getDoctor().getId());
        }
        return comments;
    }

    @PostMapping(value = "/save/{id}")
    public ResponseEntity saveComment(@PathVariable Long id, @RequestBody Comment comment,
                                      HttpServletRequest request) {
        Claims claims = (Claims) request.getAttribute("claims");
        if (userService.findUserByUsername(claims.getSubject()).isPresent() &&
                userService.findUserById(id).isPresent()
        ) {
            AppUser currentUser = userService.findUserByUsername(claims.getSubject()).get();
            Doctor doctor = userService.findUserById(id).get().getDoctor();
            commentService.save(comment, currentUser, doctor);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(value = "/delete/{id}")
    public ResponseEntity deleteComment(@PathVariable Long id, HttpServletRequest request) {
        Claims claims = (Claims) request.getAttribute("claims");
        if (userService.findUserByUsername(claims.getSubject()).isPresent()) {
            AppUser currentUser = userService.findUserByUsername(claims.getSubject()).get();
            if (checkIfCurrentUserCanDeleteComment(currentUser, id)) {
                commentService.delete(id);
                return new ResponseEntity(HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    private boolean checkIfCurrentUserCanDeleteComment(AppUser user, Long commentId) {
        if(commentService.findById(commentId).isPresent()) {
            if (commentService.findById(commentId).get().getSenderName().
                    equals(user.getUsername())) {
                return true;
            }
            for (String role : user.getRoles()) {
                if (role.equals("ROLE_ADMIN")) {
                    return true;
                }
            }
        }
        return false;
    }
}
