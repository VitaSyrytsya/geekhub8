package org.geekhub.examination.dto;

public class DialogDto {

    private Long id;

    private ChatMessageDto lastMessage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChatMessageDto getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(ChatMessageDto lastMessage) {
        this.lastMessage = lastMessage;
    }
}
