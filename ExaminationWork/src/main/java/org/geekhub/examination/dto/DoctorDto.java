package org.geekhub.examination.dto;

import java.util.ArrayList;
import java.util.List;

public class DoctorDto {

    private Long id;

    private String imgSrc;

    private String surname;

    private String university;

    private RatingDto rating;

    private String experience;

    private List<String> specialityList = new ArrayList<>();

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public RatingDto getRating() {
        return rating;
    }

    public void setRating(RatingDto rating) {
        this.rating = rating;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public List<String> getSpecialityList() {
        return specialityList;
    }

    public void setSpecialityList(List<String> specialityList) {
        this.specialityList = specialityList;
    }
}
