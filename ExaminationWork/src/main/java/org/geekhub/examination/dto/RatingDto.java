package org.geekhub.examination.dto;

import java.util.HashMap;
import java.util.Map;

public class RatingDto {

    private Long id;

    private int averageValue;

    private Map<Long, Integer> uniqueUsersValues = new HashMap<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAverageValue() {
        return averageValue;
    }

    public void setAverageValue(int averageValue) {
        this.averageValue = averageValue;
    }

    public Map<Long, Integer> getUniqueUsersValues() {
        return uniqueUsersValues;
    }

    public void setUniqueUsersValues(Map<Long, Integer> uniqueUsersValues) {
        this.uniqueUsersValues = uniqueUsersValues;
    }
}
