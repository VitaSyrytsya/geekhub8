package org.geekhub.examination.exception;

public class SetRatingToCurrentUserByCurrentUserException extends RuntimeException {
    public  SetRatingToCurrentUserByCurrentUserException(String s){
        super(s);
    }
}
