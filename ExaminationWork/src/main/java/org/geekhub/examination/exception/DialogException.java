package org.geekhub.examination.exception;

public class DialogException extends RuntimeException {
    public DialogException(String s){
        super(s);
    }
}
