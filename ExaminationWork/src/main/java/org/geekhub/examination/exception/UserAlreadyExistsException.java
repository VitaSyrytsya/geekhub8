package org.geekhub.examination.exception;

public class UserAlreadyExistsException extends RuntimeException {
    public UserAlreadyExistsException(String s){
        super(s);
    }
}
