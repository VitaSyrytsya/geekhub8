package org.geekhub.examination.mapper;

import org.geekhub.examination.config.security.config.WebTokenFilter;
import org.springframework.stereotype.Component;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.dto.ChatMessageDto;
import org.geekhub.examination.dto.CommentDto;
import org.geekhub.examination.dto.DialogDto;
import org.geekhub.examination.dto.DoctorDto;
import org.geekhub.examination.dto.RatingDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.ChatMessage;
import org.geekhub.examination.entity.Comment;
import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.entity.Rating;

import java.util.logging.Logger;

@Component
public class ObjectMapperToDto {

    private static final Logger log = Logger.getLogger(WebTokenFilter.class.getName());

    public AppUserDto mapUser(AppUser appUser) {
        AppUserDto appUserDto = new AppUserDto();
        try {
            appUserDto.setId(appUser.getId());
            appUserDto.setName(appUser.getName());
            appUserDto.setUsername(appUser.getUsername());
            appUserDto.setPassword(appUser.getPassword());
            appUserDto.setPassPhrase(appUser.getPassPhrase());
            appUserDto.setRoles(appUser.getRoles());
            appUserDto.setImgSrc(appUser.getImgSrc());
            if (appUser.getDoctor() != null) {
                appUserDto.setDoctor(mapDoctor(appUser.getDoctor()));
            }
        } catch (NullPointerException e) {
          log.info("Trying to map object that have null field" + e.getMessage());
          throw new RuntimeException(e.getMessage());
        }
        return appUserDto;
    }

    private DoctorDto mapDoctor(Doctor doctor) {
        DoctorDto doctorDto = new DoctorDto();
        try {
            doctorDto.setExperience(doctor.getExperience());
            doctorDto.setImgSrc(doctor.getImgSrc());
            doctorDto.setRating(mapRating(doctor.getRating()));
            doctorDto.setSpecialityList(doctor.getSpecialityList());
            doctorDto.setSurname(doctor.getSurname());
            doctorDto.setUniversity(doctor.getUniversity());
            doctorDto.setId(doctor.getId());
        }catch (NullPointerException e){
            log.info("Trying to map object that have null field" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        return doctorDto;
    }

    public DialogDto mapDialog(Dialog dialog) {
        DialogDto dialogDto = new DialogDto();
        try {
            dialogDto.setId(dialog.getId());
            if (dialog.getLastMessage() != null) {
                dialogDto.setLastMessage(mapChatMessage(dialog.getLastMessage()));
            }
        }catch (NullPointerException e){
            log.info("Trying to map object that have null field" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        return dialogDto;
    }

    public ChatMessageDto mapChatMessage(ChatMessage chatMessage) {
        ChatMessageDto chatMessageDto = new ChatMessageDto();
        try{
            chatMessageDto.setId(chatMessage.getId());
            chatMessageDto.setDate(chatMessage.getDate());
            chatMessageDto.setSender(chatMessage.getSender());
            chatMessageDto.setReceiver(chatMessage.getReceiver());
            chatMessageDto.setContent(chatMessage.getContent());
        }catch (NullPointerException e){
            log.info("Trying to map object that have null field" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        return chatMessageDto;
    }

    public CommentDto mapComment(Comment comment) {
        CommentDto commentDto = new CommentDto();
        try {
            commentDto.setId(comment.getId());
            commentDto.setContent(comment.getContent());
            commentDto.setTime(comment.getLocalDateTime());
            commentDto.setSenderName(comment.getSenderName());
            commentDto.setImgSrc(comment.getImgSrc());
        }catch (NullPointerException e){
            log.info("Trying to map object that have null field" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        return commentDto;

    }

    public RatingDto mapRating(Rating rating) {
        RatingDto ratingDto = new RatingDto();
        try{
            ratingDto.setId(rating.getId());
            ratingDto.setAverageValue(rating.getAverageValue());
            ratingDto.setUniqueUsersValues(rating.getUniqueUsersValues());
        }catch (NullPointerException e){
            log.info("Trying to map object that have null field" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        return ratingDto;
    }
}
