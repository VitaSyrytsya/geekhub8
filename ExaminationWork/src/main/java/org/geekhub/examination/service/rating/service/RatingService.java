package org.geekhub.examination.service.rating.service;

import org.geekhub.examination.dto.RatingDto;
import org.geekhub.examination.entity.Rating;

import java.util.Map;

public interface RatingService {

    RatingDto save(Rating rating, Map<Long, Integer> currentUserEvalutation);

    Rating save(Rating rating);

    boolean delete(Rating rating);
}
