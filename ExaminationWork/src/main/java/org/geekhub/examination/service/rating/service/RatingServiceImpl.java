package org.geekhub.examination.service.rating.service;

import org.geekhub.examination.repository.RatingRepository;
import org.springframework.stereotype.Service;
import org.geekhub.examination.dto.RatingDto;
import org.geekhub.examination.entity.Rating;
import org.geekhub.examination.mapper.ObjectMapperToDto;

import java.util.Map;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;
    private final ObjectMapperToDto objectMapperToDto;

    public RatingServiceImpl(RatingRepository ratingRepository, ObjectMapperToDto objectMapperToDtoImpl) {
        this.ratingRepository = ratingRepository;
        this.objectMapperToDto = objectMapperToDtoImpl;
    }

    @Override
    public RatingDto save(Rating rating, Map<Long, Integer> currentUserEvalutation) {
        Map<Long, Integer> currentRating = rating.getUniqueUsersValues();
        Map.Entry<Long,Integer> entry = currentUserEvalutation.entrySet().iterator().next();
        Long key = entry.getKey();
        currentRating.remove(key);
        currentRating.put(key, currentUserEvalutation.get(key));
        rating.setUniqueUsersValues(currentRating);

        return objectMapperToDto.mapRating(ratingRepository.save(rating));
    }

    @Override
    public Rating save(Rating rating) {
        return ratingRepository.save(rating);
    }

    @Override
    public boolean delete(Rating rating) {
        ratingRepository.delete(rating);
        return true;
    }
}
