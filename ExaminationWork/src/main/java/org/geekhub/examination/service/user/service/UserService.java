package org.geekhub.examination.service.user.service;

import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import java.util.List;
import java.util.Optional;

public interface UserService {

    AppUser save(AppUser user);

    boolean deleteUser(AppUser user);

    Optional<AppUser> getAuthenticatedUser(String username, String password);

    List<AppUserDto>findAllUsersThatAreNotDoctors();

    List<AppUserDto>findAllUsers();

    Optional<AppUser> findUserByUsername(String username);

    Optional<AppUser> findUserById(Long id);

    AppUser findUserByPassPhrase(String passphrase);

    AppUserDto findDtoUserById(Long id);

    AppUserDto findDtoUserByUsername(String username);



}
