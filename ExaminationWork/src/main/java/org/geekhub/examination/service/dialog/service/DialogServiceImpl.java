package org.geekhub.examination.service.dialog.service;

import org.geekhub.examination.repository.DialogRepository;
import org.springframework.stereotype.Service;
import org.geekhub.examination.dto.DialogDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.user.service.UserService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DialogServiceImpl implements DialogService {

    private final DialogRepository dialogRepository;
    private final UserService userService;
    private final ObjectMapperToDto objectMapperToDto;

    public DialogServiceImpl(DialogRepository dialogRepository, UserService userService, ObjectMapperToDto objectMapperToDtoImpl) {
        this.dialogRepository = dialogRepository;
        this.userService = userService;
        this.objectMapperToDto = objectMapperToDtoImpl;
    }

    @Override
    public List<DialogDto> getUserDialogsOrderedByLastMessageDateDescending(String username) {
        List<DialogDto> dialogDtoList = new ArrayList<>();
        if (userService.findUserByUsername(username).isPresent()) {
            AppUser currentUser = userService.findUserByUsername(username).get();
            dialogDtoList = currentUser.getDialogs().stream().map(objectMapperToDto::mapDialog).collect(Collectors.toList());
            dialogDtoList.sort(new DialogComparatorByLastMessageDateDescending());
        }
        return dialogDtoList; }

    @Override
    public Dialog createNewDialog(AppUser participantOne, AppUser participantTwo) {
        List<AppUser> participants = new ArrayList();
        participants.add(participantOne);
        participants.add(participantTwo);
        Dialog dialog = new Dialog();
        dialog.setParticipants(participants);

        return dialogRepository.save(dialog);
    }

    @Override
    public void saveDialog(Dialog dialog)
    {
        dialogRepository.save(dialog);
    }


    @Override
    public void delete(Dialog dialog)
    {
        dialogRepository.delete(dialog);
    }

    @Override
    public Optional<Dialog> findDialogById(Long id) {
        return dialogRepository.findById(id);
    }

    @Override
    public Optional<Dialog> findDialogByParticipants(AppUser participantOne, AppUser participantTwo) {
        if (participantOne.getDialogs().size() == 0) {
            return Optional.empty();
        } else {
            for (Dialog dialog : participantOne.getDialogs()) {
                for (AppUser user : dialog.getParticipants()) {
                    if (user.getId().equals(participantTwo.getId())) {
                        return Optional.of(dialog);
                    }
                }
            }
        }
        return Optional.empty();
    }

    static class DialogComparatorByLastMessageDateDescending implements Comparator<DialogDto> {
        @Override
        public int compare(DialogDto dialog1, DialogDto dialog2) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dialog1DateTime = LocalDateTime
                    .parse(dialog1.getLastMessage().getDate(), formatter);
            LocalDateTime dialog2DateTime = LocalDateTime
                    .parse(dialog2.getLastMessage().getDate(), formatter);
            int i;
            i = dialog2DateTime.compareTo(dialog1DateTime);
            return i;
        }
    }
}

