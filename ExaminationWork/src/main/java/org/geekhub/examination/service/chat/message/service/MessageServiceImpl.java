package org.geekhub.examination.service.chat.message.service;

import org.geekhub.examination.repository.ChatMessageRepository;
import org.springframework.stereotype.Service;
import org.geekhub.examination.entity.ChatMessage;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class MessageServiceImpl implements MessageService {

    private final ChatMessageRepository chatMessageRepository;

    public MessageServiceImpl(ChatMessageRepository chatMessageRepository) {
        this.chatMessageRepository = chatMessageRepository;
    }

    @Override
    public void save(ChatMessage chatMessage) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String date = LocalDateTime.now().format(formatter);
        chatMessage.setDate(date);
        chatMessageRepository.save(chatMessage);
    }
}
