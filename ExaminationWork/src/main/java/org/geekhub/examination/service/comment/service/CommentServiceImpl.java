package org.geekhub.examination.service.comment.service;

import org.geekhub.examination.repository.DoctorRepository;
import org.springframework.stereotype.Service;
import org.geekhub.examination.dto.CommentDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Comment;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.repository.CommentRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {
    private final DoctorRepository doctorRepository;
    private final ObjectMapperToDto objectMapperToDto;
    private final CommentRepository commentRepository;

    public CommentServiceImpl(DoctorRepository doctorRepository, ObjectMapperToDto objectMapperToDtoImpl, CommentRepository commentRepository) {
        this.doctorRepository = doctorRepository;
        this.objectMapperToDto = objectMapperToDtoImpl;
        this.commentRepository = commentRepository;
    }

    @Override
    public List<CommentDto> getAllCommentsByDoctor(Long doctorId) {
        List<CommentDto> comments = new ArrayList<>();
        if(doctorRepository.findById(doctorId).isPresent()) {
            for (Comment comment : doctorRepository.findById(doctorId).get().getComments()) {
                comments.add(objectMapperToDto.mapComment(comment));
            }
            comments.sort(new CommentComparatorByLastMessageDateDescending());
        }
        return comments;
    }

    @Override
    public Comment save(Comment comment, AppUser sender, Doctor doctor) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateTime = LocalDateTime.now().format(formatter);
        comment.setDoctor(doctor);
        comment.setLocalDateTime(dateTime);
        comment.setImgSrc(sender.getImgSrc());
        comment.setSenderName(sender.getUsername());

        return commentRepository.save(comment);
    }

    @Override
    public Optional<Comment> findById(Long id) {
        return commentRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        commentRepository.deleteById(id);
    }

    class CommentComparatorByLastMessageDateDescending implements Comparator<CommentDto> {
        @Override
        public int compare(CommentDto comment1, CommentDto comment2) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime comment1DateTime = LocalDateTime
                    .parse(comment1.getLocalDateTime(), formatter);
            LocalDateTime comment2DateTime = LocalDateTime
                    .parse(comment2.getLocalDateTime(), formatter);
            int i;
            i = comment2DateTime.compareTo(comment1DateTime);
            return i;
        }
    }


}
