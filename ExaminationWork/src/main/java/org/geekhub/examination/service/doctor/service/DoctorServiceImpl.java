package org.geekhub.examination.service.doctor.service;

import org.geekhub.examination.repository.DoctorRepository;
import org.springframework.stereotype.Service;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.entity.Rating;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.rating.service.RatingService;
import org.geekhub.examination.service.user.service.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorServiceImpl implements DoctorService {
    private final UserService userService;
    private final RatingService ratingService;
    private final DoctorRepository doctorRepository;
    private final ObjectMapperToDto objectMapperToDto;


    public DoctorServiceImpl(UserService userService, RatingService ratingService, DoctorRepository doctorRepository, ObjectMapperToDto objectMapperToDtoImpl) {
        this.userService = userService;
        this.ratingService = ratingService;
        this.doctorRepository = doctorRepository;
        this.objectMapperToDto = objectMapperToDtoImpl;
    }

    @Override
    public boolean save(AppUser user, Doctor doctor) {
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_DOCTOR");
        user.setRoles(roles);
        if (user.getDoctor() == null) {
            user.setDoctor(doctor);
            Doctor savedDoctor = userService.save(user).getDoctor();
            Rating rating = new Rating();
            rating.setAverageValue(1);
            rating.setDoctor(savedDoctor);
            ratingService.save(rating);
        } else {
            user.getDoctor().setSpecialityList(doctor.getSpecialityList());
            user.getDoctor().setExperience(doctor.getExperience());
            user.getDoctor().setSurname(doctor.getSurname());
            user.getDoctor().setUniversity(doctor.getUniversity());
            userService.save(user);
        }
        return true;
    }

    @Override
    public List<AppUserDto> findAllDoctors() {
        List<AppUserDto> doctors = new ArrayList<>();
        if (doctorRepository.findAll().size() > 0) {
            doctors = doctorRepository.findAll()
                    .stream()
                    .map(x -> objectMapperToDto.mapUser(x.getUser()))
                    .collect(Collectors.toList());
        }
        return doctors;
    }

    @Override
    public List<AppUserDto> findAllDoctorsSpecifiedInOneSpeciality(String speciality) {
        List<AppUserDto> doctorsInOneSpeciality = new ArrayList<>();
        for(Doctor doctor: doctorRepository.findAll()){
            for(String doctorSpeciality: doctor.getSpecialityList()){
                if(doctorSpeciality.equals(speciality)){
                    doctorsInOneSpeciality.add(objectMapperToDto.mapUser(doctor.getUser()));
                }
            }
        }
        return doctorsInOneSpeciality;
    }

    @Override
    public List<AppUserDto> findAllDoctorsOrderedByRatingDescending() {
        List<AppUserDto>allDoctors = findAllDoctors();
        allDoctors.sort(new DoctorComparatorByRatingDescending());
        return allDoctors;
    }


    @Override
    public boolean deleteDoctor(AppUser user) {
        Doctor doctorToDelete = user.getDoctor();
        if (doctorToDelete != null) {
            List<String> roles = new ArrayList<>();
            roles.add("ROLE_USER");
            user.setDoctor(null);
            user.setRoles(roles);
            userService.save(user);
            doctorRepository.delete(doctorToDelete);
            return true;
        }
        return false;
    }


}

