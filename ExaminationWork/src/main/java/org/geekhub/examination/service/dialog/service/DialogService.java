package org.geekhub.examination.service.dialog.service;

import org.geekhub.examination.dto.DialogDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Dialog;
import java.util.List;
import java.util.Optional;

public interface DialogService {

     List<DialogDto>getUserDialogsOrderedByLastMessageDateDescending(String username);

     Dialog createNewDialog(AppUser participantOne, AppUser participantTwo);

     void saveDialog(Dialog dialog);

     void delete(Dialog dialog);

     Optional<Dialog> findDialogById(Long id);

     Optional<Dialog> findDialogByParticipants(AppUser participantOne, AppUser participantTwo);
}
