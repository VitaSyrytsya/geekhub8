package org.geekhub.examination.service.doctor.service;

import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Doctor;

import java.util.List;

public interface DoctorService {

     boolean save(AppUser user, Doctor doctor);

     List<AppUserDto>findAllDoctors();

     List<AppUserDto>findAllDoctorsSpecifiedInOneSpeciality(String speciality);

     List<AppUserDto>findAllDoctorsOrderedByRatingDescending();

     boolean deleteDoctor(AppUser user);


}
