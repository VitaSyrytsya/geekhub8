package org.geekhub.examination.service.comment.service;

import org.geekhub.examination.dto.CommentDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Comment;
import org.geekhub.examination.entity.Doctor;

import java.util.List;
import java.util.Optional;

public interface CommentService {

    List<CommentDto>getAllCommentsByDoctor(Long doctorId);

    Comment save(Comment comment, AppUser sender, Doctor doctor);

    Optional<Comment> findById(Long id);

    void delete(Long id);
}
