package org.geekhub.examination.service.doctor.service;

import org.geekhub.examination.dto.AppUserDto;

import java.util.Comparator;

public class DoctorComparatorByRatingDescending implements Comparator<AppUserDto> {
   @Override
   public int compare(AppUserDto doctor1, AppUserDto doctor2) {
       int i;
       i = Integer.compare(doctor2.getDoctor().getRating()
               .getAverageValue(), doctor1.getDoctor().getRating()
               .getAverageValue());

       return i;
   }
}
