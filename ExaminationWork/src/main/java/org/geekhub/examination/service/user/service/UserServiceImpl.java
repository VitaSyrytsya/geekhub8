package org.geekhub.examination.service.user.service;

import org.geekhub.examination.repository.AppUserRepository;
import org.geekhub.examination.repository.DialogRepository;
import org.springframework.stereotype.Service;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.mapper.ObjectMapperToDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {


    private final AppUserRepository appUserRepository;
    private final DialogRepository dialogRepository;
    private final ObjectMapperToDto objectMapperToDto;
    private final PasswordEncoder passwordEncoder;


    public UserServiceImpl(AppUserRepository appUserRepository, DialogRepository dialogRepository, ObjectMapperToDto objectMapperToDtoImpl, PasswordEncoder passwordEncoder) {
        this.appUserRepository = appUserRepository;
        this.dialogRepository = dialogRepository;
        this.objectMapperToDto = objectMapperToDtoImpl;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AppUser save(AppUser user) {
        if (user.getImgSrc() == null) {
            String default_Image_Src = "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";
            user.setImgSrc(default_Image_Src);
        }
        if (user.getId()!=null && appUserRepository.findById(user.getId()).isPresent()) {
            AppUser oldUser = appUserRepository.findById(user.getId()).get();
            if (!oldUser.getPassword().equals(user.getPassword())) {
                user.setPassword(passwordEncoder.encode(user.getPassword()));
            }
        } else {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        appUserRepository.save(user);
        return user;
    }


    @Override
    public boolean deleteUser(AppUser user) {
        if (user.getUsername().equals("admin")) {
            return false;
        } else {
            if (user.getDialogs().size() != 0) {
                for (Dialog dialog : user.getDialogs()) {
                    dialog.getParticipants().remove(user);
                    if (dialogRepository.save(dialog).getParticipants().size() == 0) {
                        dialogRepository.delete(dialog);
                    }
                }
            }
            appUserRepository.delete(user);
            return true;
        }
    }

    @Override
    public Optional<AppUser> getAuthenticatedUser(String username, String password) {
        AppUser currentUser = appUserRepository.findOneByUsername(username);
        if (currentUser == null || !currentUser.getPassword().equals(passwordEncoder.encode(password))) {
            return Optional.empty();
        }
        return Optional.of(currentUser);
    }


    @Override
    public List<AppUserDto> findAllUsersThatAreNotDoctors() {

        List<AppUserDto> users = appUserRepository.findAll().stream().map(objectMapperToDto::mapUser).collect(Collectors.toList());
        List<AppUserDto> usersThatAreNotDoctors = new ArrayList<>();
        for (AppUserDto user : users) {
            if (!user.getRoles().contains("ROLE_DOCTOR")) {
                usersThatAreNotDoctors.add(user);
            }
        }
        return usersThatAreNotDoctors;
    }

    @Override
    public List<AppUserDto> findAllUsers() {
        return appUserRepository.findAll().stream().map(objectMapperToDto::mapUser).collect(Collectors.toList());
    }


    @Override
    public Optional<AppUser> findUserByUsername(String username) {
        return Optional.ofNullable(appUserRepository.findOneByUsername(username));
    }

    @Override
    public Optional<AppUser> findUserById(Long id) {

        return appUserRepository.findById(id);
    }

    @Override
    public AppUser findUserByPassPhrase(String passphrase) {
        return appUserRepository.findUserByPassPhrase(passphrase);
    }

    @Override
    public AppUserDto findDtoUserById(Long id) {
        return objectMapperToDto.mapUser(appUserRepository.findById(id).get());
    }

    @Override
    public AppUserDto findDtoUserByUsername(String username) {
        AppUser user = appUserRepository.findOneByUsername(username);
        if (user != null) {
            return objectMapperToDto.mapUser(appUserRepository.findOneByUsername(username));
        }
        return objectMapperToDto.mapUser(appUserRepository.findOneByUsername(username));
    }


}
