package org.geekhub.examination.config.security.config;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.geekhub.examination.entity.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@Component
public class JwtTokenProvider {

    private static final String jwtSigningKey = "secretkey";
    private final CustomUserDetailsService userDetailsService;
    private static final int Twenty_Eight_Days_In_Seconds = 2419200;
    private static final int Two_Hours_In_Seconds = 7200;

    @Autowired
    public JwtTokenProvider(CustomUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }


    Authentication getAuthentication(String token) {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    private String getUsername(String token) {
        return Jwts.parser()
                .setSigningKey( jwtSigningKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    private String createCurrentUserToken(AppUser currentUser, boolean rememberMe) {
        String token;
        if (rememberMe) {
            token = Jwts.builder().setSubject(currentUser.getUsername())
                    .claim("roles", currentUser.getRoles())
                    .claim("rememberMe", "rememberMe")
                    .setIssuedAt(new Date())
                    .signWith(SignatureAlgorithm.HS256, jwtSigningKey)
                    .compact();
        } else {
            token = Jwts.builder().setSubject(currentUser.getUsername())
                    .claim("roles", currentUser.getRoles())
                    .setIssuedAt(new Date())
                    .signWith(SignatureAlgorithm.HS256, jwtSigningKey)
                    .compact();
        }
        return token;
    }

    public void createCurrentUserCookie(AppUser currentUser, boolean rememberMe, HttpServletResponse httpServletResponse) {
        Cookie cookie;
        if (rememberMe) {
            cookie = new Cookie("token", createCurrentUserToken(currentUser, true));
            cookie.setMaxAge(Twenty_Eight_Days_In_Seconds);
        } else {
            cookie = new Cookie("token", createCurrentUserToken(currentUser, false));
            cookie.setMaxAge(Two_Hours_In_Seconds);
        }
        httpServletResponse.addCookie(cookie);
    }


}



