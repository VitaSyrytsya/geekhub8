package org.geekhub.examination.config.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {


	private final JwtTokenProvider jwtTokenProvider;

	@Autowired
	public WebSecurityConfiguration(JwtTokenProvider jwtTokenProvider) {
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	public void configure(WebSecurity web) {

		web.ignoring()

				.antMatchers("/", "/index.html", "/app/**", "/register","/recover", "/authenticate","/favicon.ico"
						, "/chat", "/info", "/ws/**",  "/doctor/allDoctors/**", "/doctor/allDoctorsOrdered/**",
						"/swagger-ui.html", "/v2/**"
				);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.httpBasic().disable()

				.csrf().disable()

				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

				.and()

				.authorizeRequests()

				.antMatchers("/info").permitAll()

				.antMatchers("/doctor/delete/**").hasAnyRole("DOCTOR", "ADMIN")

				.antMatchers("/user/create/**", "/user/update/**",
						"/doctor/create/**").hasRole("ADMIN")

				.anyRequest().authenticated()

				.and()

				.apply(new JwtConfigurer(jwtTokenProvider));

	}
}
