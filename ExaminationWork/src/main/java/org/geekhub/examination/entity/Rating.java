package org.geekhub.examination.entity;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "Rating")
@Table(name = "rating")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int averageValue;

    @ElementCollection
    private Map<Long, Integer> uniqueUsersValues = new HashMap<>();

    @OneToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAverageValue() {
        return averageValue;
    }

    public void setAverageValue(int averageValue) {
        this.averageValue = averageValue;
    }

    public Map<Long, Integer> getUniqueUsersValues() {

        return uniqueUsersValues;
    }

    public void setUniqueUsersValues(Map<Long, Integer> uniqueUsersValues) {
        int sumOfStars = uniqueUsersValues.values().stream().reduce(0, (x, y) -> x + y);
        int integerAverageValueOfStars = sumOfStars / uniqueUsersValues.size();
        this.setAverageValue(integerAverageValueOfStars);


        this.uniqueUsersValues = uniqueUsersValues;
    }

}
