package org.geekhub.examination.service;

import org.geekhub.examination.repository.DoctorRepository;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.entity.Rating;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.doctor.service.DoctorService;
import org.geekhub.examination.service.user.service.UserService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

@SpringBootTest
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class DoctorServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private DoctorService doctorService;

    @MockBean
    private DoctorRepository doctorRepository;

    @MockBean
    private UserService userService;


    @MockBean
    private ObjectMapperToDto objectMapperToDto;



    @Test
    public void shouldCreateNewDoctorIfNotExists() {
        AppUser testUser = new AppUser();
        Doctor doctor = new Doctor();
        doctor.setRating(new Rating());
        doctor.setSpecialityList(List.of("SPECIALITY"));
        doctor.setSurname("Hauptmann");
        doctor.setUniversity("Nord Ost");

        testUser.setDoctor(doctor);
        Mockito.when(userService.save(testUser)).thenReturn(testUser);

        Assert.assertTrue(doctorService.save(testUser, doctor));
    }

    @Test
    public void shouldUpdateOldDoctorIfDoctorAlreadyExists() {
        AppUser testUser = new AppUser();
        testUser.setDoctor(new Doctor());

        Doctor doctor = new Doctor();
        doctor.setSpecialityList(List.of("SPECIALITY"));
        doctor.setSurname("Hauptmann");
        doctor.setUniversity("Nord Ost");

        Assert.assertTrue(doctorService.save(testUser, doctor));
    }

    @Test
    public void shouldReturnAllMappedUsersThatAreDoctors() {
        AppUser testUser = new AppUser();
        AppUserDto testDtoUser = new AppUserDto();

        Doctor doctor = new Doctor();
        doctor.setSpecialityList(List.of("SPECIALITY"));
        doctor.setSurname("Hauptmann");
        doctor.setUniversity("Nord Ost");
        doctor.setUser(testUser);

        List<Doctor>allDoctors = List.of(doctor);
        Mockito.when(doctorRepository.findAll()).thenReturn(allDoctors);
        Mockito.when(objectMapperToDto.mapUser(testUser)).thenReturn(testDtoUser);

        Assert.assertEquals(doctorService.findAllDoctors(),
                Collections.singletonList(testDtoUser));
    }

    @Test
    public void shouldFindAllDoctorsSpecifiedInOneSpeciality() {
        AppUser testUser = new AppUser();
        AppUserDto testDtoUser = new AppUserDto();
        AppUser testUser2 = new AppUser();
        AppUserDto testDtoUser2 = new AppUserDto();

        Doctor doctor = new Doctor();
        doctor.setSpecialityList(List.of("AMNESIA"));
        doctor.setUser(testUser);
        Doctor doctor2 = new Doctor();
        doctor2.setSpecialityList(List.of("ALLERGY"));
        doctor2.setUser(testUser2);

        List<Doctor>allDoctors = List.of(doctor, doctor2);
        Mockito.when(doctorRepository.findAll()).thenReturn(allDoctors);
        Mockito.when(objectMapperToDto.mapUser(testUser)).thenReturn(testDtoUser);
        Mockito.when(objectMapperToDto.mapUser(testUser2)).thenReturn(testDtoUser2);

        Assert.assertEquals(doctorService.findAllDoctorsSpecifiedInOneSpeciality("AMNESIA"),
                Collections.singletonList(testDtoUser));
    }

    @Test
    public void shouldDeleteDoctorWhenUserIsADoctor() {
        AppUser testUser = new AppUser();
        Doctor doctor = new Doctor();
        testUser.setDoctor(doctor);

        Assert.assertTrue(doctorService.deleteDoctor(testUser));
    }
}
