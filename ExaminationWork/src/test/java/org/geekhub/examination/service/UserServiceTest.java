package org.geekhub.examination.service;

import org.geekhub.examination.repository.AppUserRepository;
import org.geekhub.examination.repository.DialogRepository;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.user.service.PasswordEncoder;
import org.geekhub.examination.service.user.service.UserService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class UserServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private UserService userService;

    @MockBean
    private AppUserRepository appUserRepository;

    @MockBean
    private DialogRepository dialogRepository;

    @MockBean
    private ObjectMapperToDto objectMapperToDto;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void checkIfUserWouldBeSavedIntoDatabase() {
        AppUser testUser = new AppUser();
        testUser.setImgSrc("xxx");
        testUser.setPassword("password");

        Mockito.when(passwordEncoder.encode("password")).thenReturn("encodedPassword");
        AppUser savedUser = userService.save(testUser);

        Mockito.verify(appUserRepository, Mockito.atLeastOnce()).save(testUser);
        Assert.assertEquals(savedUser.getPassword(), "encodedPassword");
    }

    @Test
    public void checkIfUserWouldBeUpdatedIntoDatabaseIfExists() {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        testUser.setImgSrc("xxx");
        testUser.setPassword("password");

        Mockito.when(passwordEncoder.encode("password")).thenReturn("encodedPassword");
        Mockito.when(appUserRepository.findById((long) 1)).thenReturn(Optional.of(testUser));
        userService.save(testUser);

        Mockito.verify(appUserRepository, Mockito.atLeastOnce()).save(testUser);
    }

    @Test
    public void shouldNotDeleteDefaultUserFromDatabase() {
        AppUser testUser = new AppUser();
        testUser.setUsername("admin");

        Assert.assertFalse(userService.deleteUser(testUser));
    }

    @Test
    public void shouldDeleteRegularUserFromDatabase() {
        AppUser testUser = new AppUser();
        testUser.setUsername("user");

        Assert.assertTrue(userService.deleteUser(testUser));
        Mockito.verify(appUserRepository, Mockito.atLeastOnce()).delete(testUser);
    }

    @Test
    public void shouldDeleteAllUsersDialogsIfExistWhenDeleteUser() {
        AppUser testUser = new AppUser();
        testUser.setUsername("user");
        Dialog dialog = new Dialog();
        Dialog newDialog = new Dialog();
        List<AppUser> participants = new ArrayList<>();
        newDialog.setParticipants(participants);
        participants.add(testUser);
        dialog.setParticipants(participants);
        List<Dialog> dialogs = new ArrayList<>();
        dialogs.add(dialog);
        testUser.setDialogs(dialogs);

        Mockito.when(dialogRepository.save(dialog)).thenReturn(newDialog);

        Assert.assertTrue(userService.deleteUser(testUser));
        Mockito.verify(dialogRepository, Mockito.atLeastOnce()).delete(dialog);
    }

    @Test
    public void shouldReturnAuthenticatedUserWhenItExists() {
        AppUser testUser = new AppUser();
        testUser.setUsername("username");
        testUser.setPassword("password");

        Mockito.when(appUserRepository.findOneByUsername("username")).thenReturn(testUser);
        Mockito.when(passwordEncoder.encode("password")).thenReturn("password");

        Assert.assertEquals(userService.getAuthenticatedUser("username", "password")
                .get().getUsername(), "username");
    }

    @Test
    public void shouldNotAuthenticateUserWhenPasswordsNotMatching() {
        AppUser testUser = new AppUser();
        testUser.setUsername("username");
        testUser.setPassword("password");

        Mockito.when(appUserRepository.findOneByUsername("username")).thenReturn(testUser);
        Mockito.when(passwordEncoder.encode("password")).thenReturn("encodedPassword");

        Assert.assertEquals(userService.getAuthenticatedUser("username", "password")
                , Optional.empty());
    }

    @Test
    public void shouldReturnListOfAllDtoUsersThatAreNotDoctors() {
        AppUser user1 = new AppUser();
        AppUser user2 = new AppUser();
        List<AppUser>allAppUsers = List.of(user1, user2);

        AppUserDto userDto1 = new AppUserDto();
        AppUserDto userDto2 = new AppUserDto();
        List<String>roles1 = List.of("ROLE_USER");
        List<String>roles2 = List.of("ROLE_DOCTOR");
        userDto1.setRoles(roles1);
        userDto2.setRoles(roles2);

        Mockito.when(appUserRepository.findAll()).thenReturn(allAppUsers);
        Mockito.when(objectMapperToDto.mapUser(user1)).thenReturn(userDto1);
        Mockito.when(objectMapperToDto.mapUser(user2)).thenReturn(userDto2);


        Assert.assertEquals(userService.findAllUsersThatAreNotDoctors()
                , Collections.singletonList(userDto1));
    }

    @Test
    public void shouldReturnListOfMappedUsers() {
        AppUser user1 = new AppUser();
        List<AppUser>allAppUsers = List.of(user1);
        AppUserDto userDto1 = new AppUserDto();

        Mockito.when(appUserRepository.findAll()).thenReturn(allAppUsers);
        Mockito.when(objectMapperToDto.mapUser(user1)).thenReturn(userDto1);

        Assert.assertEquals(userService.findAllUsersThatAreNotDoctors()
                , Collections.singletonList(userDto1));
    }

    @Test
    public void shouldFindUserByUsernameIfExists() {
        AppUser testUser = new AppUser();
        testUser.setUsername("username");

        Mockito.when(appUserRepository.findOneByUsername("username")).thenReturn(testUser);

        Assert.assertEquals(userService.findUserByUsername("username")
                , Optional.of(testUser));
    }

    @Test
    public void shouldFindUserByIdIfExists() {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        Mockito.when(appUserRepository.findById((long) 1)).thenReturn(Optional.of(testUser));

        Assert.assertEquals(userService.findUserById((long) 1)
                , Optional.of(testUser));
    }

    @Test
    public void shouldFindUserByPassPhraseIfExists() {
        AppUser testUser = new AppUser();
        testUser.setPassPhrase("passphrase");
        Mockito.when(appUserRepository.findUserByPassPhrase("passphrase")).thenReturn(testUser);

        Assert.assertEquals(userService.findUserByPassPhrase("passphrase")
                , testUser);
    }


}
