package org.geekhub.examination.service;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.geekhub.examination.dto.CommentDto;
import org.geekhub.examination.entity.Comment;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.repository.CommentRepository;
import org.geekhub.examination.repository.DoctorRepository;
import org.geekhub.examination.service.comment.service.CommentService;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class CommentServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private CommentService commentService;
    @MockBean
    private DoctorRepository doctorRepository;
    @MockBean
    private ObjectMapperToDto objectMapperToDto;
    @MockBean
    private CommentRepository commentRepository;

    @Test
    public void shouldReturnListOfDoctorCommentsOrderedByLastMessageDateDescending() {
        Doctor doctor = new Doctor();
        Comment comment1 = new Comment();
        Comment comment2 = new Comment();
        doctor.setComments(List.of(comment1, comment2));
        CommentDto commentDto1 = new CommentDto();
        String dateTime1 = "2019-04-14 14:00:00";
        commentDto1.setTime(dateTime1);
        CommentDto commentDto2 = new CommentDto();
        String dateTime2 = "2019-04-14 14:30:00";
        commentDto2.setTime(dateTime2);

        Mockito.when(doctorRepository.findById((long) 1)).thenReturn(Optional.of(doctor));
        Mockito.when(objectMapperToDto.mapComment(comment1)).thenReturn(commentDto1);
        Mockito.when(objectMapperToDto.mapComment(comment2)).thenReturn(commentDto2);

        Assert.assertEquals(commentService.getAllCommentsByDoctor((long) 1),
                List.of(commentDto2, commentDto1));
    }

    @Test
    public void shouldFindCommentById() {
        commentService.findById((long) 1);
        Mockito.verify(commentRepository, Mockito.atLeastOnce()).findById((long) 1);
    }

    @Test
    public void shouldDeleteCommentById() {
        commentService.delete((long) 1);
        Mockito.verify(commentRepository, Mockito.atLeastOnce()).deleteById((long) 1);
    }


}
