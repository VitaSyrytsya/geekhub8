package org.geekhub.examination.service;

import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.repository.DialogRepository;
import org.geekhub.examination.service.dialog.service.DialogService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@SpringBootTest
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class DialogServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private DialogService dialogService;

    @MockBean
    private DialogRepository dialogRepository;


    @Test
    public void shouldCreateAndSaveNewDialog() {
        Dialog testDialog = new Dialog();

        dialogService.saveDialog(testDialog);

        Mockito.verify(dialogRepository, Mockito.atLeastOnce()).save(testDialog);

    }

    @Test
    public void shouldDeleteDialog() {
        Dialog testDialog = new Dialog();

        dialogService.delete(testDialog);

        Mockito.verify(dialogRepository, Mockito.atLeastOnce()).delete(testDialog);

    }

    @Test
    public void shouldFindDialogById() {
        Dialog testDialog = new Dialog();
        testDialog.setId((long)1);

        dialogService.findDialogById((long)1);

        Mockito.verify(dialogRepository, Mockito.atLeastOnce()).findById((long)1);
    }
}
