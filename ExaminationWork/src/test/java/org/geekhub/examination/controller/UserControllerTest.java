package org.geekhub.examination.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.service.user.service.UserService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import org.springframework.web.util.NestedServletException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class UserControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @BeforeMethod
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new UserController(userService)).build();
    }

    @Test
    public void shouldReturnJsonListOfAllUsersThatAreNotDoctors() throws Exception {
        AppUserDto testUser = new AppUserDto();
        AppUserDto testUser2 = new AppUserDto();
        testUser.setUsername("Alla");
        testUser2.setUsername("Maria");
        List<AppUserDto>usersThatAreNotDoctors = List.of(testUser, testUser2);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String listOfUsersInJson = ow.writeValueAsString(usersThatAreNotDoctors);
        Mockito.when(userService.findAllUsersThatAreNotDoctors()).thenReturn(usersThatAreNotDoctors);

        mockMvc.perform(get("/user/getNoDoctors"))
                .andExpect(status().isOk())
                .andExpect(content().json(listOfUsersInJson, true));
    }


    @Test
    public void shouldReturnHttpStatusOkWhenUserFoundById() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);

        String path = "/user/get/"+ 1;

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.of(testUser));

        mockMvc.perform(get(path))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnHttpStatusNoContentWhenUserNotFoundById() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        String path = "/user/get/"+ 1;

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.empty());

        mockMvc.perform(get(path))
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldDeleteUserWhenUserFoundById() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        String path = "/user/delete/"+ 1;

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.of(testUser));
        Mockito.when(userService.deleteUser(testUser)).thenReturn(true);

        mockMvc.perform(delete(path))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldNotDeleteUserWhenUserNotFoundById() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        String path = "/user/delete/"+ 1;

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.empty());
        Mockito.when(userService.deleteUser(testUser)).thenReturn(false);

        mockMvc.perform(delete(path))
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldCreateUserWhenUserNotFoundByUsername() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        testUser.setUsername("test");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonUser = ow.writeValueAsString(testUser);
        Mockito.when(userService.findUserByUsername("test")).thenReturn(Optional.empty());

        mockMvc.perform(post("/user/create")
                .contentType(APPLICATION_JSON_UTF8)
                .content(jsonUser))
                .andExpect(status().isCreated());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void shouldThrowExceptionWhenUserFoundByUsername() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        testUser.setUsername("test");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonUser = ow.writeValueAsString(testUser);
        Mockito.when(userService.findUserByUsername("test")).thenReturn(Optional.of(testUser));

        mockMvc.perform(post("/user/create")
                .contentType(APPLICATION_JSON_UTF8)
                .content(jsonUser));
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void shouldThrowExceptionWhenTryToUpdateUserWithUsernameThatAlreadyExists() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        testUser.setUsername("test");

        AppUser testUser2 = new AppUser();
        testUser2.setId((long) 2);
        testUser2.setUsername("test");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonUser = ow.writeValueAsString(testUser2);
        Mockito.when(userService.findUserByUsername("test")).thenReturn(Optional.of(testUser));

        mockMvc.perform(put("/user/update")
                .contentType(APPLICATION_JSON_UTF8)
                .content(jsonUser));
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void shouldThrowExceptionWhenTryToUpdateDefaultUser() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        testUser.setUsername("admin");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonUser = ow.writeValueAsString(testUser);
        Mockito.when(userService.findUserByUsername("admin")).thenReturn(Optional.of(testUser));

        mockMvc.perform(put("/user/update")
                .contentType(APPLICATION_JSON_UTF8)
                .content(jsonUser));
    }

    @Test
    public void shouldReturnHttpStatusOkWhenItIsAllowedToUpdateUser() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setId((long) 2);
        testUser.setUsername("user");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonUser = ow.writeValueAsString(testUser);
        Mockito.when(userService.findUserByUsername("user")).thenReturn(Optional.of(testUser));

        mockMvc.perform(put("/user/update")
                .contentType(APPLICATION_JSON_UTF8)
                .content(jsonUser))
                .andExpect(status().isOk());
    }
}

