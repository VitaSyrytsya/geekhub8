package org.geekhub.examination.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.geekhub.examination.entity.ChatMessage;
import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.service.chat.message.service.MessageService;
import org.geekhub.examination.service.dialog.service.DialogService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.nio.charset.Charset;
import java.util.Optional;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class MessagingControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private MessageService messageService;
    @MockBean
    private DialogService dialogService;
    @MockBean
    private SimpMessagingTemplate simpMessagingTemplate;

    private static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(MediaType.APPLICATION_JSON.getType(),
                    MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @BeforeMethod
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new MessagingController(
                simpMessagingTemplate,
                messageService,
                dialogService)).build();
    }

    @Test
    public void shouldReturnJsonListOfAllMessagesFoundByUserId() throws Exception {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setSender("admin");
        chatMessage.setContent("hello!");
        Dialog dialog = new Dialog();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String sentMessageInJsonFormat = ow.writeValueAsString(chatMessage);

        Mockito.when(dialogService.findDialogById((long) 1)).thenReturn(Optional.of(dialog));

        mockMvc.perform(MockMvcRequestBuilders.post("/saveMessage/" + 1)
                .contentType(APPLICATION_JSON_UTF8)
                .content(sentMessageInJsonFormat))
                .andExpect(status().isOk());
    }
}


