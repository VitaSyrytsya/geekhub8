package org.geekhub.examination.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.geekhub.examination.dto.ChatMessageDto;
import org.geekhub.examination.dto.DialogDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.ChatMessage;
import org.geekhub.examination.entity.Dialog;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.dialog.service.DialogService;
import org.geekhub.examination.service.user.service.UserService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.List;
import java.util.Optional;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class DialogControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;

    @MockBean
    private DialogService dialogService;

    @MockBean
    private ObjectMapperToDto objectMapperToDto;

    @BeforeMethod
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new DialogController(dialogService, objectMapperToDto, userService)).build();
    }

    @Test
    public void shouldReturnJsonListOfAllDialogsWhenDialogsExistByUserId() throws Exception {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
        DialogDto dialog = new DialogDto();
        DialogDto dialog2 = new DialogDto();
        List<DialogDto> dialogs = List.of(dialog, dialog2);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String listOfDialogs = ow.writeValueAsString(dialogs);
        Mockito.when(dialogService.getUserDialogsOrderedByLastMessageDateDescending("admin")).thenReturn(dialogs);

        mockMvc.perform(MockMvcRequestBuilders.get("/dialogs")
                .requestAttr("claims", claims))
                .andExpect(status().isOk())
                .andExpect(content().json(listOfDialogs, true));
    }


    @Test
    public void shouldReturnListOfMessagesFromDialogWhenDialogExistsById() throws Exception {
        ChatMessageDto messageDto1 = new ChatMessageDto();
        messageDto1.setContent("hello");
        ChatMessageDto messageDto2 = new ChatMessageDto();
        messageDto2.setContent("hi!");
        List<ChatMessageDto> messagesDto = List.of(messageDto1, messageDto2);

        ChatMessage message1 = new ChatMessage();
        message1.setContent("hello");
        ChatMessage message2 = new ChatMessage();
        message2.setContent("hi!");
        List<ChatMessage> messages = List.of(message1, message2);

        Dialog testDialog = new Dialog();
        testDialog.setMessages(messages);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String listOfMessagesExpected = ow.writeValueAsString(messagesDto);

        Mockito.when(dialogService.findDialogById((long) 1)).thenReturn(Optional.of(testDialog));
        Mockito.when(objectMapperToDto.mapChatMessage(message1)).thenReturn(messageDto1);
        Mockito.when(objectMapperToDto.mapChatMessage(message2)).thenReturn(messageDto2);

        String path = "/getMessages/" + 1;

        mockMvc.perform(MockMvcRequestBuilders.get(path))
                .andExpect(status().isOk())
                .andExpect(content().json(listOfMessagesExpected, true));
    }

    @Test
    public void shouldFindDialogByListOfParticipantsIfDialogAlreadyExists() throws Exception {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
        DialogDto dialogDto = new DialogDto();
        Dialog dialog = new Dialog();

        AppUser user1 = new AppUser();
        user1.setId((long) 1);
        AppUser user2 = new AppUser();
        user2.setId((long) 2);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String dialogDtoExpected = ow.writeValueAsString(dialogDto);

        Mockito.when(userService.findUserByUsername("admin")).thenReturn(Optional.of(user1));
        Mockito.when(userService.findUserById((long) 2)).thenReturn(Optional.of(user2));
        Mockito.when(dialogService.findDialogByParticipants(user1, user2)).thenReturn(Optional.of(dialog));
        Mockito.when(objectMapperToDto.mapDialog(dialog)).thenReturn(dialogDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/findDialog/" + 2)
                .requestAttr("claims", claims))
                .andExpect(status().isOk())
                .andExpect(content().json(dialogDtoExpected, true));
    }


}


