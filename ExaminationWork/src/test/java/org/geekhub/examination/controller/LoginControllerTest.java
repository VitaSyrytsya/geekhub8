package org.geekhub.examination.controller;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.geekhub.examination.config.security.config.JwtTokenProvider;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.user.service.UserService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Optional;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class LoginControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;
    @MockBean
    private ObjectMapperToDto objectMapperToDto;
    @MockBean
    private JwtTokenProvider jwtTokenProvider;

    @BeforeMethod
    public void setUp() {

        this.mockMvc = MockMvcBuilders.standaloneSetup(new LoginController(userService, objectMapperToDto, jwtTokenProvider)).build();
    }

    @Test
    public void returnHttpStatusUnauthorizedWhenCredentialsDoNotMatch() throws Exception {

        Mockito.when(userService.getAuthenticatedUser("username", "password"))
                .thenReturn(Optional.empty());


        mockMvc.perform(post("/authenticate")
                .param("username", "username")
                .param("password", "password"))
                .andExpect(status().isUnauthorized());
    }


    @Test
    public void returnHttpStatusOkWhenCredentialsMatch() throws Exception {
        AppUser testUser = new AppUser();

        Mockito.when(userService.getAuthenticatedUser("username", "password"))
                .thenReturn(Optional.of(testUser));

        mockMvc.perform(post("/authenticate")
                .param("username", "username")
                .param("password", "password"))
                .andExpect(status().isOk());
    }

    @Test
    public void returnHttpStatusNotFoundWhenTryToRecoverUserByPassPhraseThatDoesNotExist() throws Exception {

        Mockito.when(userService.findUserByPassPhrase("passphrase"))
                .thenReturn(null);


        mockMvc.perform(post("/recover")
                .param("passphrase", "passphrase")
                .param("password", "password"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void returnHttpStatusOkWhenTryToRecoverExistingUser() throws Exception {
        AppUser testUser = new AppUser();

        Mockito.when(userService.findUserByPassPhrase("passphrase"))
                .thenReturn(testUser);


        mockMvc.perform(post("/recover")
                .param("passphrase", "passphrase")
                .param("password", "password"))
                .andExpect(status().isOk());
    }

    @Test
    public void returnCurrentUserIfCurrentUserExists() throws Exception {
        AppUser user = new AppUser();
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();

        Mockito.when(userService.findUserByUsername(claims.getSubject())).thenReturn(Optional.of(user));
        mockMvc.perform(get("/getUser").requestAttr("claims",claims));

        Mockito.verify(objectMapperToDto, Mockito.atLeastOnce()).mapUser(user);

    }

    @Test(expectedExceptions = NestedServletException.class)
    public void throwExceptionWhenCurrentUserDoesNotExist() throws Exception {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();

        Mockito.when(userService.findUserByUsername(claims.getSubject())).thenReturn(Optional.empty());
        mockMvc.perform(get("/getUser").requestAttr("claims",claims));

    }
}


