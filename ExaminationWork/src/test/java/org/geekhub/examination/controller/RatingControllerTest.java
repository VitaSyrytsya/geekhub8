package org.geekhub.examination.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;
import org.geekhub.examination.dto.RatingDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.entity.Rating;
import org.geekhub.examination.service.rating.service.RatingService;
import org.geekhub.examination.service.user.service.UserService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class RatingControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;
    @MockBean
    private RatingService ratingService;

    @BeforeMethod
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new RatingController(
               ratingService, userService)).build();
    }

    @Test
    public void shouldReturnRatingDtoInJsonFormatWhenRatingWasSaved() throws Exception {
        AppUser doctorUser = new AppUser();
        Doctor doctor = new Doctor();
        Rating rating = new Rating();
        doctor.setRating(rating);
        doctorUser.setDoctor(doctor);

        RatingDto ratingDto = new RatingDto();
        ratingDto.setAverageValue(10);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String returnedRatingInJsonFormat = ow.writeValueAsString(ratingDto);

        Map<Long, Integer> currentUserEvalutation = new HashMap<>();
        currentUserEvalutation.put((long) 2, 10);

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.of(doctorUser));
        Mockito.when(ratingService.save(rating, currentUserEvalutation)).thenReturn(ratingDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/rating/set/1/2")
                .param("value", String.valueOf(10)))
                .andExpect(status().isOk())
                .andExpect(content().json(returnedRatingInJsonFormat, true));
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void shouldThrowExceptionWhenUserTryToSetRatingToHimself() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/rating/set/1/1")
                .param("value", String.valueOf(10)));
    }
}



