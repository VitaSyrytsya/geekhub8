package org.geekhub.examination.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.service.doctor.service.DoctorService;
import org.geekhub.examination.service.user.service.UserService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class DoctorControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;

    @MockBean
    private DoctorService doctorService;

    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @BeforeMethod
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new DoctorController(doctorService,userService)).build();
    }

    @Test
    public void shouldReturnJsonListOfAllUsers() throws Exception {
        AppUserDto testUser = new AppUserDto();
        AppUserDto testUser2 = new AppUserDto();
        testUser.setUsername("Alla");
        testUser2.setUsername("Maria");
        List<AppUserDto>users = List.of(testUser, testUser2);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String listOfUsersInJson = ow.writeValueAsString(users);

        Mockito.when(userService.findAllUsers()).thenReturn(users);

        mockMvc.perform(get("/doctor/allUsers"))
                .andExpect(status().isOk())
                .andExpect(content().json(listOfUsersInJson, true));
    }


    @Test
    public void shouldReturnHttpStatusOkWhenDoctorFoundById() throws Exception {
        AppUserDto doctor = new AppUserDto();
        doctor.setId((long) 1);

        String path = "/doctor/get/"+ 1;

        Mockito.when(userService.findDtoUserById((long) 1)).thenReturn(doctor);

        mockMvc.perform(get(path))
                .andExpect(status().isOk());
    }


    @Test
    public void shouldDeleteDoctorWhenDoctorFoundById() throws Exception {
        AppUser doctorUser = new AppUser();
        doctorUser.setId((long) 1);
        String path = "/doctor/delete/"+ 1;

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.of(doctorUser));
        Mockito.when(doctorService.deleteDoctor(doctorUser)).thenReturn(true);

        mockMvc.perform(delete(path))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldNotDeleteDoctorWhenDoctorNotFoundById() throws Exception {
        AppUser doctorUser = new AppUser();
        doctorUser.setId((long) 1);
        String path = "/doctor/delete/"+ 1;

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.empty());
        Mockito.when(doctorService.deleteDoctor(doctorUser)).thenReturn(false);

        mockMvc.perform(delete(path))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldNotCreateDoctorWhenUserNotFoundById() throws Exception {

        String path = "/doctor/create/"+ 1;
        AppUser testUser = new AppUser();


        Doctor testDoctor = new Doctor();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonUser = ow.writeValueAsString(testDoctor);


        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.empty());
        Mockito.when(doctorService.save(testUser, testDoctor)).thenReturn(false);

        mockMvc.perform(post(path)
                .contentType(APPLICATION_JSON_UTF8)
                .content(jsonUser))
                .andExpect(status().isNoContent());
    }

}

