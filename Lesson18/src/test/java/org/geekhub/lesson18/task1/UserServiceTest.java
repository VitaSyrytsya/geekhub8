package org.geekhub.lesson18.task1;

import org.geekhub.lesson18.task1.exceptions.InvalidUserException;
import org.geekhub.lesson18.task1.service.User;
import org.geekhub.lesson18.task1.service.UserService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;



@SpringBootTest
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class UserServiceTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private UserService userService;
    @MockBean
    private DAO dao;
    @MockBean
    private EncryptPassword encryptor;

    @Test
    public void checkIfUserWouldBeAdded() {
        User user = new User("user", "password");
        user.setFirstName("user");

        userService.add(user);

        Mockito.verify(encryptor, Mockito.atLeastOnce()).encrypt("password");
        Mockito.verify(dao, Mockito.atLeastOnce()).save(user);
    }

    @Test
    public void checkIfUserServiceWouldGetAllUsers() {
        userService.getAllUsers();

        Mockito.verify(dao, Mockito.atLeastOnce()).getAllUsers();
    }

    @Test
    public void checkIfUserServiceWouldRemoveUser() {
        User user = new User();

        userService.delete(user);

        Mockito.verify(dao, Mockito.atLeastOnce()).delete(user);
    }

    @Test
    public void checkIfUserServiceWouldUpdateUser() {
        User user = new User("user", "password");
        user.setFirstName("user");

        userService.update(user);

        Mockito.verify(encryptor, Mockito.atLeastOnce()).encrypt("password");
        Mockito.verify(dao, Mockito.atLeastOnce()).update(user);
    }

    @Test
    public void checkIfUserServiceWouldFindUser() {
        userService.find("login");
        Mockito.verify(dao, Mockito.atLeastOnce()).findBy("login");
    }

    @Test(expectedExceptions = InvalidUserException.class)
    public void userServiceShouldThrowExceptionWhenTryToSaveInvalidUser() {
        User user = new User("user", "password");
        user.setFirstName("");

        userService.add(user);
    }
}
