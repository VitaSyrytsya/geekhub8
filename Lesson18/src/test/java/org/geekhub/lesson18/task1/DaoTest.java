package org.geekhub.lesson18.task1;

import org.geekhub.lesson18.task1.service.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DaoTest  {
    private EmbeddedDatabase embeddedDatabase;
    private DAO dao;

    @BeforeMethod
    public void setUp() {
        CustomRowMapper customRowMapper = new CustomRowMapper();
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .build();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(embeddedDatabase);
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(embeddedDatabase);
        dao = new DAO(jdbcTemplate, namedParameterJdbcTemplate, customRowMapper);
    }


    @AfterMethod
    public void tearDown() {
        embeddedDatabase.shutdown();
    }

    @Test
    public void checkIfElementWouldBeSaved() {
        User user = new User("user", "user");
        user.setFirstName("user");
        user.setLastName("user");
        user.setAdmin(false);

        dao.createTableForDatabase();
        dao.save(user);

        Assert.assertNotNull(dao.findBy("user"));
    }

    @Test
    public void checkIfElementWouldBeUpdated() {
        User user = new User("user", "user");
        user.setFirstName("user");
        user.setLastName("user");
        user.setAdmin(false);

        User userToUpdate = new User("user", "qwerty");
        userToUpdate.setId(1);
        userToUpdate.setFirstName("qwerty");
        userToUpdate.setLastName("qwerty");
        userToUpdate.setAdmin(true);

        dao.createTableForDatabase();
        dao.save(user);
        dao.update(userToUpdate);

        Assert.assertEquals(dao.findBy("user").getId(), 1);
        Assert.assertEquals(dao.findBy("user").getFirstName(), "qwerty");
        Assert.assertEquals(dao.findBy("user").getPassword(), "qwerty");
        Assert.assertTrue(dao.findBy("user").isAdmin());
    }

    @Test
    public void checkIfElementWouldBeDeleted() {
        User user = new User("user", "user");
        user.setFirstName("user");
        user.setLastName("user");
        user.setAdmin(false);

        dao.createTableForDatabase();
        dao.save(user);
        dao.delete(user);

        Assert.assertTrue(dao.getAllUsers().isEmpty());
    }

    @Test
    public void checkIfDaoWouldGetAllUsersFromDatabase() {
        User user = new User("user", "user");
        user.setFirstName("user");
        user.setLastName("user");
        user.setAdmin(false);

        User user1 = new User("user1", "user1");
        user.setFirstName("abc");
        user.setLastName("user1");
        user.setAdmin(false);

        dao.createTableForDatabase();
        dao.save(user);
        dao.save(user1);
        user.setId(1);user1.setId(2);

        Assert.assertEquals(dao.getAllUsers().get(0).getLogin(), "user1");
        Assert.assertEquals(dao.getAllUsers().get(1).getLogin(), "user");
    }
}
