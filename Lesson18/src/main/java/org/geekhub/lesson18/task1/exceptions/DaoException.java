package org.geekhub.lesson18.task1.exceptions;

public class DaoException extends RuntimeException{
    public DaoException(String s){
        super(s);
    }
}
