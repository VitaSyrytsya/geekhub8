package org.geekhub.lesson18.task1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.sql.DataSource;


@Configuration
@EnableWebMvc
@ComponentScan({"org.geekhub.lesson18.task1"})
public class JavaConfiguration implements WebMvcConfigurer {

    private final Environment environment;

    @Autowired
    public JavaConfiguration(Environment environment) {
        this.environment = environment;
    }

    @Bean
    @Primary
    public DataSource dataSource(Environment environment) {
        final SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.h2.Driver.class);
        dataSource.setUrl(environment.getProperty("url"));
        dataSource.setUsername(environment.getProperty("login"));
        dataSource.setPassword(environment.getProperty("password"));
        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public CustomRowMapper rowMapper() {
        return new CustomRowMapper();
    }

    @Bean(name = "databaseFiller")
    public DatabaseFillerOnStartUp getDatabase() {
        return new DatabaseFillerOnStartUp();
    }


    @Bean(name = "DAO")
    public DAO getDAO() {
        return new DAO(jdbcTemplate(dataSource(environment)),
                namedParameterJdbcTemplate(dataSource(environment)),
                new CustomRowMapper());
    }

    @Bean(name = "passwordEncryptor")
    public EncryptPassword getPasswordEncryptor() {
        return new EncryptPassword();
    }








}

