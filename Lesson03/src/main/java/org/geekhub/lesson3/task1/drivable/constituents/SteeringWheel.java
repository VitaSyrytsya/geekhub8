package org.geekhub.lesson3.task1.drivable.constituents;

import org.geekhub.lesson3.task1.drivable.StatusAware;

public class SteeringWheel implements StatusAware {
    int usedMaxTurnAngle;
    int maxTurnAngle;
    int usedTurnAngleStep;
    int createdTurnAngleStep;
    int turnAngle;

    public SteeringWheel(int i, int i1) {
        usedMaxTurnAngle = i;
        usedTurnAngleStep = i1;
        turnAngle = 0;
    }


    public int getMaxTurnAngle() {
        if (usedMaxTurnAngle > 78) {
            maxTurnAngle = 78;
        } else if (usedMaxTurnAngle <= 78 && usedMaxTurnAngle >= 40) {
            maxTurnAngle = usedMaxTurnAngle;
        } else if (usedMaxTurnAngle < 40) {
            maxTurnAngle = 40;
        }
        return maxTurnAngle;

    }

    public int getTurnAngleStep() {
        if (usedTurnAngleStep > 30) {
            createdTurnAngleStep = 30;
        } else if (usedTurnAngleStep < 10) {
            createdTurnAngleStep = 10;
        } else if (usedTurnAngleStep <= 30 && usedTurnAngleStep >= 10) {
            createdTurnAngleStep = usedTurnAngleStep;
        }
        return createdTurnAngleStep;

    }

    public int getTurnAngle() {
        if (turnAngle == 0) {
            return 0;
        } else if (turnAngle >= -50) {
            return turnAngle;
        }
        return turnAngle;
    }

    public void turnLeft() {

        if (turnAngle + -1 * usedTurnAngleStep > -50) {
            turnAngle = -1 * usedTurnAngleStep + turnAngle;
            return;
        } else if ((-1 * usedTurnAngleStep + turnAngle) <= -50) {
            turnAngle = -50;
            return;
        }
    }

    public void turnRight() {
        if (usedTurnAngleStep + turnAngle < 50) {
            turnAngle = usedTurnAngleStep + turnAngle;
        } else if (turnAngle + usedTurnAngleStep >= 50) {
            turnAngle = 50;
        }
    }


    public String status() {
        String n = null;
        if (turnAngle == 0) {
            n = "turn angle = 0";
        } else if (turnAngle < 0 || turnAngle > 0) {
            n = "turn angle = " + turnAngle;
        }


        return n;
    }
}
