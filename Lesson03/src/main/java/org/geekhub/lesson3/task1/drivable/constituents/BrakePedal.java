package org.geekhub.lesson3.task1.drivable.constituents;


public class BrakePedal {
    int usedStrength;
    int breakingStrength;

    public BrakePedal(int i) {
        usedStrength = i;
    }

    public int getBrakingStrength() {
        if (usedStrength > 25) {
            breakingStrength = 25;
        } else if (usedStrength < 5) {
            breakingStrength = 5;
        } else if (usedStrength <= 25 && usedStrength >= 5) {
            breakingStrength = usedStrength;
        }
        return breakingStrength;
    }
}
