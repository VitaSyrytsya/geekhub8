package org.geekhub.lesson3.task1.drivable.cars;

import org.geekhub.lesson3.task1.drivable.Vehicle;
import org.geekhub.lesson3.task1.drivable.constituents.*;

public class Hatchback5Doors extends Vehicle {
    int numberOfPassengers = 5;
    int numberOfDoors = 5;
    String carName;

    public Hatchback5Doors(String getz, Accelerator accelerator, BrakePedal brakePedal, Engine engine, GasTank gasTank, SteeringWheel steeringWheel) {
        this.carName = getz;
    }


    public String getName() {
        return carName;
    }


    public int getMaxNumberOfPassengers() {
        return numberOfDoors;
    }


    public int getNumberOfDoors() {
        return numberOfPassengers;
    }

}
