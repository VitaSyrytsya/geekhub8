package org.geekhub.lesson3.task1.drivable.cars;

import org.geekhub.lesson3.task1.drivable.Vehicle;
import org.geekhub.lesson3.task1.drivable.constituents.*;

public class SUV extends Vehicle {
    int numberOfDoors = 5;
    int numberOfPassengers = 7;
    String carName;

    public SUV(String pajero, Accelerator accelerator, BrakePedal brakePedal, Engine engine, GasTank gasTank, SteeringWheel steeringWheel) {
        this.carName = pajero;
    }


    public String getName() {
        return carName;
    }


    public int getMaxNumberOfPassengers() {
        return numberOfPassengers;
    }


    public int getNumberOfDoors() {
        return numberOfDoors;
    }


}
