package org.geekhub.lesson3.task1.controlpanel;

import org.geekhub.lesson3.task1.drivable.Drivable;
import org.geekhub.lesson3.task1.drivable.constituents.Engine;
import org.geekhub.lesson3.task1.drivable.constituents.GasTank;
import org.geekhub.lesson3.task1.drivable.constituents.SteeringWheel;

public interface ControlPanel {


    void turnLeft(Drivable car);


    void turnRight(Drivable car);

    void accelerate(Drivable car);

    int brake(Drivable car);

    void fillTank(Drivable car, int i);
}
