package org.geekhub.lesson3.task1.drivable.constituents;


public class Accelerator {

    final static int times = 5;
    int usedStrength;
    int acceleratorStrength;


    public Accelerator(int i) {

        usedStrength = i;
    }

    public int getAccelerateStrength() {
        if (usedStrength > 25) {

            acceleratorStrength = 25;

        }
        if (usedStrength < 5) {

            acceleratorStrength = 5;
        }
        if (usedStrength >= 5 && usedStrength <= 25) {
            acceleratorStrength = usedStrength;
            return acceleratorStrength;
        }
        return acceleratorStrength;
    }

    double accelerate;

    public double accelerate(Engine engine, GasTank gasTank) {

        if (gasTank.isEmpty() && engine.isStarted()) {
            accelerate = 0;
        } else if (engine.isStarted() && gasTank.isEmpty()) {
            accelerate = gasTank.currentVolume * getAccelerateStrength() / engine.getCapacity();
            int maxAccelerate = getAccelerateStrength() * times;
            if (accelerate >= maxAccelerate) {
                accelerate = maxAccelerate;
            }
        }
        return accelerate;


    }


}
