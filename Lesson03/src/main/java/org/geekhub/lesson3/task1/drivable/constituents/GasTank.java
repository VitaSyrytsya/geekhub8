package org.geekhub.lesson3.task1.drivable.constituents;

import org.geekhub.lesson3.task1.drivable.StatusAware;

public class GasTank implements StatusAware {


    double currentVolume;

    int usedMaxVolume;

    public GasTank(int i) {
        usedMaxVolume = i;
        currentVolume = 0;
    }

    public double getCurrentVolume() {
        if (currentVolume == 0) {
            return currentVolume;
        }
        if (currentVolume > 0) {
            return currentVolume;
        }
        return currentVolume;
    }

    public void fill(double i) {
        if (currentVolume + i >= getMaxVolume()) {
            currentVolume = getMaxVolume();

        } else if (currentVolume + i < getMaxVolume() && i >= 0) {
            currentVolume = currentVolume + i;

        } else if (i < 0) {
            currentVolume = currentVolume;
        }

    }

    public String status() {
        String st = currentVolume / getMaxVolume() * 100 + "% of fuel exist";
        return st;
    }

    public void use(double i) {
        if (i >= currentVolume) {
            currentVolume = 0;
        } else if (i < currentVolume && i >= 0) {
            currentVolume = currentVolume - i;

        } else if (i <= 0) {
            currentVolume = currentVolume;
        }
    }

    public boolean isEmpty() {
        if (currentVolume == 0 || currentVolume < 0.001) {
            return true;

        } else if (currentVolume >= 0.001) {
            return false;
        }
        return false;
    }

    int maxVolume;

    public int getMaxVolume() {

        if (usedMaxVolume < 5) {
            maxVolume = 5;
            return maxVolume;
        } else if (usedMaxVolume >= 5 && usedMaxVolume <= 100) {
            maxVolume = usedMaxVolume;
            return maxVolume;
        } else if (usedMaxVolume > 100) {
            maxVolume = 100;
            return maxVolume;
        }
        return maxVolume;
    }
}
