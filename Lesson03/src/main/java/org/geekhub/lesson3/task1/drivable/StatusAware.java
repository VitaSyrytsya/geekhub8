package org.geekhub.lesson3.task1.drivable;

public interface StatusAware {
    String status();
}
