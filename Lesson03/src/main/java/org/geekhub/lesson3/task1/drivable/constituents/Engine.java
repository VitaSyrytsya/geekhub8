package org.geekhub.lesson3.task1.drivable.constituents;

import org.geekhub.lesson3.task1.drivable.StatusAware;

public class Engine implements StatusAware {

    double usedCapacity;
    double capacity;
    int o;


    public Engine(double i, int i1) {
        o = 0;
        usedCapacity = i;
        usedMaxSpeed = i1;
        isStarted();
    }


    public double getCapacity() {
        if (usedCapacity > 6) {
            capacity = 6;
        } else if (usedCapacity < 0.5) {
            capacity = 0.5;
        } else if (usedCapacity <= 6 && usedCapacity >= 0.5) {
            capacity = usedCapacity;
        }
        return capacity;
    }

    int createdMaxSpeed;
    int usedMaxSpeed;

    public int getMaxSpeed() {
        if (usedMaxSpeed > 400) {
            createdMaxSpeed = 400;
        } else if (usedMaxSpeed < 60) {
            createdMaxSpeed = 60;
        } else if (usedMaxSpeed <= 400 && usedMaxSpeed >= 60) {
            createdMaxSpeed = usedMaxSpeed;
        }
        return createdMaxSpeed;


    }


    public void start() {
        o = 1;
        isStarted();
    }


    String sstatus;

    public String status() {
        if (isStarted()) {
            sstatus = "started";
        } else {
            sstatus = "stopped";
        }
        return sstatus;
    }


    public boolean isStarted() {
        if (o == 1) {
            return true;
        } else {
            return false;
        }

    }


    public void stop() {
        o = -1;
        isStarted();
    }

    public void work(GasTank tank) {
        if (isStarted()) {
            tank.getCurrentVolume();
            tank.use(getCapacity());
        } else {
            tank.getCurrentVolume();
        }


    }
}
