package org.geekhub.lesson6.task2;
//
public class TaskLimitExceededException extends RuntimeException  {

    public TaskLimitExceededException(String message) {
        super(message);
    }

}
