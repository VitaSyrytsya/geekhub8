package org.geekhub.lesson6.task2;
//
import java.time.LocalDate;

public class Task implements Comparable<Task> {
    private final String category;
    private final String description;
    private final LocalDate date;

    public Task(String home, String clean_kitchen, LocalDate date) {
        this.category = home;
        this.description = clean_kitchen;
        this.date = date;
    }


    public String getDescription() {

        return description;
    }


    public String getCategory()
    {
        return category;
    }





    public LocalDate getDate() {

        return date;
    }

    @Override
    public String toString() {
        return category+ ""+ description+ ""+ date;
    }


    @Override
    public boolean equals(Object obj) {
        Task task = (Task)obj;
        return this.category.equals(task.category)&&
              this.description.equals(task.description) &&
                this.date.isEqual(((Task) obj).date);
    }

    @Override
    public int compareTo(Task o) {
        if( this.date.isBefore(o.date)){
            return 1;
        }else if((o.date).isBefore(this.date)){
            return -1;
        }else{
            return 0;
        }

    }
}
