package org.geekhub.lesson6.task1;

public class Color {
    public static Color RED = new Color(255,0,0);
    public static Color GREEN = new Color(0,255,0);
    public static Color BLUE = new Color(0,255,0);

    private int i;
    private int i1;
    private int i2;


    public Color(int i, int i1, int i2) {
        this.i = i;
        this.i1 = i1;
        this.i2 = i2;
    }

    @Override
    public boolean equals(Object obj) {
        Color color = (Color) obj;
        return this.i == color.i&&
                this.i1 == color.i1&&
                this.i2 ==color.i2;
    }

    public int getI1() {
        return i1;
    }

    public int getI() {
        return i;
    }

    public int getI2() {
        return i2;
    }
}
