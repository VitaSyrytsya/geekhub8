package org.geekhub.lesson6.task1;
//

import java.util.List;

public interface Garage {

    List<Car> getCars();

}