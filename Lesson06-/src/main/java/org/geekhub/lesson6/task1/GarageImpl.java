package org.geekhub.lesson6.task1;

import java.util.*;
import java.util.List;


public class GarageImpl implements Garage {

    private Car[] car;

    public GarageImpl(Car... car) {
        this.car = car;
    }

    @Override
    public List<Car> getCars() {
        List<Car> carrs = new ArrayList<>(Arrays.asList(car));
        if (carrs.isEmpty()) {
            return carrs;
        } else {
            Collections.sort(carrs);
            return carrs;
        }

    }
}
