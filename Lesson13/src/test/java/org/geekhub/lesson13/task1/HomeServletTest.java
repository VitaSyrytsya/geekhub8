package org.geekhub.lesson13.task1;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class HomeServletTest extends Mockito {
    private HomeServlet homeServlet;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    @BeforeMethod
    public void setUp() {
        homeServlet = new HomeServlet();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
    }

    @Test
    public void checkIfElementWillBeAddedToMapIfKeyExistsAlready() throws IOException {
        Map<String, LinkedList<String>> animals = new HashMap<>();
        LinkedList<String> dogs = new LinkedList<>(Arrays.asList("Bim", "Rex"));

        animals.put("Dog", dogs);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("add");
        when(request.getParameter("key")).thenReturn("Dog");
        when(request.getParameter("attribute")).thenReturn("Alto");
        when(session.getAttribute("animals")).thenReturn(animals);
        homeServlet.doPost(request, response);

        Assert.assertEquals(animals.get("Dog").getLast(), "Alto");
    }

    @Test
    public void checkIfElementAndKeyWillBeAddedToMapIfKeyDoesNotExist() throws IOException {
        Map<String, LinkedList<String>> animals = new HashMap<>();
        LinkedList<String> dogs = new LinkedList<>(Arrays.asList("Bim", "Rex"));

        animals.put("Dog", dogs);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("add");
        when(request.getParameter("key")).thenReturn("Cat");
        when(request.getParameter("attribute")).thenReturn("Richard");
        when(session.getAttribute("animals")).thenReturn(animals);
        homeServlet.doPost(request, response);

        Assert.assertEquals(animals.get("Cat").getFirst(), "Richard");
    }

    @Test
    public void checkIfExistingElementWillBeRemovedFromMap() throws IOException {
        Map<String, LinkedList<String>> animals = new HashMap<>();
        LinkedList<String> dogs = new LinkedList<>(Arrays.asList("Bim", "Rex"));

        animals.put("Dog", dogs);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("delete");
        when(request.getParameter("key")).thenReturn("Dog");
        when(request.getParameter("attribute")).thenReturn("Bim");
        when(session.getAttribute("animals")).thenReturn(animals);
        homeServlet.doPost(request, response);

        Assert.assertEquals(1, animals.get("Dog").size());
        Assert.assertEquals("Rex", animals.get("Dog").getFirst());
    }

    @Test
    public void checkIfKeyWillBeRemovedFromMapWhenDeleteTheLastAttribute() throws IOException {
        Map<String, LinkedList<String>> animals = new HashMap<>();
        LinkedList<String> dogs = new LinkedList<>(Collections.singletonList("Rex"));

        animals.put("Dog", dogs);
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("action")).thenReturn("delete");
        when(request.getParameter("key")).thenReturn("Dog");
        when(request.getParameter("attribute")).thenReturn("Rex");
        when(session.getAttribute("animals")).thenReturn(animals);
        homeServlet.doPost(request, response);

        Assert.assertTrue(animals.isEmpty());
    }

    @Test
    public void shouldThrowExceptionWhenInputValuesAreEmpty(){
        boolean thrown = false;
        Map<String, List<String>> animals = new HashMap<>();

        try {
            homeServlet.transformMapDependingOnInputAttributes(animals);
        } catch (InvalidObjectException e) {
            thrown = true;
        }
        Assert.assertTrue(thrown);
    }
}
//