package org.geekhub.lesson13.task1;

import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(filterName = "browser")
public class BrowserFilter implements Filter {

    private boolean isBrowserValid(String userAgent) {
        if (userAgent.contains("Chrome")) {
            String substring = userAgent.substring(userAgent.indexOf("Chrome"))
                    .split(" ")[0];
            String browserVer = substring.split("/")[1].substring(0, 2);
            System.out.println("browser version" + Integer.parseInt(browserVer));
            if (Integer.parseInt(browserVer) < 65) {
                throw new BrowserUnsupportedException ("Please, update your browser to do this operation!");
            }
        } else if (userAgent.contains("Edge")) {
            throw new BrowserUnsupportedException("Your browser does not support this operation!");
        }
        return true;
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        UserAgent userAgent = UserAgent.parseUserAgentString(((HttpServletRequest) request).getHeader("User-Agent"));
        String browserName = userAgent.getBrowser().getName();
        System.out.println(browserName);
        String usserAgent = httpServletRequest.getHeader("User-Agent");
        if (isBrowserValid(usserAgent)) {
            chain.doFilter(request, response);
        }
    }
}
