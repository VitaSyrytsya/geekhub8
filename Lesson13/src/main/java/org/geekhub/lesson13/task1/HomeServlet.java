package org.geekhub.lesson13.task1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@WebServlet(name = "animal", urlPatterns = {"/animal"})
public class HomeServlet extends HttpServlet {
    private Map<String, List<String>> animals = new HashMap<>();
    private String attribute = "";
    private String key = "";
    private String action = "";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<String> cats = new LinkedList<>(Arrays.asList("Tom", "Dave", "Kate"));
        List<String> mouses = new LinkedList<>(Arrays.asList("Jerry", "Mikky"));
        List<String> dogs = new LinkedList<>(Arrays.asList("Bim", "Rex"));
        animals.put("Cat", cats);
        animals.put("Dog", dogs);
        animals.put("Mouse", mouses);

        session.setAttribute("animals", animals);
        req.getRequestDispatcher("/myJSP/animals.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();

        action = req.getParameter("action");
        key = req.getParameter("key");
        attribute = req.getParameter("attribute");
        Map<String, List<String>> map = (Map<String, List<String>>) session.getAttribute("animals");

        transformMapDependingOnInputAttributes(map);

        session.removeAttribute("animals");
        session.setAttribute("animals", map);
        resp.sendRedirect("/myJSP/animals.jsp");
    }

    protected void transformMapDependingOnInputAttributes(Map<String, List<String>> map) throws InvalidObjectException {
        if (key.isEmpty() || attribute.isEmpty()) {
            throw new InvalidObjectException("Please, input values!");
        }
        if (action.equals("delete")) {
            if (map.get(key).size() == 1) {
                map.remove(key);
            } else {
                map.get(key).remove(attribute);
            }
        } else if (action.equals("add")) {
            Set<String> keys = map.keySet();
            if (listContainsKey(keys, key)) {
                map.get(key).add(attribute);
            } else {
                List<String> newList = new LinkedList<>(Collections.singleton(attribute));
                map.put(key, newList);
            }
        }
    }

    private boolean listContainsKey(Set<String> list, String key) {
        for (String l : list) {
            if (key.equals(l)) {
                return true;
            }
        }
        return false;
    }
}
//