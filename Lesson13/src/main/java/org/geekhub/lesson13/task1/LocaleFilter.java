package org.geekhub.lesson13.task1;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@WebFilter(filterName = "locale")
public class LocaleFilter implements Filter {

    private boolean checkIfCurrentTimeIsValid(Date currentTime){
        DateFormat dateFormat = new SimpleDateFormat("HH.mmss");
        double time = Double.parseDouble(dateFormat.format(currentTime));
        return (!(time >= 1)) || (!(time < 5));
    }



    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Date currentTime = Calendar.getInstance().getTime();
        if(checkIfCurrentTimeIsValid(currentTime)){
            chain.doFilter(request, response);
        }else {
            throw new TimeLimitException("Time limited");
        }
    }
}
//