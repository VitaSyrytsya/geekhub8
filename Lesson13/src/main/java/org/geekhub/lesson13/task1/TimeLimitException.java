package org.geekhub.lesson13.task1;

class TimeLimitException extends RuntimeException {
    TimeLimitException(String s){
        super(s);
    }
}
