package org.geekhub.lesson13.task1;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;

@WebFilter(filterName = "execution")

public class ExecutionTimeLoggerFilter implements Filter {
    private static final Logger log = Logger.getLogger(ExecutionTimeLoggerFilter.class.getName());

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();

        log.info("Creation time:"+ new Date(session.getCreationTime()));
        chain.doFilter(request, response);
        log.info("Last access time" + new Date(session.getLastAccessedTime()));
        log.info("Total execution time:" + (session.getLastAccessedTime()-session.getCreationTime()));
    }
}
//