<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "add" uri = "/WEB-INF/standartTag.tld"%>
<!DOCTYPE html>
<html>
<head>
    <style>
        .classlink {
            margin: 0;
            border: 0;
            background: none;
            overflow: visible;
            color: blue;
            cursor: pointer;
            text-decoration: underline;
        }
    </style>
</head>
<body>
<table border="1" cellspacing="0" cellpadding="15" width="30%" height="40">
    <tr>
        <td><h3>Name</h3></td>
        <td><h3>Value</h3></td>
        <td><h3>Action</h3></td>
    </tr>
    <c:forEach var="animalType" items="${animals.keySet()}">
        <td>${animalType}</td>
        <c:forEach var="an" items="${animals.get(animalType)}">
            <td> ${an}</td>
            <td>
                <form action="/animal" method="post">
                    <input type="hidden" name="action" value="${"delete"}">
                    <input type="hidden" name="attribute" value="${an}">
                    <input type="hidden" name="key" value="${animalType}">
                    <input type="submit" value="delete" class="classlink">
                </form>
            </td>
            <tr>
            <c:choose>
                <c:when test="${an.equals(animals.get(animalType).getLast())}">
                </c:when>
                <c:otherwise>
                    <td></td>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        </tr>
    </c:forEach>
    <tr>
        <form action="/animal" method="post">
            <td><input type="text" name="key"></td>
            <td><input type="text" name="attribute"></td>
            <td><input type="submit" name="action" value="add"></td>
        </form>
    </tr>
</table>
</body>
</html>

<!--!-->




