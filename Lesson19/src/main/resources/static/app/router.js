angular.module('Lesson19').config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('users', {
            url: '/users',
            templateUrl: 'app/view/users.html',
            controller: 'UsersController'
        })
        .state('simple', {
            url: '/simpleUsers',
            templateUrl: 'app/view/users-for-simple-user.html',
            controller: 'SimpleController'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'app/view/login.html',
            controller: 'LoginController'
        })
});
