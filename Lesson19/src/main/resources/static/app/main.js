angular.module('Lesson19', ['ui.router'])

    .run(function ($rootScope, $state) {

        $rootScope.$on('$stateChangeStart', function () {

            $state.go('login');

        });
    });