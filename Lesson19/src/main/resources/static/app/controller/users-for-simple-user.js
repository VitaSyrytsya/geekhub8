angular.module('Lesson19')
    .controller('SimpleController', function($http, $scope, $state) {

        var init = function(){
            $http({
                method: 'GET',
                url: 'user/getAll',
            }).success(function (res) {
                $scope.users = res;
            });
        };
        init();
    });
