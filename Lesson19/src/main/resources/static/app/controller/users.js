angular.module('Lesson19')
    .controller('UsersController', function($http, $scope, $state) {

        var init = function(){
            $http({
                method: 'GET',
                url: 'user/getAll',
            }).success(function (res) {
               $scope.users = res;
            });
        };

        init();

        $scope.search = function(username){
            $http.get('user/get/' +  username)
                .success(function (res) {
                    $scope.users = res;
                });
        };

        $scope.entity = {};

        $scope.edit = function(index){
            console.log($scope.users[index]);
            $scope.entity = $scope.users[index];
            $scope.entity.index = index;
            $scope.entity.editable = true;
        };

        $scope.delete = function(index){
            $http.delete('user/delete/' +  $scope.users[index].id)
                .success(function () {
                    init();
            });
        };

        $scope.save = function(index){
            $scope.users[index].editable = false;

            $http.post('user/add', $scope.users[index])
                    .success(function () {
                        init();
                    });



        };

        $scope.add = function(){
            $scope.users.push({
                name : "",
                country : "",
                editable : true
            })
        }
    });
