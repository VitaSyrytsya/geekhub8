package org.geekhub.lesson19.controller;

import org.geekhub.lesson19.persistence.Session;
import org.geekhub.lesson19.persistence.User;
import org.geekhub.lesson19.service.user.service.UserService;
import org.geekhub.lesson19.service.session.service.SessionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;
    private final SessionService sessionService;
    private static final Long defaultUserId = 1L;

    public UserController(UserService userService, SessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @GetMapping(value = "/getAll")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping(value = "/get/{username}")
    public List<User> findUserByUsername(@PathVariable String username) {
       List<User>user = new ArrayList<>();
        if(userService.find(username).isPresent()){
            user.add(userService.find(username).get());
        }
        return user;
    }

    @PostMapping(value = "/add")
    public ResponseEntity add(@RequestBody User user,
                              HttpServletRequest request) {
        if (checkIfCurrentUserHasAdminRights(request)) {
            if(user.getId()!=null && user.getId().equals(defaultUserId)){
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
            if (userService.add(user)) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity add(@PathVariable Long id,
                              HttpServletRequest request) {
        if(checkIfCurrentUserHasAdminRights(request)){
           if(userService.findById(id).isPresent()&&!userService.findById(id)
                   .get().getId()
                   .equals(defaultUserId)){
               User currentUser = userService.findById(id).get();
               if(userService.delete(currentUser)){
                   return new ResponseEntity<>(HttpStatus.OK);
               }
           }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }


    private boolean checkIfCurrentUserHasAdminRights(HttpServletRequest request) {
        HttpSession currentUserSession = request.getSession();
        Session customSession = sessionService.get(currentUserSession.getId());
        if (customSession != null) {
            String currentUserName = customSession.getUsername();
            if (userService.find(currentUserName).isPresent()) {
                User currentUser = userService.find(currentUserName).get();
                return currentUser.isAdmin();
            }
        }
        return false;
    }
}
