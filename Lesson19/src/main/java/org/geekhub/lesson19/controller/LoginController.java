package org.geekhub.lesson19.controller;

import org.geekhub.lesson19.persistence.Session;
import org.geekhub.lesson19.persistence.User;
import org.geekhub.lesson19.service.user.service.UserService;
import org.geekhub.lesson19.service.session.service.SessionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class LoginController {

    private final UserService userService;
    private final SessionService sessionService;

    public LoginController(UserService userService, SessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @GetMapping(value = "/login")
    public ResponseEntity<User> getAuthenticatedUser(@RequestParam String username,
                                                     @RequestParam String password,
                                                     HttpServletRequest request){


        if(userService.getAuthenticatedUser(username, password).isPresent()){
            User currentUser = userService.getAuthenticatedUser(username, password).get();
            HttpSession httpSession = request.getSession();
            Session session = new Session();
            session.setSessionId(httpSession.getId());
            session.setUsername(username);
            sessionService.add(session);
            return new ResponseEntity<>(currentUser,
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
