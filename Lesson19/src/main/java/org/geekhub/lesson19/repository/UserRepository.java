package org.geekhub.lesson19.repository;

import org.geekhub.lesson19.persistence.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}