package org.geekhub.lesson19.repository;

import org.geekhub.lesson19.persistence.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;

@Repository
public interface SessionRepository extends JpaRepository<Session, String> {
   Session findBySessionId(String sessionId);
   Session findByUsername(String username);
}
