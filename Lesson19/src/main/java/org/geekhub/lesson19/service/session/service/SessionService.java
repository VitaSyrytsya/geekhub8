package org.geekhub.lesson19.service.session.service;

import org.geekhub.lesson19.persistence.Session;

public interface SessionService {

    Session add(Session session);

    Session get(String sessionId);
}

