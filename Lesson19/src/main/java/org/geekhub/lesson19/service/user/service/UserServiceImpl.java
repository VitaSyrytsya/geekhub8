package org.geekhub.lesson19.service.user.service;

import org.geekhub.lesson19.persistence.User;
import org.geekhub.lesson19.repository.UserRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> find(String username) {
        return Optional.of(userRepository.findByUsername(username));
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> getAuthenticatedUser(String login, String password) {
        if (userRepository.findByUsername(login) != null) {
            User user = userRepository.findByUsername(login);

            if (user.getPassword().equals(passwordEncoder.encode(password))) {
                return Optional.of(user);
            }
            return Optional.empty();
        }
        return Optional.empty();
    }

    @Override
    public boolean add(User user) {
        if (checkIfUserFieldsAreNotEmpty(user)) {
            if (!user.isAdmin()) {
                user.setAdmin(false);
            }
            if (user.getId() != null && user.getPassword()
                    .equals(userRepository.findById(user.getId()).get().getPassword())) {
                userRepository.save(user);
                return true;
            } else {
                user.setPassword(passwordEncoder.encode(user.getPassword()));
            }
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(User user) {
       userRepository.delete(user);
       return true;
    }



    private boolean checkIfUserFieldsAreNotEmpty(User user) {
        return user.getPassword() != null && !user.getPassword().isEmpty() &&
                user.getUsername() != null && !user.getUsername().isEmpty() &&
                user.getFirstName() != null && !user.getFirstName().isEmpty();
    }




}
