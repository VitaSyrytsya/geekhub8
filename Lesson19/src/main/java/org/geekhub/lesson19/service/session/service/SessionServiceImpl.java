package org.geekhub.lesson19.service.session.service;

import org.geekhub.lesson19.persistence.Session;
import org.geekhub.lesson19.repository.SessionRepository;
import org.springframework.stereotype.Service;

@Service
public class SessionServiceImpl implements SessionService {

    private final SessionRepository sessionRepository;

    public SessionServiceImpl(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    public Session add(Session session) {
        if(sessionRepository.findByUsername(session.getUsername())!=null){
            sessionRepository.delete(sessionRepository.findByUsername(session.getUsername()));
        }
        return sessionRepository.save(session);
    }


    @Override
    public Session get(String sessionId) {
        return sessionRepository.findBySessionId(sessionId);
    }
}
