package org.geekhub.lesson19.service.user.service;

import org.geekhub.lesson19.persistence.User;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAllUsers();

    Optional<User> find(String username);

    Optional<User> findById(Long id);

    Optional<User> getAuthenticatedUser(String login, String password);

    boolean add(User user);

    boolean delete(User user);


}
