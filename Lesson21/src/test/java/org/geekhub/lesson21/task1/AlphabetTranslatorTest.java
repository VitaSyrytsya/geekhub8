package org.geekhub.lesson21.task1;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AlphabetTranslatorTest {

    private AlphabetTranslator alphabetTranslator;

    @BeforeMethod
    public void setUp() {
        alphabetTranslator = new AlphabetTranslator();
    }

    @Test(dataProvider = "actualAndConvertedStrings")
    public void shouldConvertActualStringToMorseAlphabetString(String actual, String expected){
        Assert.assertEquals(alphabetTranslator.translateRegularString(actual), expected);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenSymbolNotFound(){

        alphabetTranslator.translateRegularString("$0m*th!#g Br0k*#!");
    }


    @DataProvider
    private Object[][] actualAndConvertedStrings() {
        return new Object[][]{
                {"SOS", "...---..."},
                {"S O S",".../---/..."},
                {"It is a good day to die" ,"..-/...../.-/--.-------../-...--.--/----/-....."},
                {"GeekHub", "--...-.-......--..."}
        };
    }
}
